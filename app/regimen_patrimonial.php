<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class regimen_patrimonial extends Model
{
    protected $table ='regimen_patrimonial';
    public $timestamps = false;
}
