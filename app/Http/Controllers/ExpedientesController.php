<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;;
use App\Expedientes;
use Illuminate\Pagination\LengthAwarePaginator;
use DB;
use App\user;
use App\tramites_tipo;
use App\tramite;

use App\notaria;
use App\modulos;
use App\tramite_comprador;
use App\entidad;
use App\tipo_inmueble;
use App\tipo_operacion;
use App\desarrollador;
use App\bancos;
use App\fondos;
use App\estado_civil;
use App\regimen_patrimonial;
use App\nacionalidad;
use App\adquirente;
use App\tipo_iodentificacion;
use App\ejecutivo_desarrollador;
use App\ejecutivo_desarrollador_tipo;
use App\tramitetipocompravende;

use Illuminate\Support\Facades\Auth;



class ExpedientesController extends Controller
{
public function init(){
        $var = tramites_tipo::all();
        $nota = notaria::all();
        $modu = modulos::all();
        $tra = tramite::all();
        $ent = entidad::all();
        $tipoInm = tipo_inmueble::all();
        $tipoOpe = tipo_operacion::all();
        $desa = desarrollador::all();
        $ban = bancos::all();
        $fon = fondos::all();
        $estadoCiv = estado_civil::all();
        $regimenPat = regimen_patrimonial::all();
        $nac = nacionalidad::all();
        $adq = adquirente::all();
        $tipoioden = tipo_iodentificacion::all();
        $tramitetipocomven = tramitetipocompravende::all();
        $user = user::all();

        return view('expedientes.expediente_add',[
            'tramites_tipo' => $var,
            'notaria' => $nota,
            'modulos' => $modu,
            'tramite' => $tra,
            'entidad' => $ent,
            'tipo_inmueble' => $tipoInm,
            'tipo_operacion' => $tipoOpe,
            'desarrollador' => $desa,
            'bancos' => $ban,
            'fondos' => $fon,
            'estado_civil' => $estadoCiv,
            'regimen_patrimonial' => $regimenPat,
            'nacionalidad' => $nac,
            'adquirente' => $adq,
            'tipo_iodentificacion' => $tipoioden,
            'tramitetipocompravende' => $tramitetipocomven,
            'user' => $user,
        ]);
    }

    public function GeneralInmueble(Request $request)
    {
        $tramite = new tramite;
        // $tramite->Folio = $request->Folio;
        $tramite->idTipoTramite = $request->TipoTramite;
        $tramite->idNotaria = $request->Notaria;
        $tramite->FechaApertura = $request->FechaApertura;
        $tramite->FechaTermino = $request->FechaTermino;
        $tramite->idEntidad = $request->EntidadFederativa;
        $tramite->idModulos = $request->modulo;
        $tramite->Calle = $request->Calle;
        $tramite->NumExt = $request->NumExt;
        $tramite->NumInt = $request->NumInt;
        $tramite->Colonia = $request->Colonia;
        $tramite->CP = $request->CP;
        $tramite->ValorCatastral = $request->ValorCatastral;
        $tramite->ValorAvaluo = $request->ValorAvaluo;
        $tramite->ValorVenta = $request->ValorVenta;
        $tramite->idTipoInmuebles = $request->TipoInmueble;
        $tramite->idTipoOperacion = $request->TipoOperacion;
        $tramite->idDesarrollador = $request->Desarrollador;
        $tramite->idBanco = $request->Banco;
        $tramite->idFondoVivienda = $request->FondoVivienda;
        $tramite->isCodominio = $request->isCondominio;
        $tramite->municipio = $request->municipio;
        $tramite->TieneAvaluoBancario = $request->TieneAvaluoBancario;
        $tramite->AplicaJornadaNotarial = $request->AplicaJornadaNotarial;
        $tramite->FechaCaptura = new \DateTime();
        $tramite->UserCreate = $request->user()->id;
        //dd($tramite);

        if ($tramite->save()) {
          return response()->json([
      			'success' => true,
      			'message' => 'Se registro con exíto',
            'lastTramite' => $tramite->id
      		],200);

        }else {
          return response()->json([
      			'success' => false,
      			'message' => 'Ocurrio un error inesperado'
      		],401);
        }
    }
public function Comprador(Request $request)
    {
      // dd($request->all());
        $tramite_comprador = new tramite_comprador;
        $tramite_comprador->idTipoCompraVende = $request->compravende;
        $tramite_comprador->idTramite = $request->idTramiteComprador;
        $tramite_comprador->Nombre = $request->Nombre;
        $tramite_comprador->Domicilio = $request->Domicilio;
        $tramite_comprador->CorreoElectronico = $request->CorreoElectronico;
        $tramite_comprador->TelCasa = $request->TelCasa;
        $tramite_comprador->TelOficina = $request->TelOficina;
        $tramite_comprador->Extension = $request->Extension;
        $tramite_comprador->Celular = $request->Celular;
        $tramite_comprador->idEstadoCivil = $request->EstadoCivil;
        $tramite_comprador->idRegimenPatrimonial = $request->RegimenPatrimonial;
        $tramite_comprador->idNacionalidad = $request->Nacionalidad;
        $tramite_comprador->LugarNacimiento = $request->LugarNacimiento;
        $tramite_comprador->FechaNacimiento = $request->FechaNacimiento;
        $tramite_comprador->CURP = $request->CURP;
        $tramite_comprador->RFC = $request->RFC;
        $tramite_comprador->idAdquirente = $request->Adquirente;
        $tramite_comprador->idTipoIdentificacion = $request->TipoIdentificacion;
        $tramite_comprador->NumeroIdentificacion = $request->NumIdentificacion;
        $tramite_comprador->AplicaPoderRepLegal = $request->AplicaPoderRepresentanteLegal;
        $tramite_comprador->AplicaObligadoSolidario = $request->AplicaObligadoSolidario;
        //$tramite_comprador->idTipoCompraVende = $request->idTipoCompraVende;
        //dd($tramite_comprador);

        if ($tramite_comprador->save()) {
          return response()->json([
      			'success' => true,
      			'message' => 'Se registro con exíto'
      		],200);

        }else {
          return response()->json([
      			'success' => false,
      			'message' => 'Ocurrio un error inesperado'
      		],401);
        }
    }

    public function Vendedor(Request $request)
    {
        $tramite_comprador = new tramite_comprador;
//$tramite_comprador->idTramite = $request->idTramite;
        $tramite_comprador->idTipoCompraVende = $request->compravende;
        $tramite_comprador->Nombre = $request->Nombre;
        $tramite_comprador->Domicilio = $request->Domicilio;
        $tramite_comprador->CorreoElectronico = $request->CorreoElectronico;
        $tramite_comprador->TelCasa = $request->TelCasa;
        $tramite_comprador->TelOficina = $request->TelOficina;
        $tramite_comprador->Extension = $request->Extension;
        $tramite_comprador->Celular = $request->Celular;
        $tramite_comprador->idEstadoCivil = $request->EstadoCivil;
        $tramite_comprador->idRegimenPatrimonial = $request->RegimenPatrimonial;
        $tramite_comprador->idNacionalidad = $request->Nacionalidad;
        $tramite_comprador->LugarNacimiento = $request->LugarNacimiento;
        $tramite_comprador->FechaNacimiento = $request->FechaNacimiento;
        $tramite_comprador->CURP = $request->CURP;
        $tramite_comprador->RFC = $request->RFC;
        $tramite_comprador->idAdquirente = $request->Enajenante;
        $tramite_comprador->idTipoIdentificacion = $request->TipoIdentificacion;
        $tramite_comprador->NumeroIdentificacion = $request->NumIdentificacion;
        $tramite_comprador->AplicaPoderRepLegal = $request->AplicaPoderRepresentanteLegal;
        $tramite_comprador->AplicaObligadoSolidario = $request->AplicaCalculo;
        //$tramite_comprador->idTipoCompraVende = $request->idTipoCompraVende;
       //dd($tramite_comprador);

        if ($tramite_comprador->save()) {
          return response()->json([
      			'success' => true,
      			'message' => 'Se registro con exíto'
      		],200);

        }else {
          return response()->json([
      			'success' => false,
      			'message' => 'Ocurrio un error inesperado'
      		],401);
        }
    }

     public function ContactoBanco(Request $request)
    {
        $ejecutivo_desarrollador = new ejecutivo_desarrollador;
      //  $ejecutivo_desarrollador->idTipoEjecutivoDesarrollador = $request->idTipoEjecutivoDesarrollador;

        $ejecutivo_desarrollador->NombreEjecutivo = $request->NombreEjecutivo;
        $ejecutivo_desarrollador->Correo = $request->CorreoElectronico;
        $ejecutivo_desarrollador->Telefono = $request->Telefono;
        $ejecutivo_desarrollador->Ext = $request->Extension;
        $ejecutivo_desarrollador->Celular = $request->Celular;
        $ejecutivo_desarrollador->FechaCreate = new \DateTime();

      // dd($ejecutivo_desarrollador);

        if ($ejecutivo_desarrollador->save()) {
          return response()->json([
      			'success' => true,
      			'message' => 'Se registro con exíto'
      		],200);

        }else {
          return response()->json([
      			'success' => false,
      			'message' => 'Ocurrio un error inesperado'
      		],401);
        }
    }

     public function ContactoDesarrollador(Request $request)
    {
        $ejecutivo_desarrollador = new ejecutivo_desarrollador;
      //  $ejecutivo_desarrollador->idTipoEjecutivoDesarrollador = $request->idTipoEjecutivoDesarrollador;

        $ejecutivo_desarrollador->NombreEjecutivo = $request->NombreEjecutivo;
        $ejecutivo_desarrollador->Correo = $request->CorreoElectronico;
        $ejecutivo_desarrollador->Telefono = $request->Telefono;
        $ejecutivo_desarrollador->Ext = $request->Extension;
        $ejecutivo_desarrollador->Celular = $request->Celular;
        $ejecutivo_desarrollador->FechaCreate = new \DateTime();

      // dd($ejecutivo_desarrollador);

        if ($ejecutivo_desarrollador->save()) {
          return response()->json([
      			'success' => true,
      			'message' => 'Se registro con exíto'
      		],200);

        }else {
          return response()->json([
      			'success' => false,
      			'message' => 'Ocurrio un error inesperado'
      		],401);
        }
    }
    public function edit(Request $request) {
      $Tramite = new tramite;
      // $Tramite_comprador = new tramite_comprador;

      $toEdit = $Tramite->toUpdate($request);
      // $toEditC = $Tramite_comprador->toUpdate($request);

      $modu = modulos::all();
      $tra = tramite::all();
      $ent = entidad::all();
      $ban = bancos::all();
      $regimenPat = regimen_patrimonial::all();
      $user = user::all();

      
      $tipoOpe = tipo_operacion::all();
      $desa = desarrollador::all();
      $tipoioden = tipo_iodentificacion::all();

      // dd($toEdit);
      return view('expedientes.expediente_edit',[
        "TOEDIT" => $toEdit,
        'modulos' => $modu,
        'tramite' => $tra,
        'entidad' => $ent,
        'tipo_operacion' => $tipoOpe,
        'desarrollador' => $desa,
        'bancos' => $ban,
        'regimen_patrimonial' => $regimenPat,
        'tipo_iodentificacion' => $tipoioden,
        'user' => $user,
        

      ]);
    }
 public function savereplace(Request $request){
        $toUPDATE=[
         //'Folio' => $request->Folio,
        'Calle' => $request->Calle,
        'NumExt' => $request->NumExt,
        'NumInt' => $request->NumInt,
        'Colonia' => $request->Colonia,
        'CP' => $request->CP,
        'ValorCatastral' => $request->ValorCatastral,
        'ValorAvaluo' => $request->ValorAvaluo,
        'ValorVenta' => $request->ValorVenta,
        'idDesarrollador' => $request->Desarrollador,
        'idBanco' => $request->Banco,
        'municipio' => $request->municipio,
        'FechaCaptura' => new \DateTime(),
        'UserCreate' => $request->user()->id,
        'Nombre' => $request->Nombre,
        'Domicilio' => $request->Domicilio,
        'CorreoElectronico' => $request->CorreoElectronico,
        'TelCasa' => $request->TelCasa,
        'TelOficina' => $request->TelOficina,
        'Extension' => $request->Extension,
        'Celular' => $request->Celular,
        'LugarNacimiento' => $request->LugarNacimiento,
        'FechaNacimiento' => $request->FechaNacimiento,
        'CURP' => $request->CURP,
        'RFC' => $request->RFC,
        'idTipoIdentificacion' => $request->TipoIdentificacion,
        'NumeroIdentificacion' => $request->NumIdentificacion,

        ];
        if(DB::table('tramite')->join('tramite_comprador', 'tramite_comprador.idTramite', '=', 'tramite.id')->where('tramite.id',$request->idTramite)->update($toUPDATE)){
            return response()->json([
                'success' => 'Se guardo correctamente'
            ]);
        }else{
            return response()->json([
                'success' => 'No hay nada que actualizar'
            ]);
        }
    }
}
