<?php
namespace App\Http\Controllers\Avances;

use App\Http\Requests;
use App\Http\Controllers\Controller;

use Request;
use Response;
use Illuminate\Support\Facades\Input;
use DB; 
use DNS1D;

use App\tramite;
use App\Avances\TramitesAvances;
use App\Avances\TramitesAvancesFirmas;
use App\Avances\TramitesAvancesFrimantes;
use App\Avances\TramitesRubros;
use App\Avances\RubrosCategorias;
use App\Avances\Rubros;

/**
* Controlador Avances
* 
* @package    Plataforma API
* @subpackage Controlador
* @author     Eliecer Ramirez Esquinca <ramirez.esquinca@gmail.com>
* @created    2018-05-10
*
* Controlador `Avance`: Manejo de avances para los tramites x tipoo de documento
*
*/
class AvanceController extends Controller {
	/**
	 * Muestra una lista de los recurso según los parametros a procesar en la petición.
	 *
	 * <h3>Lista de parametros Request:</h3>
	 * <Ul>Paginación
	 * <Li> <code>$pagina</code> numero del puntero(offset) para la sentencia limit </ li>
	 * <Li> <code>$limite</code> numero de filas a mostrar por página</ li>	 
	 * </Ul>
	 * <Ul>Busqueda
	 * <Li> <code>$valor</code> string con el valor para hacer la busqueda</ li>
	 * <Li> <code>$order</code> campo de la base de datos por la que se debe ordenar la información. Por Defaul es ASC, pero si se antepone el signo - es de manera DESC</ li>	 
	 * </Ul>
	 *
	 * Ejemplo ordenamiento con respecto a id:
	 * <code>
	 * http://url?pagina=1&limite=5&order=id ASC 
	 * </code>
	 * <code>
	 * http://url?pagina=1&limite=5&order=-id DESC
	 * </code>
	 *
	 * Todo Los parametros son opcionales, pero si existe pagina debe de existir tambien limite
	 * @return Response 
	 * <code style="color:green"> Respuesta Ok json(array("status": 200, "messages": "Operación realizada con exito", "data": array(resultado)),status) </code>
	 * <code> Respuesta Error json(array("status": 404, "messages": "No hay resultados"),status) </code>
	 */
	public function index(){
		$datos = Request::all();
		// Si existe el paarametro pagina en la url devolver las filas según sea el caso
		// si no existe parametros en la url devolver todos las filas de la tabla correspondiente
		// esta opción es para devolver todos los datos cuando la tabla es de tipo catálogo
		if(array_key_exists("pagina", $datos)){
			$pagina = $datos["pagina"];
			if(isset($datos["order"])){
				$order = $datos["order"];
				if(strpos(" ".$order,"-"))
					$orden = "desc";
				else
					$orden = "asc";
				$order=str_replace("-", "", $order); 
			}
			else{
				$order = "id"; $orden = "desc";
			}
			
			if($pagina == 0){
				$pagina = 1;
			}
			if($pagina == 1)
				$datos["limite"] = $datos["limite"] - 1;
			// si existe buscar se realiza esta linea para devolver las filas que en el campo que coincidan con el valor que el usuario escribio
			// si no existe buscar devolver las filas con el limite y la pagina correspondiente a la paginación
			if(array_key_exists("buscar", $datos)){
				$columna = $datos["columna"];
				$valor   = $datos["valor"];
				$data = TramitesAvances::with("Usuarios", "Personas", "TramitesAvancesArticulos", "TiposTramitesAvances", "TramitesAvancesFirmas", "Devoluciones")
				->orderBy($order, $orden);
				
				$search = trim($valor);
				$keyword = $search;
				$data = $data->whereNested(function($query) use ($keyword){	
						$query->Where("id", "LIKE", "%".$keyword."%")
						->orWhere("codigo", "LIKE", "%".$keyword."%")
						->orWhere("folio", "LIKE", "%".$keyword."%")
						->orWhere("created_at", "LIKE", "%".$keyword."%"); 
				});
				
				$total = $data->get();
				$data = $data->skip($pagina-1)->take($datos["limite"])->get();
			}
			else{
				$data = TramitesAvances::with("Usuarios", "Personas", "TramitesAvancesArticulos", "TiposTramitesAvances", "TramitesAvancesFirmas", "Devoluciones")			
				->skip($pagina-1)->take($datos["limite"])->orderBy($order, $orden)->get();
				$total =  TramitesAvances::all();
			}
			
		}
		else{
			$data = TramitesAvances::with("Usuarios", "Personas", "TramitesAvancesArticulos", "TiposTramitesAvances", "TramitesAvancesFirmas", "Devoluciones")
			->get();
			$total = $data;
		}

		if(!$data){
			return Response::json(array("status" => 204, "messages" => "No hay resultados"),204);
		} 
		else {
			return Response::json(array("status" => 200, "messages" => "Operación realizada con exito", "data" => $data, "total" => count($total)), 200);			
		}
	}

	/**
	 * Crear un nuevo registro en la base de datos con los datos enviados
	 *
	 * <h4>Request</h4>
	 * Recibe un input request tipo json de los datos a almacenar en la tabla correspondiente
	 *
	 * @return Response
	 * <code style="color:green"> Respuesta Ok json(array("status": 201, "messages": "Creado", "data": array(resultado)),status) </code>
	 * <code> Respuesta Error json(array("status": 500, "messages": "Error interno del servidor"),status) </code>
	 */
	public function store(){
		$datos = (object) Input::json()->all();	
		$success = false;

        DB::beginTransaction();
        try{
        	$data = tramite::find($datos->id);
            $success = $this->campos($datos, $data);

        } catch (\Exception $e) {
            DB::rollback();
            return Response::json(["status" => 500, 'error' => $e->getMessage()], 500);
        } 
        if ($success){
            DB::commit();
            return Response::json(array("status" => 201,"messages" => "Creado","data" => $data), 201);
        } 
        else{
            DB::rollback();
            return Response::json(array("status" => 409,"messages" => "Conflicto"), 200);
        }
		
	}

	
	/**
	 * Actualizar el  registro especificado en el la base de datos
	 *
	 * <h4>Request</h4>
	 * Recibe un Input Request con el json de los datos
	 *
	 * @param  int  $id que corresponde al identificador del dato a actualizar 	 
	 * @return Response
	 * <code style="color:green"> Respuesta Ok json(array("status": 200, "messages": "Operación realizada con exito", "data": array(resultado)),status) </code>
	 * <code> Respuesta Error json(array("status": 304, "messages": "No modificado"),status) </code>
	 */
	public function update($id){
		$datos = (object) Input::json()->all();		
		$success = false;
        
        DB::beginTransaction();
        try{
        	$data = tramite::find($id);

            if(!$data){
                return Response::json(['error' => "No se encuentra el recurso que esta buscando."], HttpResponse::HTTP_NOT_FOUND);
            }
            
            $success = $this->campos($datos, $data);

        } catch (\Exception $e) {
            DB::rollback();
            return Response::json(["status" => 500, 'error' => $e->getMessage()], 500);
        } 
        if($success){
			DB::commit();
			return Response::json(array("status" => 200, "messages" => "Operación realizada con exito", "data" => $data), 200);
		} 
		else {
			DB::rollback();
			return Response::json(array("status" => 304, "messages" => "No modificado"),200);
		}
	}

	public function campos($datos, $data){				

        $data->LastUpdate = date("Y-m-d");        
        if ($data->save()) {	

        	if(property_exists($datos, "rubros_categorias")){

        		$rubros_categorias = array_filter($datos->rubros_categorias, function($v){return $v !== null;});
      
        		foreach ($rubros_categorias as $key => $value) {
        			$value = (object) $value;
        			if($value != null){
        				
            			if(property_exists($value, "rubros")) {
            				$rubros = array_filter($value->rubros, function($v){return $v !== null;});
            				foreach ($rubros as $k1 => $v1) {
			        			$v1 = (object) $v1;
			        			if($v1 != null){	
			        				if($v1->activado){	        			
				        				$avance = TramitesAvances::where("tramites_id", $data->id)->where("id", $v1->avances_id)->first();

				        				if(!$avance){
				        					$avance = TramitesAvances::where("tramites_id", $data->id)->where("rubros_id", $v1->id)->first();
				        					if(!$avance)
				        						$avance = new TramitesAvances;
				        				}

				        				$avance->tramites_id 	= $data->id;
				        				$avance->rubros_id   	= $v1->id;
				        				$avance->activado    	= $v1->activado;
				        				$avance->fecha_inicio  	= date("Y-m-d", strtotime($v1->fecha_inicio));
				        				if($v1->fecha_entrega != '')
				        				$avance->fecha_entrega  = date("Y-m-d", strtotime($v1->fecha_entrega));
				        				$avance->fecha_termino  = date("Y-m-d", strtotime($v1->fecha_termino));
				        				$avance->comentario   	= $v1->comentario;

				        				$avance->save();
				        			}
			        			}
			        		}
            			}   
            		}
        		}
        	}
        	
        	if(property_exists($datos, "tramites_avances_firmas")){

				$tramites_avances_firmas = array_filter($datos->tramites_avances_firmas, function($v){return $v !== null;});
				TramitesAvancesFirmas::where("tramites_id", $data->id)->delete();
				foreach ($tramites_avances_firmas as $key => $value) {
        			$value = (object) $value;
        			if($value != null){		        			
        				DB::update("update tramites_avances_firmas set deleted_at = null where tramites_id = $data->id ");
        				$firma = TramitesAvancesFirmas::where("tramites_id", $data->id)->first();

        				if(!$firma)
        					$firma = new TramitesAvancesFirmas;

        				$firma->numero_escritura 	= $data->numero_escritura;
        				$firma->libro 				= $value->libro;
        				$firma->folio 				= $value->folio;
        				$firma->fecha_pase 			= date("Y-m-d", strtotime($value->fecha_pase));
        				$firma->fecha_firma 		= date("Y-m-d", strtotime($value->fecha_firma));
        				$firma->fecha_limite_firma 	= date("Y-m-d", strtotime($value->fecha_limite_firma));
        				$firma->fecha_autorizacion	= date("Y-m-d", strtotime($value->fecha_autorizacion));
        				$firma->numero_firmantes 	= $value->numero_firmantes;

        				if($firma->save()){
        					if(property_exists($value, "tramites_avances_firmantes")){
	        					$tramites_avances_firmantes = array_filter($value->tramites_avances_firmantes, function($v){return $v !== null;});
								TramitesAvancesFirmantes::where("tramites_avances_firmas_id", $firma->id)->delete();
								foreach ($tramites_avances_firmantes as $k1 => $v1) {
				        			$v1 = (object) $v1;
				        			if($v1 != null){

				        				DB::update("update tramites_avances_firmantes set deleted_at = null where tramites_avances_firmas_id = $firma->id and nombre = '$v1->nombre'");
				        				$firmantes = TramitesAvancesFirmantes::where("tramites_id", $data->id)->where("nombre", $v1->nombre)->first();

				        				if(!$firmantes)
				        					$firmantes = new TramitesAvancesFirmas;

				        				$firmantes->nombre = $v1->nombre;
				        				$firmantes->correo = $v1->correo;
				        				$firmantes->lugar_firma = $v1->lugar_firma;
				        				$firmantes->fecha_firma = date("Y-m-d", strtotime($v1->fecha_firma));

				        				$firmantes->save();
				        			}
				        		}	
				        	}        			
        				}
        			}
        		}
			}
        	
			$success = true;
		}  
		return $success;     						
	}


	/**
	 * Devuelve la información del registro especificado.
	 *
	 * @param  int  $id que corresponde al identificador del recurso a mostrar
	 *
	 * @return Response
	 * <code style="color:green"> Respuesta Ok json(array("status": 200, "messages": "Operación realizada con exito", "data": array(resultado)),status) </code>
	 * <code> Respuesta Error json(array("status": 404, "messages": "No hay resultados"),status) </code>
	 */
	public function show($id){
		$data = tramite::with("TramitesAvancesFirmas")->find($id);			
		
		if(!$data){
			return Response::json(array("status"=> 404,"messages" => "No hay resultados"),404);
		} 
		else {				
			$categorias = RubrosCategorias::with("Rubros")->get();
			foreach ($categorias as $k => $v) {
				foreach ($v->rubros as $key => $value) {
					$tr = TramitesRubros::where("tramites_tipos_id", $id)->where("rubros_id", $value->id)->first();
					$value->pertenece = 0;					
					if($tr){
						$value->pertenece = 1;
					}

					$valor = TramitesAvances::where("tramites_id", $id)->where("rubros_id", $value->id)->first();
					$value->fecha_inicio = date('Y-m-d');
					$value->fecha_entrega = '';
					$value->fecha_termino = date('Y-m-d');
					$value->activado = 0;
					$value->comentario = '';
					$value->avances_id = '';
					if($valor){
						
						$value->fecha_inicio = $valor->fecha_inicio;
						$value->fecha_entrega = $valor->fecha_entrega;
						$value->fecha_termino = $valor->fecha_termino;
						$value->activado = $valor->activado;
						$value->comentario = $valor->comentario;
						$value->avances_id = $valor->id;
					}
				}				
			}
			$data->rubros_categorias = $categorias;
			return Response::json(array("status" => 200, "messages" => "Operación realizada con exito", "data" => $data), 200);
		}
	}
	
	/**
	 * Elimine el registro especificado del la base de datos (softdelete).
	 *
	 * @param  int  $id que corresponde al identificador del dato a eliminar
	 *
	 * @return Response
	 * <code style="color:green"> Respuesta Ok json(array("status": 200, "messages": "Operación realizada con exito", "data": array(resultado)),status) </code>
	 * <code> Respuesta Error json(array("status": 500, "messages": "Error interno del servidor"),status) </code>
	 */
	public function destroy($id){
		$success = false;
        DB::beginTransaction();
        try {
			$data = TramitesAvances::where("tramites_id", $id)->delete();
    		$firmas = TramitesAvancesFirmas::where("tramites_id", $id)->get();
    		foreach ($firmas as $key => $value) {
    			TramitesAvancesFirmantes::where("tramites_avances_firmas_id", $value->id)->delete();
    			$value->delete(); 
    		}  		
			
			$success=true;
		} 
		catch (\Exception $e) {
			return Response::json($e->getMessage(), 500);
        }
        if ($success){
			DB::commit();
			return Response::json(array("status" => 200,"messages" => "Operación realizada con exito", "data" => $data), 200);
		} 
		else {
			DB::rollback();
			return Response::json(array("status" => 404, "messages" => "No se encontro el registro"), 404);
		}
	}

	public function init($id){
		return view('avances.avances',["id" => $id]);
	}
}