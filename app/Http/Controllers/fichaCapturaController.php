<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use DB;
use App\tramite;
use Illuminate\Pagination\LengthAwarePaginator;

class fichaCapturaController extends Controller
{
    public function init(Request $request){
        $Tramite = new tramite;
        $toEdit = $Tramite->toUpdate($request);
        return view('fichasCaptura.fichas_captura',[
            "TOEDIT" => $toEdit,
        ]);
    }
}
