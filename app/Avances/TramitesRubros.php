<?php namespace App\Avances;

use App\BaseModel;

/**
* Modelo Articulos
* 
* @package    Plataforma API
* @subpackage Controlador
* @author     Eliecer Ramirez Esquinca <ramirez.esquinca@gmail.com>
* @created    2015-07-20
*
* Modelo `Articulos`: Manejo de los grupos de usuario
*
*/
class TramitesRubros extends BaseModel {

	public function TramitesTipo(){
        return $this->belongsTo('App\tramites_tipo','tramites_tipos_id', "id");
    }
}