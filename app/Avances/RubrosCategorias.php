<?php namespace App\Avances;

use App\BaseModel;

/**
* Modelo Articulos
* 
* @package    Plataforma API
* @subpackage Controlador
* @author     Eliecer Ramirez Esquinca <ramirez.esquinca@gmail.com>
* @created    2015-07-20
*
* Modelo `Articulos`: Manejo de los grupos de usuario
*
*/
class RubrosCategorias extends BaseModel {

	public function Rubros(){
        return $this->hasMany('App\Avances\Rubros','rubros_categorias_id')->orderBy("orden", "asc");
    }

}