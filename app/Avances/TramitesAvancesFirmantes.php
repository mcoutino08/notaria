<?php namespace App\Avances;

use App\BaseModel;

/**
* Modelo Articulos
* 
* @package    Plataforma API
* @subpackage Controlador
* @author     Eliecer Ramirez Esquinca <ramirez.esquinca@gmail.com>
* @created    2015-07-20
*
* Modelo `Articulos`: Manejo de los grupos de usuario
*
*/
class TramitesAvancesFirmantes extends BaseModel {

	public function TramitesAvancesFirmas(){
        return $this->belongsTo('App\Avances\TramitesAvancesFirmas','tramites_avances_id', "id");
    }
}