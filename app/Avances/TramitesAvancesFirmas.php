<?php namespace App\Avances;

use App\BaseModel;

/**
* Modelo Articulos
* 
* @package    Plataforma API
* @subpackage Controlador
* @author     Eliecer Ramirez Esquinca <ramirez.esquinca@gmail.com>
* @created    2015-07-20
*
* Modelo `Articulos`: Manejo de los grupos de usuario
*
*/
class TramitesAvancesFirmas extends BaseModel {

	public function TramitesAvancesFirmas(){
        return $this->hasMany('App\Avances\TramitesAvancesFirmas','tramites_avances_firmas_id');
    }

    public function TramitesAvances(){
		return $this->belongsTo('App\Avances\TramitesAvances','tramites_avances_id','id');
    }
}