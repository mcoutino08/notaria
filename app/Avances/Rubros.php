<?php namespace App\Avances;

use App\BaseModel;

/**
* Modelo Articulos
* 
* @package    Plataforma API
* @subpackage Controlador
* @author     Eliecer Ramirez Esquinca <ramirez.esquinca@gmail.com>
* @created    2015-07-20
*
* Modelo `Articulos`: Manejo de los grupos de usuario
*
*/
class Rubros extends BaseModel {

	public function RubrosCategorias(){
        return $this->belongsTo('App\Avances\RubrosCategorias','rubros_categorias_id', "id");
    }
}