<?php namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

use Session;

abstract class BaseModel extends Model {

    use SoftDeletes;

    public static function boot(){
        parent::boot();

        static::creating(function($item){
            if(\Auth::user()){
                $item->creado_por = \Auth::user()->id;
            }            
        });

        static::updating(function($item){
            if(\Auth::user()){
                $item->modificado_por = \Auth::user()->id;
            }
        });

        static::deleting(function($item){
            if(\Auth::user()){
                $item->borrado_por = \Auth::user()->id;
            }
        });
    }
}