<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class notaria extends Model
{
    protected $table ='notaria';
    public $timestamps = false;
}
