<?php

namespace App;
use DB;
use App\tramites_tipo;
use App\entidad;
use App\tipo_inmueble;
use App\tipo_operacion;
use App\desarrollador;
use App\bancos;
use App\fondos;
use App\usuarios;

use Illuminate\Database\Eloquent\Model;

class tramite extends Model
{
    protected $table ='tramite';
    public $timestamps = false;
    public function tramites_tipo(){return $this->belongsTo('App\tramites_tipo');}
    public function entidad(){return $this->hasMany('App\entidad');}
    public function tipo_inmueble(){return $this->hasMany('App\tipo_inmueble');}
    public function tipo_operacion(){return $this->hasMany('App\tipo_operacion');}
    public function desarrollador(){return $this->hasMany('App\desarrollador');}
    public function bancos(){return $this->hasMany('App\bancos');}
    public function fondos(){return $this->hasMany('App\fondos');}
    public function usuarios(){return $this->hasMany('App\usuarios');}

    public function toUpdate($request){

$selectRaw = "tramite_comprador.idTipoCompraVende, tramitetipocompravende.TipoCompraVende, tramite.idModulos, modulos.Modulo,tramite.id, tramite.Calle, tramite.NumExt, tramite.NumInt, tramite.Colonia, tramite.CP, tramite.municipio, tramite.ValorCatastral, tramite.ValorAvaluo, tramite.ValorVenta, tipo_operacion.TipoOperacion, tramite.idTipoOperacion, desarrollador.Desarrollador, tramite.idDesarrollador, bancos.Banco, tramite.idBanco, tramite.FechaCaptura, tramite_comprador.idTramite, tramite_comprador.Nombre, tramite_comprador.Domicilio, tramite_comprador.CorreoElectronico, tramite_comprador.TelCasa, tramite_comprador.TelOficina, tramite_comprador.Extension, tramite_comprador.Celular, tramite_comprador.LugarNacimiento, tramite_comprador.FechaNacimiento, tramite_comprador.CURP, tramite_comprador.RFC, tipo_iodentificacion.TipoIdentificacion, tramite_comprador.idTipoIdentificacion, tramite_comprador.NumeroIdentificacion";
$Query = DB::table('tramite')->selectRaw($selectRaw);
        # join to tipo_operacion
        $Query->join('tipo_operacion', function($join){
            $join->on('tramite.idTipoOperacion','=','tipo_operacion.id');
        });
        # join to desarrollador
        $Query->join('desarrollador', function($join){
            $join->on('tramite.idDesarrollador','=','desarrollador.id');
        });
        # join to bancos
        $Query->join('bancos', function($join){
            $join->on('tramite.idBanco','=','bancos.id');
        });
        # join to tramite_comprador
        $Query->join('tramite_comprador', function($join){
            $join->on('tramite_comprador.idTramite','=','tramite.id');
        });
        # join to tipo_iodentificacion
        $Query->join('tipo_iodentificacion', function($join){
            $join->on('tramite_comprador.idTipoIdentificacion','=','tipo_iodentificacion.id');
        });
        # join to modulos
        $Query->join('modulos', function($join){
            $join->on('tramite.idModulos','=','modulos.id');
        });
         # join to tramitetipocompravende
        $Query->join('tramitetipocompravende', function($join){
            $join->on('tramite_comprador.idTipoCompraVende','=','tramitetipocompravende.idTipo');
        });
        if ($request->has('id')) {
         $Query->where('tramite.id',$request->id);
        }
        return $Query->get();
    }
}