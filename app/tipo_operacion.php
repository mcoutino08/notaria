<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class tipo_operacion extends Model
{
    protected $table ='tipo_operacion';
    public $timestamps = false;
}
