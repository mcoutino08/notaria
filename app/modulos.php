<?php

namespace App;
use DB;
use Illuminate\Database\Eloquent\Model;

class modulos extends Model
{
    protected $table ='modulos';
    public $timestamps = false; 
    protected $fillable = [
        'Modulo', 
        'idNotaria'
    ];
    public function notaria(){return $this->hasMany('App\notaria');}
}
