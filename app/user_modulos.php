<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class user_modulos extends Model
{
    protected $table ='user_modulos';
    public $timestamps = false; 
    protected $fillable = [
        'idUsuario', 
        'idModulo'
    ];
}
