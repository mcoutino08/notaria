@extends('layouts.app')

@section('content')

<div class="container">
  <nav class="navbar navbar-default">
    <div class="container-fluid">
      <div class="navbar-header">

      </div>
      <div id="navbar1" class="navbar-collapse collapse">
        <ul class="nav navbar-nav">
          <li class="active"><a href="#">Inicio</a></li>
          <li class="dropdown">
            <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false">Reportes <span class="caret"></span></a>
            <ul class="dropdown-menu" role="menu">
              <li><a href="#">General</a></li>
              <li><a href="#">Por expediente</a></li>
              <li><a href="#">Indicadores</a></li>
            </ul>
          </li>
           <li class="dropdown">
            <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false">Fichas <span class="caret"></span></a>
            <ul class="dropdown-menu" role="menu">
              <li><a href="#">Bancos</a></li>
              <li><a href="#">Desarrolladores</a></li>
              <li><a href="#">Particulares</a></li>
              <li><a href="#">Otros</a></li>
            </ul>
          </li>
          <li class=""><a href="#">Expedientes</a></li>
           <li class=""><a href="#">Archivos</a></li>
        </ul>
      </div>
    </div>
  </nav>
</div>
@endsection
