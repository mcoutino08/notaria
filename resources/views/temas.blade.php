@extends('layouts.metronic')
@section('Activo')
<a href="/cat_temas">Temas
    <span class="selected"> </span>
</a>
@endsection
@section('content')
                    <div class="page-bar">
                        <ul class="page-breadcrumb">
                            <li>
                                <a href="index.php">Inicio</a>
                                <i class="fa fa-circle"></i>
                            </li>
                            <li>
                                <span>Catalogo</span>
                                <i class="fa fa-circle"></i>
                            </li>
                            <li>
                                <span>Temas</span>
                            </li>
                        </ul>
                    </div>
                    <h3 class="page-title bold"><i class="icon-notebook"></i> Catalogo
                        <small>Temas</small>
                    </h3>
                    <div class="row">
                            <div class="col-md-12">
                                <!-- BEGIN EXAMPLE TABLE PORTLET-->
                                <div class="portlet light bordered">
                                    <div class="portlet-title">
                                        <div class="caption font-dark">
                                            <i class="icon-settings font-dark"></i>
                                            <span class="caption-subject bold uppercase">Lista de Temas</span>
                                        </div>
                                        <div class="actions">
                                            <a class="btn sbold blue btn-sm"  onclick="f_popup_info(0,'/temas-add','Agregar Nuevo Tema');">Nuevo Tema <i class="fa fa-plus"></i></a>
                                            
                                        </div>
                                    </div>
                                    
                                    <div id="datosTabla" class="portlet-body table-both-scroll">




                                    </div>
                                </div>
                                <!-- END EXAMPLE TABLE PORTLET-->
                            </div>
                    </div>
@section('content_modals')
        <div class="modal fade draggable-modal" id="ModalInfo" tabindex="-1" role="basic" aria-hidden="true" data-backdrop="static" data-keyboard="false">
            <div class="modal-dialog modal-lg">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
                        <h4 class="modal-title" id="ModalTitleInfo">Formulario</h4>
                    </div>
                    <div id="ModalBody" class="modal-body"></div>
                </div>
                <!-- /.modal-content -->
            </div>
            <!-- /.modal-dialog -->
        </div>
@stop
@stop
@section('content_js')
<script src="{{ asset('js/jquery.validate.js') }}"></script>
<script src="{{ asset('js/jquery.forupm.js') }}"></script>
<script src="{{ asset('js/jquery_temas.js') }}"></script>
@stop