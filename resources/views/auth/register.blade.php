@extends('layouts.app')

@section('content')
<div class="container">
<div class="portlet box yellow">

    <div class="portlet-body">
        <div class="tabbable-custom ">

            <div class="tab-content">
                <div class="tab-pane active" id="DatosPersonales">

                    <div class="portlet-title">
                        <div class="caption">
                            <span class="caption-subject font-green bold uppercase">
                                <h4>
                                    <center>Datos Personales</center>
                                </h4>
                            </span>
                            <br>
                        </div>
                    </div>
                    <!-- ______________________________________________________________________________- -->


    <div class="row">
        <div class="col-md-8 col-md-offset-2">
            <div class="panel panel-default">
                <div class="panel-heading">Registro</div>

                <div class="panel-body">
                    <form class="form-horizontal" method="POST" action="{{ route('register') }}">
                        {{ csrf_field() }}

                        <div class="form-group{{ $errors->has('name') ? ' has-error' : '' }}">
                            <label for="name" class="col-md-4 control-label">Nombre</label>

                            <div class="col-md-6">
                                <input id="name" type="text" class="form-control" name="name" value="{{ old('name') }}" required autofocus>

                                @if ($errors->has('name'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('name') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>

                        <div class="form-group{{ $errors->has('email') ? ' has-error' : '' }}">
                            <label for="email" class="col-md-4 control-label">Correo</label>

                            <div class="col-md-6">
                                <input id="email" type="email" class="form-control" name="email" value="{{ old('email') }}" required>

                                @if ($errors->has('email'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('email') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>

                         <div class="form-group{{ $errors->has('Celular') ? ' has-error' : '' }}">
                            <label for="Celular" class="col-md-4 control-label">Num. Celular</label>

                            <div class="col-md-6">
                                <input id="Celular" type="Celular" class="form-control" name="Celular" value="{{ old('Celular') }}" required>

                                @if ($errors->has('Celular'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('Celular') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>

                        <div class="form-group{{ $errors->has('idNotaria') ? ' has-error' : '' }}">
                            <label for="name" class="col-md-4 control-label">Notaria</label>

                            <div class="col-md-6">
                              <select id="SelectNotaria" class="form-control" name="idNotaria" value="{{ old('idNotaria') }}" required autofocus>
                                <option value="" disabled selected>Notarias</option>
                                <option value=""></option>
                                <option value=""></option>
                              </select>

                                @if ($errors->has('idNotaria'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('idNotaria') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>
<div class="row">
    <div class="col-md-4">
                         <center>
                <div class="form-group ">
                <label for="">Notaria 15</label>
                <div class="mt-checkbox-list">
                    <label  class="mt-checkbox">&nbsp;&nbsp;
                    <input name="1"  type="checkbox">15-A
                        <span></span>
                    </label>
                    <label class="mt-checkbox">&nbsp;&nbsp;
                        <input name="2" type="checkbox"> 15-B
                        <span></span>
                    </label>
                    <label class="mt-checkbox mt-checkbox">&nbsp;&nbsp;
                        <input name="3" type="checkbox"> 15-C
                        <span></span>
                    </label>
                         <label class="mt-checkbox mt-checkbox">&nbsp;&nbsp;
                        <input name="3" type="checkbox"> 15-D
                        <span></span>
                    </label>
                                        <label class="mt-checkbox mt-checkbox">&nbsp;&nbsp;
                        <input name="4" type="checkbox"> 15-E
                        <span></span>
                    </label>
                                        <label class="mt-checkbox mt-checkbox">&nbsp;&nbsp;
                        <input name="5" type="checkbox"> 15-F
                        <span></span>
                    </label>
                                        <label class="mt-checkbox mt-checkbox">&nbsp;&nbsp;
                        <input name="6" type="checkbox"> 15-G
                        <span></span>
                    </label>
                </div>
                                
            </div>                  
            </center>
    </div>
        <div class="col-md-4">
            <center>
                  <div class="form-group">
                <label for="">Notaria 182</label>
                <div class="mt-checkbox-list">
                    <label class="mt-checkbox">&nbsp;&nbsp;
                        <input name="7" type="checkbox">182-A
                        <span></span>
                    </label>
                    <label class="mt-checkbox">&nbsp;&nbsp;
                        <input name="8" type="checkbox"> 182-B
                        <span></span>
                    </label>
                    <label class="mt-checkbox mt-checkbox">&nbsp;&nbsp;
                        <input name="9" type="checkbox"> 182-C
                        <span></span>
                    </label>
                                        <label class="mt-checkbox mt-checkbox">&nbsp;&nbsp;
                        <input name="10" type="checkbox"> 182-D
                        <span></span>
                    </label>
                </div>
            </div>
            </center>
        </div>
        <div class="col-md-4">
                <center>
                  <div class="form-group">
                <label for="">Notaria 95</label>
                <div class="mt-checkbox-list">
                    <label class="mt-checkbox">
                        <input name="11" type="checkbox">95-A
                        <span></span>
                    </label>
                    <label class="mt-checkbox">
                        <input name="12" type="checkbox"> 95-B
                        <span></span>
                    </label>
                    <label class="mt-checkbox mt-checkbox">
                        <input name="13" type="checkbox" > 95-C
                        <span></span>
                    </label>
                           <label class="mt-checkbox mt-checkbox">
                        <input name="14" type="checkbox" > 95-E
                        <span></span>
                    </label>
                </div>
            </div>
            </center>
        </div>
</div>

<div class="row">
    
        <div class="col-md-4">
            <center>
                  <div class="form-group">
                <label for="">Notaria 10</label>
                <div class="mt-checkbox-list">
                    <label class="mt-checkbox">&nbsp;&nbsp;
                        <input name="4" type="checkbox">10-A
                        <span></span>
                    </label>
                    <label class="mt-checkbox">&nbsp;&nbsp;
                        <input name="5" type="checkbox"> 10-B
                        <span></span>
                    </label>
                    <label class="mt-checkbox mt-checkbox">&nbsp;&nbsp;
                        <input name="6" type="checkbox"> 10-C
                        <span></span>
                    </label>
                                        <label class="mt-checkbox mt-checkbox">&nbsp;&nbsp;
                        <input name="6" type="checkbox"> 10-D
                        <span></span>
                    </label>
                </div>
            </div>
            </center>
        </div>
</div>


                        <div class="form-group{{ $errors->has('password') ? ' has-error' : '' }}">
                            <label for="password" class="col-md-4 control-label">Contraseña</label>

                            <div class="col-md-6">
                                <input id="password" type="Password" class="form-control" name="password" required>

                                @if ($errors->has('password'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('password') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>

                        <div class="form-group">
                            <label for="password-confirm" class="col-md-4 control-label">Confirmar Contraseña</label>

                            <div class="col-md-6">
                                <input id="password-confirm" type="password" class="form-control" name="password_confirmation" required>
                            </div>
                        </div>

                        

                        <div class="form-group">
                            <div class="col-md-6 col-md-offset-4">
                                <button type="submit" class="btn btn-primary">
                                    Register
                                </button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>

                    {{-- ________________________________________________________________________________ --}}
                </div>
                <!-- _____________________________________________________________________________ -->
                {{-- ------------------------------------------------------------------------------------------- --}}
            </div>
        </div>
    </div>
</div>

</div>
@endsection
@section('content_js')
  <script src="{{ asset('js/register.js') }}"></script>
@endsection
