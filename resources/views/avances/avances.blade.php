@extends('layouts.metronic_2') @section('Activo')
<a href="/avances">
    <span class="selected"> </span>
</a>
@endsection @section('content')
<form method="POST" action="submit" id="avances" ng-controller="FormularioCtrl" ng-init="ver('avances', '{{$id}}')">
<div class="portlet box yellow">
    <div class="portlet-title">
        <div class="caption" style="width: 96%;">
            <span style="float:left">
                <i class="fa fa-gift"> </i> DOCUMENTOS PARA INTEGRACIÓN DEL EXPEDIENTE
            </span>
            <span style="float:right">
                <i class="fa fa-folder"> </i> Expediente: 1
                <button type="button" class="btn btn-success" ng-click="guardar()" ng-disabled="cargando">
                    <i class="fa fa-save" ng-if="!cargando"> </i> 
                    <i class="fa fa-cog fa-spin" ng-if="cargando"> </i> 
                    Guardar
                </button>
            </span>
            </div>
        <div class="tools">
            <a href="javascript:;" class="collapse" data-original-title="" title=""> </a>
            <a href="#portlet-config" data-toggle="modal" class="config" data-original-title="" title=""> </a>
        </div>
    </div>
    <div class="portlet-body">
        <div class="tabbable-custom ">
            <ul class="nav nav-tabs ">
                <li ng-repeat="item in dato.rubros_categorias" class="<% clase[item.id] %>" id="t<%item.id%>">
                    <a href="#tab<%item.id%>" data-toggle="tab" aria-expanded="true" ng-click="cambiar_clase(item.id)"><% item.nombre %></a>
                </li>                
            </ul>

             <div class="tab-content">
                <div ng-repeat="item in dato.rubros_categorias" class="tab-pane <% clase[item.id] %>" id="tab<%item.id%>">
                    <table class="table table-bordered">
                        <thead>
                            <tr>
                                <th colspan="2" scope="col"><center>RUBRO </center></th>
                                <th scope="col">ACTIVAR</th>
                                <th scope="col">FECHA DE INICIO</th>
                                <th scope="col">FECHA DE ENTREGA</th>
                                <th scope="col">FECHA DE TÉRMINO</th>
                                <th scope="col">COMENTARIOS</th>
                            </tr>
                        </thead>
                        <tbody>
                            <tr ng-repeat="val in item.rubros">
                               <th colspan="2" scope="row" ><% val.nombre %></th>
                               <td>
                                    <div class="custom-control custom-checkbox" ng-if="val.pertenece == false">
                                        <input type="checkbox" class="custom-control-input" ng-checked="val.activado" ng-modelo="val.activado" ng-click="val.activado = !val.activado">
                                        <label class="custom-control-label" for="escrituraAcreditePropiedad"></label>
                                    </div>
                                </td>
                               <td><%val.fecha_inicio%></td>
                               <td>
                                    <div class="form-group">
                                        <input type="date" class="form-control" ng-disabled="!val.activado" ng-model="val.fecha_entrega">
                                    </div></td>
                               <td><%val.fecha_termino%></td>
                               <td>
                                   <div class="form-group">
                                        <input type="text" class="form-control" ng-disabled="!val.activado" ng-model="val.comentario">
                                    </div>
                               </td>
                            </tr>
                        </tbody>
                    </table>
                </div>
            </div>
            
        </div>
    </div>
</div>
</form>

@endsection 
