@extends('layouts.metronic_modals')
@section('content_popup_css')
<link href="{{ asset('css/jquery.minicolors.css') }}" rel="stylesheet">
@stop
@section('content_popup_modals')
            <!-- BEGIN FORM-->
            <form action="#" id="form1" name="form1" class="horizontal-form">
                <div class="form-body">
                    
                    <div class="row">
                        <div class="col-md-12">
                            <div class="form-group">
                                <label class="control-label">Tipo de Cobertura</label>
                                <div class="input-icon left tooltips" data-placement="top" data-original-title="Tipo Medio">
                                    <i class="fa fa-tasks"></i>
                                    <select class="form-control" id="idCobertura" name="idCobertura" placeholder="Tipo Cobertura">
                                        <option value="" disabled selected>Seleccione Cobertura</option>
                                        @forelse($tipocoberturas as $tipocobertura)
                                        <option value="{{$tipocobertura->id}}">{{$tipocobertura->Cobertura}}</option>
                                        @empty
                                        <p>No hay contenido que mostrar</p>
                                        @endforelse                                   
                                        </select>
                                </div>
                            </div>
                        </div>                                        
                    </div>
                    <div class="row">
                        <div class="col-md-12">
                            <div class="form-group">
                            <label class="control-label">Tema</label>
                            <div class="input-icon right">
                            <i class="fa"></i>
                            <input type="text" class="form-control" id="NombreTema" name="NombreTema" aria-required="true" aria-describedby="url-error" aria-invalid="true"> </div>
                            </div>
                        </div>                                        
                    </div>
                    
                    <div class="row">
                        <div class="col-md-12">
                            <div class="form-group">
                            <label class="control-label">Descripcion corta del tema</label>
                            <textarea id="DescripcionTema" name="DescripcionTema" rows="5" style="width: 99%" class="form-control"></textarea>
                            </div>
                        </div>                                        
                    </div>                                      
                </div>
                <div class="form-actions right">
                    <input type="hidden" name="MM_insert" id="MM_insert" value="form1">
                    <input type="hidden" name="Act" id="Act" value="create">
                    <button  type="button" data-dismiss="modal" class="btn default">Cancelar</button>
                    <button type="submit" id="btnSave" name="btnSave" class="btn blue">
                    <i class="fa fa-check"></i> Guardar</button>
                </div>
            </form>
            <!-- END FORM-->
@stop
@section('content_popup_js')
<script src="{{ asset('js/jquery.minicolors.min.js') }}"></script>
<script src="{{ asset('js/jquery_temas-add-edit.js') }}"></script>
@stop