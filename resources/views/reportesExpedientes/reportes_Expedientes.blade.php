@extends('layouts.metronic') @section('Activo')
<a href="/reportesExpedientes">
    <span class="selected"> </span>
</a>
@endsection @section('content')

<table class="table table-bordered">
<thead>
<tr>
    <th colspan="12"><center><h3>DOCUMENTOS PARA INTEGRACIÓN DEL EXPEDIENTE</h3></center></th>
</tr>
<tr>
    <th colspan="12">Expediente 1</th>
</tr>
<tr>   
    <th colspan="12"><center>VENDEDOR</center></th>
</tr>
</thead>
<tbody>
<tr>
        <th colspan="2" scope="col"><center>RUBRO </center></th>
        <th scope="col"></th>
        <th scope="col" >FECHA DE INICIO</th>
        <th scope="col">FECHA DE ENTREGA</th>
        <th scope="col">FECHA DE TÉRMINO</th>
        <th scope="col">COMENTARIOS</th>
        <th scope="col">SEMAFORO</th>
        <th scope="col">DIAS ANTES/RETRASO</th>
</tr>
<tr>
       <th colspan="2" scope="row" >Escritura que acredite su propiedad</th>
       <td>
        <div class="custom-control custom-checkbox">
        <input type="checkbox" class="custom-control-input" id="escrituraAcreditePropiedad">
        <label class="custom-control-label" for="escrituraAcreditePropiedad"></label>
        </div>
        </td>
       <td >15/01/2018</td>
       <td >22/01/2018</td>
       <td >22/01/2018</td>
       <td ><input type="text" class="form-control"> </input></td>
       <th bgcolor="LightGreen">OK</th>
       <td>0</td>
</tr>
<tr>
        <th colspan="2" scope="row">Precio del inmueble para cotización</th>
        <td>
         <div class="custom-control custom-checkbox">
         <input type="checkbox" class="custom-control-input" id="precioInmuebleCotizacion">
         <label class="custom-control-label" for="precioInmuebleCotizacion"></label>
         </div>
         </td>
        <td >15/01/2018</td>
        <td >22/01/2018</td>
        <td >22/01/2018</td>
        <td ><input type="text" class="form-control"> </input></td>
        <th bgcolor="LightGreen">OK</th>
        <td>0</td>
 </tr>
 <tr>
        <th colspan="2" scope="row">Carta de instrucción de cancelación de hipoteca o de reserva de dominio vigente </th>
        <td>
         <div class="custom-control custom-checkbox">
         <input type="checkbox" class="custom-control-input" id="cartaInstruccionCancelacion">
         <label class="custom-control-label" for="cartaInstruccionCancelacion"></label>
         </div>
         </td>
        <td >15/01/2018</td>
        <td >22/01/2018</td>
        <td >22/01/2018</td>
        <td ><input type="text" class="form-control"> </input></td>
        <th bgcolor="LightGreen">OK</th>
        <td>0</td>
 </tr>
 <tr>
        <th colspan="2" scope="row">Última boleta predial  </th>
        <td>
         <div class="custom-control custom-checkbox">
         <input type="checkbox" class="custom-control-input" id="ultimaBoletaPredial ">
         <label class="custom-control-label" for="ultimaBoletaPredial"></label>
         </div>
         </td>
        <td >15/01/2018</td>
        <td >22/01/2018</td>
        <td >22/01/2018</td>
        <td ><input type="text" class="form-control"> </input></td>
        <th bgcolor="LightGreen">OK</th>
        <td>0</td>
 </tr>
 <tr>
        <th colspan="2" scope="row">Última boleta de agua  </th>
        <td>
         <div class="custom-control custom-checkbox">
         <input type="checkbox" class="custom-control-input" id="ultimaBoletaAgua  ">
         <label class="custom-control-label" for="ultimaBoletaAgua"></label>
         </div>
         </td>
        <td >15/01/2018</td>
        <td >22/01/2018</td>
        <td >22/01/2018</td>
        <td ><input type="text" class="form-control"> </input></td>
        <th bgcolor="LightGreen">OK</th>
        <td>0</td>
 </tr>
 <tr>
        <th colspan="2" scope="row">Manifestación de construcción (en caso de compra de terreno) </th>
        <td>
         <div class="custom-control custom-checkbox">
         <input type="checkbox" class="custom-control-input" id="manifestacionConstruccion">
         <label class="custom-control-label" for="manifestacionConstruccion"></label>
         </div>
         </td>
        <td >15/01/2018</td>
        <td >22/01/2018</td>
        <td >22/01/2018</td>
        <td ><input type="text" class="form-control"> </input></td>
        <th bgcolor="LightGreen">OK</th>
        <td>0</td>
 </tr>
 <tr>
        <th colspan="2" scope="row">Aviso de terminación de obra (en caso de compra de terreno)</th>
        <td>
         <div class="custom-control custom-checkbox">
         <input type="checkbox" class="custom-control-input" id="avisoTerminacionObra">
         <label class="custom-control-label" for="avisoTerminacionObra"></label>
         </div>
         </td>
        <td >15/01/2018</td>
        <td >22/01/2018</td>
        <td >22/01/2018</td>
        <td ><input type="text" class="form-control"> </input></td>
        <th bgcolor="LightGreen">OK</th>
        <td>0</td>
 </tr>
 <tr>
        <th colspan="2" scope="row">Alineamiento y número oficial (en caso de compra de terreno)</th>
        <td>
         <div class="custom-control custom-checkbox">
         <input type="checkbox" class="custom-control-input" id="alineamientoNumeroOficial">
         <label class="custom-control-label" for="alineamientoNumeroOficial"></label>
         </div>
         </td>
        <td >15/01/2018</td>
        <td >22/01/2018</td>
        <td >22/01/2018</td>
        <td ><input type="text" class="form-control"> </input></td>
        <th bgcolor="LightGreen">OK</th>
        <td>0</td>
 </tr>
 <tr>
        <th colspan="2" scope="row">Regulación de construcción (por casa o depto. de terreno, edificios no nuevos, adecuaciones)</th>
        <td>
         <div class="custom-control custom-checkbox">
         <input type="checkbox" class="custom-control-input" id="regulacionConstruccion">
         <label class="custom-control-label" for="regulacionConstruccion"></label>
         </div>
         </td>
        <td >15/01/2018</td>
        <td >22/01/2018</td>
        <td >22/01/2018</td>
        <td ><input type="text" class="form-control"> </input></td>
        <th bgcolor="LightGreen">OK</th>
        <td>0</td>
 </tr>
 <tr>
        <th colspan="2" scope="row">Escritura de constitución del condominio</th>
        <td>
         <div class="custom-control custom-checkbox">
         <input type="checkbox" class="custom-control-input" id="escrituraConstitucionCondominio">
         <label class="custom-control-label" for="escrituraConstitucionCondominio"></label>
         </div>
         </td>
        <td >15/01/2018</td>
        <td >22/01/2018</td>
        <td >22/01/2018</td>
        <td ><input type="text" class="form-control"> </input></td>
        <th bgcolor="LightGreen">OK</th>
        <td>0</td>
 </tr>
 <tr>
        <th colspan="2" scope="row">Constancia de no adeudo por pago de mantenimiento</th>
        <td>
         <div class="custom-control custom-checkbox">
         <input type="checkbox" class="custom-control-input" id="ConstanciaNoAdeudoMantenimiento">
         <label class="custom-control-label" for="ConstanciaNoAdeudoMantenimiento"></label>
         </div>
         </td>
        <td >15/01/2018</td>
        <td >22/01/2018</td>
        <td >22/01/2018</td>
        <td ><input type="text" class="form-control"> </input></td>
        <th bgcolor="LightGreen">OK</th>
        <td>0</td>
        
 </tr>
 <tr>
        <th colspan="2" scope="row">Reglamento del condominio</th>
        <td>
         <div class="custom-control custom-checkbox">
         <input type="checkbox" class="custom-control-input" id="reglamentoCondominio">
         <label class="custom-control-label" for="reglamentoCondominio"></label>
         </div>
         </td>
        <td >15/01/2018</td>
        <td >22/01/2018</td>
        <td >22/01/2018</td>
        <td ><input type="text" class="form-control"> </input></td>
        <th bgcolor="LightGreen">OK</th>
        <td>0</td>
 </tr>
 <tr>
        <th colspan="2" scope="row">Acta de matrimonio</th>
        <td>
         <div class="custom-control custom-checkbox">
         <input type="checkbox" class="custom-control-input" id="actaMatrimonio">
         <label class="custom-control-label" for="actaMatrimonio"></label>
         </div>
         </td>
        <td >15/01/2018</td>
        <td >22/01/2018</td>
        <td >22/01/2018</td>
        <td ><input type="text" class="form-control"> </input></td>
        <th bgcolor="LightGreen">OK</th>
        <td>0</td>
 </tr>
 <tr>
        <th colspan="2" scope="row">Acta de nacimiento</th>
        <td>
         <div class="custom-control custom-checkbox">
         <input type="checkbox" class="custom-control-input" id="actaNacimiento">
         <label class="custom-control-label" for="actaNacimiento"></label>
         </div>
         </td>
        <td >15/01/2018</td>
        <td >22/01/2018</td>
        <td >22/01/2018</td>
        <td ><input type="text" class="form-control"> </input></td>
        <th bgcolor="LightGreen">OK</th>
        <td>0</td>
 </tr>
 <tr>
        <th colspan="2" scope="row">Forma Migratoria</th>
        <td>
         <div class="custom-control custom-checkbox">
         <input type="checkbox" class="custom-control-input" id="FormaMigratoria">
         <label class="custom-control-label" for="FormaMigratoria"></label>
         </div>
         </td>
        <td >15/01/2018</td>
        <td >22/01/2018</td>
        <td >22/01/2018</td>
        <td ><input type="text" class="form-control"> </input></td>
        <th bgcolor="LightGreen">OK</th>
        <td>0</td>
 </tr>
 <tr>
        <th colspan="2" scope="row">Medio de Pago - Contado</th>
        <td>
         <div class="custom-control custom-checkbox">
         <input type="checkbox" class="custom-control-input" id="MedioPagoContado">
         <label class="custom-control-label" for="MedioPagoContado"></label>
         </div>
         </td>
        <td >15/01/2018</td>
        <td >22/01/2018</td>
        <td >22/01/2018</td>
        <td ><input type="text" class="form-control"> </input></td>
        <th bgcolor="LightGreen">OK</th>
        <td>0</td>
 </tr>
 <tr>
        <th colspan="2" scope="row">Medio de Pago - Transferencia electrónica</th>
        <td>
         <div class="custom-control custom-checkbox">
         <input type="checkbox" class="custom-control-input" id="MedioPagoTransferenciaElectronica">
         <label class="custom-control-label" for="MedioPagoTransferenciaElectronica"></label>
         </div>
         </td>
        <td >15/01/2018</td>
        <td >22/01/2018</td>
        <td >22/01/2018</td>
        <td ><input type="text" class="form-control"> </input></td>
        <th bgcolor="LightGreen">OK</th>
        <td>0</td>
 </tr>
 <tr>
        <th colspan="2" scope="row">Medio de Pago - Cheque de caja</th>
        <td>
         <div class="custom-control custom-checkbox">
         <input type="checkbox" class="custom-control-input" id="MedioPagoChequeCaja">
         <label class="custom-control-label" for="MedioPagoChequeCaja"></label>
         </div>
         </td>
        <td >15/01/2018</td>
        <td >22/01/2018</td>
        <td >22/01/2018</td>
        <td ><input type="text" class="form-control"> </input></td>
        <th bgcolor="LightGreen">OK</th>
        <td>0</td>
 </tr>
 <tr>
        <th colspan="2" scope="row">Medio de Pago - Cheque certificado</th>
        <td>
         <div class="custom-control custom-checkbox">
         <input type="checkbox" class="custom-control-input" id="MedioPagoChequeCertificado">
         <label class="custom-control-label" for="MedioPagoChequeCertificado"></label>
         </div>
         </td>
        <td >15/01/2018</td>
        <td >22/01/2018</td>
        <td >22/01/2018</td>
        <td ><input type="text" class="form-control"> </input></td>
        <th bgcolor="LightGreen">OK</th>
        <td>0</td>
 </tr>
 <tr>
        <th colspan="2" scope="row">Comprobante de domicilio (luz, teléfono, estado de cuenta)</th>
        <td>
         <div class="custom-control custom-checkbox">
         <input type="checkbox" class="custom-control-input" id="MedioPagoChequeCertificado">
         <label class="custom-control-label" for="MedioPagoChequeCertificado"></label>
         </div>
         </td>
        <td >15/01/2018</td>
        <td >22/01/2018</td>
        <td >22/01/2018</td>
        <td ><input type="text" class="form-control"> </input></td>
        <th bgcolor="LightGreen">OK</th>
        <td>0</td>
 </tr>
 <tr>
        <th colspan="2" scope="row">Identificación oficial (INE, pasaporte, cédula profesional, licencia)</th>
        <td>
         <div class="custom-control custom-checkbox">
         <input type="checkbox" class="custom-control-input" id="IdentificacionOficial">
         <label class="custom-control-label" for="IdentificacionOficial"></label>
         </div>
         </td>
        <td >15/01/2018</td>
        <td >22/01/2018</td>
        <td >22/01/2018</td>
        <td ><input type="text" class="form-control"> </input></td>
        <th bgcolor="LightGreen">OK</th>
        <td>0</td>
 </tr>
 <tr>
        <th colspan="2" scope="row">CURP</th>
        <td>
         <div class="custom-control custom-checkbox">
         <input type="checkbox" class="custom-control-input" id="CURP">
         <label class="custom-control-label" for="CURP"></label>
         </div>
         </td>
        <td >15/01/2018</td>
        <td >22/01/2018</td>
        <td >22/01/2018</td>
        <td ><input type="text" class="form-control"> </input></td>
        <th bgcolor="LightGreen">OK</th>
        <td>0</td>
 </tr>
 <tr>
        <th colspan="2" scope="row">RFC</th>
        <td>
         <div class="custom-control custom-checkbox">
         <input type="checkbox" class="custom-control-input" id="RFC">
         <label class="custom-control-label" for="RFC"></label>
         </div>
         </td>
        <td >15/01/2018</td>
        <td >22/01/2018</td>
        <td >22/01/2018</td>
        <td ><input type="text" class="form-control"> </input></td>
        <th bgcolor="LightGreen">OK</th>
        <td>0</td>
 </tr>
 <tr>
        <th colspan="2" scope="row">UIF (Comprobante de domicilio)</th>
        <td>
         <div class="custom-control custom-checkbox">
         <input type="checkbox" class="custom-control-input" id="UIFComprobanteDomicilio">
         <label class="custom-control-label" for="UIFComprobanteDomicilio"></label>
         </div>
         </td>
        <td >15/01/2018</td>
        <td >22/01/2018</td>
        <td >22/01/2018</td>
        <td ><input type="text" class="form-control"> </input></td>
        <th bgcolor="LightGreen">OK</th>
        <td>0</td>
 </tr>
 <tr>
        <th colspan="2" scope="row">UIF (Identificación)</th>
        <td>
         <div class="custom-control custom-checkbox">
         <input type="checkbox" class="custom-control-input" id="UIFIdentificacion">
         <label class="custom-control-label" for="UIFIdentificacion"></label>
         </div>
         </td>
        <td >15/01/2018</td>
        <td >22/01/2018</td>
        <td >22/01/2018</td>
        <td ><input type="text" class="form-control"> </input></td>
        <th bgcolor="LightGreen">OK</th>
        <td>0</td>
 </tr>
 <tr>
        <th colspan="2" scope="row">UIF (RFC)</th>
        <td>
         <div class="custom-control custom-checkbox">
         <input type="checkbox" class="custom-control-input" id="UIFRFC">
         <label class="custom-control-label" for="UIFRFC"></label>
         </div>
         </td>
        <td >15/01/2018</td>
        <td >22/01/2018</td>
        <td >22/01/2018</td>
        <td ><input type="text" class="form-control"> </input></td>
        <th bgcolor="LightGreen">OK</th>
        <td>0</td>
 </tr>
 <tr>
        <th colspan="2" scope="row">UIF (Medio de Pago)</th>
        <td>
         <div class="custom-control custom-checkbox">
         <input type="checkbox" class="custom-control-input" id="UIFMedioPago">
         <label class="custom-control-label" for="UIFMedioPago"></label>
         </div>
         </td>
        <td >15/01/2018</td>
        <td >22/01/2018</td>
        <td >22/01/2018</td>
        <td ><input type="text" class="form-control"> </input></td>
        <th bgcolor="LightGreen">OK</th>
        <td>0</td>
 </tr>
 <tr>
        <th colspan="2" scope="row">UIF (Cartas de dueño y beneficiario)</th>
        <td>
         <div class="custom-control custom-checkbox">
         <input type="checkbox" class="custom-control-input" id="UIFCartasDueñoBeneficiario">
         <label class="custom-control-label" for="UIFCartasDueñoBeneficiario"></label>
         </div>
         </td>
        <td >15/01/2018</td>
        <td >22/01/2018</td>
        <td >22/01/2018</td>
        <td ><input type="text" class="form-control"> </input></td>
        <th bgcolor="LightGreen">OK</th>
        <td>0</td>
 </tr>
 <tr>
        <th colspan="2" scope="row">UIF (Pago de tercero)</th>
        <td>
         <div class="custom-control custom-checkbox">
         <input type="checkbox" class="custom-control-input" id="UIFPagoTercero">
         <label class="custom-control-label" for="UIFPagoTercero"></label>
         </div>
         </td>
        <td >15/01/2018</td>
        <td >22/01/2018</td>
        <td >22/01/2018</td>
        <td ><input type="text" class="form-control"> </input></td>
        <th bgcolor="LightGreen">OK</th>
        <td>0</td>
 </tr>
 <tr>
        <th colspan="2" scope="row">Acta constitutiva</th>
        <td>
         <div class="custom-control custom-checkbox">
         <input type="checkbox" class="custom-control-input" id="ActaConstitutiva">
         <label class="custom-control-label" for="ActaConstitutiva"></label>
         </div>
         </td>
        <td >15/01/2018</td>
        <td >22/01/2018</td>
        <td >22/01/2018</td>
        <td ><input type="text" class="form-control"> </input></td>
        <th bgcolor="LightGreen">OK</th>
        <td>0</td>
 </tr>
 <tr>
        <th colspan="2" scope="row">Poder del representante legal</th>
        <td>
         <div class="custom-control custom-checkbox">
         <input type="checkbox" class="custom-control-input" id="PoderRepresentanteLegal">
         <label class="custom-control-label" for="PoderRepresentanteLegal"></label>
         </div>
         </td>
        <td >15/01/2018</td>
        <td >22/01/2018</td>
        <td >22/01/2018</td>
        <td ><input type="text" class="form-control"> </input></td>
        <th bgcolor="LightGreen">OK</th>
        <td>0</td>
 </tr>
 <tr>
        <th colspan="2" scope="row">Avalúo para efectos del crédito otorgado por el banco</th>
        <td>
         <div class="custom-control custom-checkbox">
         <input type="checkbox" class="custom-control-input" id="AvaluoParaEfectos">
         <label class="custom-control-label" for="AvaluoParaEfectos"></label>
         </div>
         </td>
        <td >15/01/2018</td>
        <td >22/01/2018</td>
        <td >22/01/2018</td>
        <td ><input type="text" class="form-control"> </input></td>
        <th bgcolor="LightGreen">OK</th>
        <td>0</td>
 </tr>
 <tr>
        <th colspan="2" scope="row">Carátula de crédito </th>
        <td>
         <div class="custom-control custom-checkbox">
         <input type="checkbox" class="custom-control-input" id="CaratulaCredito ">
         <label class="custom-control-label" for="CaratulaCredito"></label>
         </div>
         </td>
        <td >15/01/2018</td>
        <td >22/01/2018</td>
        <td >22/01/2018</td>
        <td ><input type="text" class="form-control"> </input></td>
        <th bgcolor="LightGreen">OK</th>
        <td>0</td>
 </tr>
 <tr>
        <th colspan="2" scope="row">Tabla de amortización </th>
        <td>
         <div class="custom-control custom-checkbox">
         <input type="checkbox" class="custom-control-input" id="TablaAmortizacion  ">
         <label class="custom-control-label" for="TablaAmortizacion"></label>
         </div>
         </td>
        <td >15/01/2018</td>
        <td >22/01/2018</td>
        <td >22/01/2018</td>
        <td ><input type="text" class="form-control"> </input></td>
        <th bgcolor="LightGreen">OK</th>
        <td>0</td>
 </tr>
 <tr>
        <th colspan="2" scope="row">Kit  CESI Infonavit</th>
        <td>
         <div class="custom-control custom-checkbox">
         <input type="checkbox" class="custom-control-input" id="KitCESIInfonavit  ">
         <label class="custom-control-label" for="KitCESIInfonavit"></label>
         </div>
         </td>
        <td >15/01/2018</td>
        <td >22/01/2018</td>
        <td >22/01/2018</td>
        <td ><input type="text" class="form-control"> </input></td>
        <th bgcolor="LightGreen">OK</th>
        <td>0</td>
 </tr>
 <tr>
        <th colspan="2" scope="row">Kit FOVISSSTE</th>
        <td>
         <div class="custom-control custom-checkbox">
         <input type="checkbox" class="custom-control-input" id="KitFOVISSSTE  ">
         <label class="custom-control-label" for="KitFOVISSSTE"></label>
         </div>
         </td>
        <td >15/01/2018</td>
        <td >22/01/2018</td>
        <td >22/01/2018</td>
        <td ><input type="text" class="form-control"> </input></td>
        <th bgcolor="LightGreen">OK</th>
        <td>0</td>
 </tr>
 <tr>
        <th colspan="2" scope="row">Última declaración anual  (solo vendedor pf con act empresarial)</th>
        <td>
         <div class="custom-control custom-checkbox">
         <input type="checkbox" class="custom-control-input" id="ultimaDeclaracionAnual  ">
         <label class="custom-control-label" for="ultimaDeclaracionAnual"></label>
         </div>
         </td>
        <td >15/01/2018</td>
        <td >22/01/2018</td>
        <td >22/01/2018</td>
        <td ><input type="text" class="form-control"> </input></td>
        <th bgcolor="LightGreen">OK</th>
        <td>0</td>
 </tr>
 <tr>
        <th colspan="2" scope="row">Tres declaraciones mensuales (solo local comercial)</th>
        <td>
         <div class="custom-control custom-checkbox">
         <input type="checkbox" class="custom-control-input" id="TresDeclaracionesMensuales  ">
         <label class="custom-control-label" for="TresDeclaracionesMensuales"></label>
         </div>
         </td>
        <td >15/01/2018</td>
        <td >22/01/2018</td>
        <td >22/01/2018</td>
        <td ><input type="text" class="form-control"> </input></td>
        <th bgcolor="LightGreen">OK</th>
        <td>0</td>
 </tr>
 <tr>
        <th colspan="2" scope="row">Complemento de factura que dio el Notario al vendedor CFDI </th>
        <td>
         <div class="custom-control custom-checkbox">
         <input type="checkbox" class="custom-control-input" id="ComplementoFactura  ">
         <label class="custom-control-label" for="ComplementoFactura"></label>
         </div>
         </td>
        <td >15/01/2018</td>
        <td >22/01/2018</td>
        <td >22/01/2018</td>
        <td ><input type="text" class="form-control"> </input></td>
        <th bgcolor="LightGreen">OK</th>
        <td>0</td>
 </tr>
 <tr>
        <th colspan="2" scope="row">Facturas de inversiones que demuestren mejoras hechas al inmueble</th>
        <td>
         <div class="custom-control custom-checkbox">
         <input type="checkbox" class="custom-control-input" id="FacturasInversiones  ">
         <label class="custom-control-label" for="FacturasInversiones"></label>
         </div>
         </td>
        <td >15/01/2018</td>
        <td >22/01/2018</td>
        <td >22/01/2018</td>
        <td ><input type="text" class="form-control"> </input></td>
        <th bgcolor="LightGreen">OK</th>
        <td>0</td>
 </tr>
 <tr>
        <th colspan="2" scope="row">Gastos notariales por el trámite factura (abril 2014 para atrás) </th>
        <td>
         <div class="custom-control custom-checkbox">
         <input type="checkbox" class="custom-control-input" id="GastosNotariales  ">
         <label class="custom-control-label" for="GastosNotariales"></label>
         </div>
         </td>
        <td >15/01/2018</td>
        <td >22/01/2018</td>
        <td >22/01/2018</td>
        <td ><input type="text" class="form-control"> </input></td>
        <th bgcolor="LightGreen">OK</th>
        <td>0</td>
 </tr>
 <tr>
        <th colspan="2" scope="row">Comisiones y mediaciones Facturas de corredor y bróker</th>
        <td>
         <div class="custom-control custom-checkbox">
         <input type="checkbox" class="custom-control-input" id="ComisionesMediaciones  ">
         <label class="custom-control-label" for="ComisionesMediaciones"></label>
         </div>
         </td>
        <td >15/01/2018</td>
        <td >22/01/2018</td>
        <td >22/01/2018</td>
        <td ><input type="text" class="form-control"> </input></td>
        <th bgcolor="LightGreen">OK</th>
        <td>0</td>
 </tr>
 <tr>
        <th colspan="2" scope="row">Otros deducibles </th>
        <td>
         <div class="custom-control custom-checkbox">
         <input type="checkbox" class="custom-control-input" id="OtrosDeducibles   ">
         <label class="custom-control-label" for="OtrosDeducibles"></label>
         </div>
         </td>
        <td >15/01/2018</td>
        <td >22/01/2018</td>
        <td >22/01/2018</td>
        <td ><input type="text" class="form-control"> </input></td>
        <th bgcolor="LightGreen">OK</th>
        <td>0</td>
 </tr>

</tbody>
</table>
{{-- fin docuemntos de integracion --}}
<table class="table table-bordered">
<thead>
    <tr>   
        <th colspan="12"><center><h4>COMPRADOR</h4></center></th>
    </tr>
</thead>
<tbody>
<tr>
    <th colspan="2" scope="col"><center>RUBRO </center></th>
    <th scope="col"></th>
    <th scope="col" >FECHA DE INICIO</th>
    <th scope="col">FECHA DE ENTREGA</th>
    <th scope="col">FECHA DE TÉRMINO</th>
    <th scope="col">COMENTARIOS</th>
    <th scope="col">SEMAFORO</th>
    <th scope="col">DIAS ANTES/RETRASO</th>
</tr>
<tr>
    <th colspan="2" scope="row" >Anticipo </th>
    <td>
    <div class="custom-control custom-checkbox">
    <input type="checkbox" class="custom-control-input" id="Anticipo">
    <label class="custom-control-label" for="Anticipo"></label>
    </div>
    </td>
    <td >15/01/2018</td>
    <td >17/01/2018</td>
    <td >17/01/2018</td>
    <td ><input type="text" class="form-control"> </input></td>
    <th bgcolor="LightGreen">OK</th>
    <td>0</td>
</tr>
<tr>
    <th colspan="2" scope="row" >Identificación oficial (INE, pasaporte, cédula profesional, otras) </th>
    <td>
    <div class="custom-control custom-checkbox">
    <input type="checkbox" class="custom-control-input" id="IdentificacionOficial_comprador">
    <label class="custom-control-label" for="IdentificacionOficial_comprador"></label>
    </div>
    </td>
    <td >15/01/2018</td>
    <td >22/01/2018</td>
    <td >22/01/2018</td>
    <td ><input type="text" class="form-control"> </input></td>
    <th bgcolor="LightGreen">OK</th>
    <td>0</td>
</tr>
<tr>
        <th colspan="2" scope="row" >Acta de matrimonio </th>
        <td>
        <div class="custom-control custom-checkbox">
        <input type="checkbox" class="custom-control-input" id="ActaMatrimonio_comprador">
        <label class="custom-control-label" for="ActaMatrimonio_comprador"></label>
        </div>
        </td>
        <td >15/01/2018</td>
        <td >22/01/2018</td>
        <td >22/01/2018</td>
        <td ><input type="text" class="form-control"> </input></td>
        <th bgcolor="LightGreen">OK</th>
        <td>0</td>
</tr>
<tr>
        <th colspan="2" scope="row" >Acta de Nacimiento </th>
        <td>
        <div class="custom-control custom-checkbox">
        <input type="checkbox" class="custom-control-input" id="ActaNacimiento_comprador">
        <label class="custom-control-label" for="ActaNacimiento_comprador"></label>
        </div>
        </td>
        <td >15/01/2018</td>
        <td >22/01/2018</td>
        <td >22/01/2018</td>
        <td ><input type="text" class="form-control"> </input></td>
        <th bgcolor="LightGreen">OK</th>
        <td>0</td>
</tr>
<tr>
        <th colspan="2" scope="row" >CURP </th>
        <td>
        <div class="custom-control custom-checkbox">
        <input type="checkbox" class="custom-control-input" id="CURP_comprador">
        <label class="custom-control-label" for="CURP_comprador"></label>
        </div>
        </td>
        <td >15/01/2018</td>
        <td >22/01/2018</td>
        <td >22/01/2018</td>
        <td ><input type="text" class="form-control"> </input></td>
        <th bgcolor="LightGreen">OK</th>
        <td>0</td>
</tr>
<tr>
        <th colspan="2" scope="row" >RFC </th>
        <td>
        <div class="custom-control custom-checkbox">
        <input type="checkbox" class="custom-control-input" id="RFC_comprador">
        <label class="custom-control-label" for="RFC_comprador"></label>
        </div>
        </td>
        <td >15/01/2018</td>
        <td >22/01/2018</td>
        <td >22/01/2018</td>
        <td ><input type="text" class="form-control"> </input></td>
        <th bgcolor="LightGreen">OK</th>
        <td>0</td>
</tr>
<tr>
        <th colspan="2" scope="row" >Acta constitutiva </th>
        <td>
        <div class="custom-control custom-checkbox">
        <input type="checkbox" class="custom-control-input" id="ActaConstitutiva_comprador">
        <label class="custom-control-label" for="ActaConstitutiva_comprador"></label>
        </div>
        </td>
        <td >15/01/2018</td>
        <td >22/01/2018</td>
        <td >22/01/2018</td>
        <td ><input type="text" class="form-control"> </input></td>
        <th bgcolor="LightGreen">OK</th>
        <td>0</td>
</tr>
<tr>
        <th colspan="2" scope="row" >Poder del representante legal </th>
        <td>
        <div class="custom-control custom-checkbox">
        <input type="checkbox" class="custom-control-input" id="PoderRepresentanteLegal_comprador">
        <label class="custom-control-label" for="PoderRepresentanteLegal_comprador"></label>
        </div>
        </td>
        <td >15/01/2018</td>
        <td >22/01/2018</td>
        <td >22/01/2018</td>
        <td ><input type="text" class="form-control"> </input></td>
        <th bgcolor="LightGreen">OK</th>
        <td>0</td>
</tr>
<tr>
        <th colspan="2" scope="row" >Extranjero</th>
        <td>
        <div class="custom-control custom-checkbox">
        <input type="checkbox" class="custom-control-input" id="Extranjero_comprador">
        <label class="custom-control-label" for="Extranjero_comprador"></label>
        </div>
        </td>
        <td >15/01/2018</td>
        <td >22/01/2018</td>
        <td >22/01/2018</td>
        <td ><input type="text" class="form-control"> </input></td>
        <th bgcolor="LightGreen">OK</th>
        <td>0</td>
</tr>
<tr>
        <th colspan="2" scope="row" >Forma Migratoria </th>
        <td>
        <div class="custom-control custom-checkbox">
        <input type="checkbox" class="custom-control-input" id="FormaMigratoria_comprador">
        <label class="custom-control-label" for="FormaMigratoria_comprador"></label>
        </div>
        </td>
        <td >15/01/2018</td>
        <td >22/01/2018</td>
        <td >22/01/2018</td>
        <td ><input type="text" class="form-control"> </input></td>
        <th bgcolor="LightGreen">OK</th>
        <td>0</td>
</tr>
<tr>
        <th colspan="2" scope="row" >Identificación del garante hipotecario </th>
        <td>
        <div class="custom-control custom-checkbox">
        <input type="checkbox" class="custom-control-input" id="IdentificacionGaranteHipotecario_comprador">
        <label class="custom-control-label" for="IdentificacionGaranteHipotecario_comprador"></label>
        </div>
        </td>
        <td >15/01/2018</td>
        <td >22/01/2018</td>
        <td >22/01/2018</td>
        <td ><input type="text" class="form-control"> </input></td>
        <th bgcolor="LightGreen">OK</th>
        <td>0</td>
</tr>
<tr>
        <th  colspan="2" scope="row" >Identificación oficial del obligado solidario </th>
        <td>
        <div class="custom-control custom-checkbox">
        <input type="checkbox" class="custom-control-input" id="IdentificacionOficialObligadoOlidario_comprador">
        <label class="custom-control-label" for="IdentificacionOficialObligadoOlidario_comprador"></label>
        </div>
        </td>
        <td >15/01/2018</td>
        <td >22/01/2018</td>
        <td >22/01/2018</td>
        <td ><input type="text" class="form-control"> </input></td>
        <th bgcolor="LightGreen">OK</th>
        <td>0</td>
</tr>
    </tbody>
    </table>
{{-- fin comprador --}}
<div class="row">
<div class="col-md-4">
</div>
<div class="col-md-4">
<button id="" type="submit" class="btn btn-success">Guardar</button>
<button id="" type="submit" class="btn btn-warning">Actualizar </button>
</div>
<div class="col-md-4">
</div>
</div>
<br>
<table class="table table-bordered">
<thead>
    <tr>   
        <th colspan="12"><center><h3>PREFIRMA / PREVIOS</h3></center></th>
    </tr>
</thead>
<tbody>
<tr>
    <th colspan="2" scope="col"><center>RUBRO </center></th>
    <th scope="col"></th>
    <th scope="col" >FECHA DE INICIO</th>
    <th scope="col">FECHA DE ENTREGA</th>
    <th scope="col">FECHA DE TÉRMINO</th>
    <th scope="col">COMENTARIOS</th>
    <th scope="col">SEMAFORO</th>
    <th scope="col">DIAS ANTES/RETRASO</th>
</tr>
<tr>
    <th class="bg-info" colspan="2" scope="row" >Certificado de Libertad de Gravamen </th>
    <td class="bg-info">
    <div class="custom-control custom-checkbox">
    <input type="checkbox" class="custom-control-input" id="CertificadoLibertadGravamen">
    <label class="custom-control-label" for="CertificadoLibertadGravamen"></label>
    </div>
    </td>
    <td class="bg-info">16/01/2018</td>
    <td class="bg-info">18/01/2018</td>
    <td class="bg-info">18/01/2018</td>
    <td class="bg-info"><input type="text" class="form-control"> </input></td>
    <th bgcolor="LightGreen">OK</th>
    <td>0</td>
</tr>
<tr>
    <th colspan="2" scope="row" >Generar línea de captura </th>
    <td>
    <div class="custom-control custom-checkbox">
    <input type="checkbox" class="custom-control-input" id="GenerarLineaCaptura">
    <label class="custom-control-label" for="GenerarLineaCaptura"></label>
    </div>
    </td>
    <td >16/01/2018</td>
    <td >16/01/2018</td>
    <td >16/01/2018</td>
    <td ><input type="text" class="form-control"> </input></td>
    <th bgcolor="LightGreen">OK</th>
    <td>0</td>
</tr>
<tr>
    <th colspan="2" scope="row" >Ingresar solicitud</th>
    <td>
    <div class="custom-control custom-checkbox">
    <input type="checkbox" class="custom-control-input" id="IngresarSolicitud">
    <label class="custom-control-label" for="IngresarSolicitud"></label>
    </div>
    </td>
    <td >17/01/2018</td>
    <td >17/01/2018</td>
    <td >17/01/2018</td>
    <td ><input type="text" class="form-control"> </input></td>
    <th bgcolor="LightGreen">OK</th>
    <td>0</td>
</tr>
<tr>
    <th colspan="2" scope="row" >Obtener certificado</th>
    <td>
    <div class="custom-control custom-checkbox">
    <input type="checkbox" class="custom-control-input" id="ObtenerCertificado">
    <label class="custom-control-label" for="ObtenerCertificado"></label>
    </div>
    </td>
    <td >17/01/2018</td>
    <td >18/01/2018</td>
    <td >18/01/2018</td>
    <td ><input type="text" class="form-control"> </input></td>
    <th bgcolor="LightGreen">OK</th>
    <td>0</td>
</tr>
<tr>
    <th colspan="2" scope="row" >Informar al banco</th>
    <td>
    <div class="custom-control custom-checkbox">
    <input type="checkbox" class="custom-control-input" id="InformarBanco">
    <label class="custom-control-label" for="InformarBanco"></label>
    </div>
    </td>
    <td >18/01/2018</td>
    <td >18/01/2018</td>
    <td >18/01/2018</td>
    <td ><input type="text" class="form-control"> </input></td>
    <th bgcolor="LightGreen">OK</th>
    <td>0</td>
</tr>
<!-- _______________________________________________________________________________ -->
<tr>
    <th class="bg-info" colspan="2" scope="row" >Zonificación </th>
    <td class="bg-info">
    <div class="custom-control custom-checkbox">
    <input type="checkbox" class="custom-control-input" id="Zonificacion">
    <label class="custom-control-label" for="Zonificacion"></label>
    </div>
    </td>
    <td class="bg-info">16/01/2018</td>
    <td class="bg-info">22/01/2018</td>
    <td class="bg-info">18/01/2018</td>
    <td class="bg-info"><input type="text" class="form-control"> </input></td>
    <th bgcolor="LightGreen">OK</th>
    <td>0</td>
</tr>
<tr>
    <th colspan="2" scope="row" >Generar línea de captura</th>
    <td>
    <div class="custom-control custom-checkbox">
    <input type="checkbox" class="custom-control-input" id="GenerarLineaCaptura">
    <label class="custom-control-label" for="GenerarLineaCaptura"></label>
    </div>
    </td>
    <td >16/01/2018</td>
    <td >16/01/2018</td>
    <td >16/01/2018</td>
    <td ><input type="text" class="form-control"> </input></td>
    <th bgcolor="LightGreen">OK</th>
    <td>0</td>
</tr>
<tr>
    <th colspan="2" scope="row" >Realizar pago</th>
    <td>
    <div class="custom-control custom-checkbox">
    <input type="checkbox" class="custom-control-input" id="RealizarPago">
    <label class="custom-control-label" for="RealizarPago"></label>
    </div>
    </td>
    <td >16/01/2018</td>
    <td >16/01/2018</td>
    <td >16/01/2018</td>
    <td ><input type="text" class="form-control"> </input></td>
    <th bgcolor="LightGreen">OK</th>
    <td>0</td>
</tr>
<tr>
    <th colspan="2" scope="row" >Ingresar solicitud</th>
    <td>
    <div class="custom-control custom-checkbox">
    <input type="checkbox" class="custom-control-input" id="IngresarSolicitud">
    <label class="custom-control-label" for="IngresarSolicitud"></label>
    </div>
    </td>
    <td >17/01/2018</td>
    <td >17/01/2018</td>
    <td >17/01/2018</td>
    <td ><input type="text" class="form-control"> </input></td>
    <th bgcolor="LightGreen">OK</th>
    <td>0</td>
</tr>
<tr>
    <th colspan="2" scope="row" >Obtener certificado</th>
    <td>
    <div class="custom-control custom-checkbox">
    <input type="checkbox" class="custom-control-input" id="ObtenerCertificado">
    <label class="custom-control-label" for="ObtenerCertificado"></label>
    </div>
    </td>
    <td >17/01/2018</td>
    <td >22/01/2018</td>
    <td >18/01/2018</td>
    <td ><input type="text" class="form-control"> </input></td>
    <th bgcolor="LightGreen">OK</th>
    <td>0</td>
</tr>
<!-- _______________________________________________________________________________ -->
<tr>
    <th class="bg-info" colspan="2" scope="row" >Agua </th>
    <td class="bg-info">
    <div class="custom-control custom-checkbox">
    <input type="checkbox" class="custom-control-input" id="Agua">
    <label class="custom-control-label" for="Agua"></label>
    </div>
    </td>
    <td class="bg-info">16/01/2018</td>
    <td class="bg-info">18/01/2018</td>
    <td class="bg-info">18/01/2018</td>
    <td class="bg-info"><input type="text" class="form-control"> </input></td>
    <th bgcolor="LightGreen">OK</th>
    <td>0</td>
</tr>
<tr>
    <th colspan="2" scope="row" >Generar línea de captura</th>
    <td>
    <div class="custom-control custom-checkbox">
    <input type="checkbox" class="custom-control-input" id="GenerarLineaCaptura">
    <label class="custom-control-label" for="GenerarLineaCaptura"></label>
    </div>
    </td>
    <td >16/01/2018</td>
    <td >16/01/2018</td>
    <td >16/01/2018</td>
    <td ><input type="text" class="form-control"> </input></td>
    <th bgcolor="LightGreen">OK</th>
    <td>0</td>
</tr>
<tr>
    <th colspan="2" scope="row" >Realizar pago</th>
    <td>
    <div class="custom-control custom-checkbox">
    <input type="checkbox" class="custom-control-input" id="RealizarPago">
    <label class="custom-control-label" for="RealizarPago"></label>
    </div>
    </td>
    <td >16/01/2018</td>
    <td >16/01/2018</td>
    <td >16/01/2018</td>
    <td ><input type="text" class="form-control"> </input></td>
    <th bgcolor="LightGreen">OK</th>
    <td>0</td>
</tr>
<tr>
    <th colspan="2" scope="row" >Ingresar solicitud</th>
    <td>
    <div class="custom-control custom-checkbox">
    <input type="checkbox" class="custom-control-input" id="IngresarSolicitud">
    <label class="custom-control-label" for="IngresarSolicitud"></label>
    </div>
    </td>
    <td >17/01/2018</td>
    <td >17/01/2018</td>
    <td >17/01/2018</td>
    <td ><input type="text" class="form-control"> </input></td>
    <th bgcolor="LightGreen">OK</th>
    <td>0</td>
</tr>
<tr>
    <th colspan="2" scope="row" >Obtener certificado</th>
    <td>
    <div class="custom-control custom-checkbox">
    <input type="checkbox" class="custom-control-input" id="ObtenerCertificado">
    <label class="custom-control-label" for="ObtenerCertificado"></label>
    </div>
    </td>
    <td >17/01/2018</td>
    <td >22/01/2018</td>
    <td >18/01/2018</td>
    <td ><input type="text" class="form-control"> </input></td>
    <th bgcolor="LightGreen">OK</th>
    <td>0</td>
</tr>
<tr>
    <th colspan="2" scope="row" >Informar adeudos</th>
    <td>
    <div class="custom-control custom-checkbox">
    <input type="checkbox" class="custom-control-input" id="ObtenerCertificado">
    <label class="custom-control-label" for="ObtenerCertificado"></label>
    </div>
    </td>
    <td >18/01/2018</td>
    <td >18/01/2018</td>
    <td >18/01/2018</td>
    <td ><input type="text" class="form-control"> </input></td>
    <th bgcolor="LightGreen">OK</th>
    <td>0</td>
</tr>
<!-- _______________________________________________________________________________ -->
<tr>
    <th class="bg-info" colspan="2" scope="row" >Predial </th>
    <td class="bg-info">
    <div class="custom-control custom-checkbox">
    <input type="checkbox" class="custom-control-input" id="Predial">
    <label class="custom-control-label" for="Predial"></label>
    </div>
    </td>
    <td class="bg-info">16/01/2018</td>
    <td class="bg-info">18/01/2018</td>
    <td class="bg-info">18/01/2018</td>
    <td class="bg-info"><input type="text" class="form-control"> </input></td>
    <th bgcolor="LightGreen">OK</th>
    <td>0</td>
</tr>
<tr>
    <th colspan="2" scope="row" >Generar línea de captura</th>
    <td>
    <div class="custom-control custom-checkbox">
    <input type="checkbox" class="custom-control-input" id="GenerarLineaCaptura">
    <label class="custom-control-label" for="GenerarLineaCaptura"></label>
    </div>
    </td>
    <td >16/01/2018</td>
    <td >16/01/2018</td>
    <td >16/01/2018</td>
    <td ><input type="text" class="form-control"> </input></td>
    <th bgcolor="LightGreen">OK</th>
    <td>0</td>
</tr>
<tr>
    <th colspan="2" scope="row" >Realizar pago</th>
    <td>
    <div class="custom-control custom-checkbox">
    <input type="checkbox" class="custom-control-input" id="RealizarPago">
    <label class="custom-control-label" for="RealizarPago"></label>
    </div>
    </td>
    <td >16/01/2018</td>
    <td >16/01/2018</td>
    <td >16/01/2018</td>
    <td ><input type="text" class="form-control"> </input></td>
    <th bgcolor="LightGreen">OK</th>
    <td>0</td>
</tr>
<tr>
    <th colspan="2" scope="row" >Ingresar solicitud</th>
    <td>
    <div class="custom-control custom-checkbox">
    <input type="checkbox" class="custom-control-input" id="IngresarSolicitud">
    <label class="custom-control-label" for="IngresarSolicitud"></label>
    </div>
    </td>
    <td >17/01/2018</td>
    <td >17/01/2018</td>
    <td >17/01/2018</td>
    <td ><input type="text" class="form-control"> </input></td>
    <th bgcolor="LightGreen">OK</th>
    <td>0</td>
</tr>
<tr>
    <th colspan="2" scope="row" >Obtener certificado</th>
    <td>
    <div class="custom-control custom-checkbox">
    <input type="checkbox" class="custom-control-input" id="ObtenerCertificado">
    <label class="custom-control-label" for="ObtenerCertificado"></label>
    </div>
    </td>
    <td >17/01/2018</td>
    <td >22/01/2018</td>
    <td >18/01/2018</td>
    <td ><input type="text" class="form-control"> </input></td>
    <th bgcolor="LightGreen">OK</th>
    <td>0</td>
</tr>
<tr>
    <th colspan="2" scope="row" >Informar adeudos</th>
    <td>
    <div class="custom-control custom-checkbox">
    <input type="checkbox" class="custom-control-input" id="ObtenerCertificado">
    <label class="custom-control-label" for="ObtenerCertificado"></label>
    </div>
    </td>
    <td >18/01/2018</td>
    <td >18/01/2018</td>
    <td >18/01/2018</td>
    <td ><input type="text" class="form-control"> </input></td>
    <th bgcolor="LightGreen">OK</th>
    <td>0</td>
</tr>
<!-- _______________________________________________________________________________ -->
<tr>
    <th class="bg-info" colspan="2" scope="row" >Talón de jornada</th>
    <td class="bg-info">
    <div class="custom-control custom-checkbox">
    <input type="checkbox" class="custom-control-input" id="TalonJornada">
    <label class="custom-control-label" for="TalonJornada"></label>
    </div>
    </td>
    <td class="bg-info">16/01/2018</td>
    <td class="bg-info">22/01/2018</td>
    <td class="bg-info">18/01/2018</td>
    <td class="bg-info"><input type="text" class="form-control"> </input></td>
    <th bgcolor="LightGreen">OK</th>
    <td>0</td>
</tr>
<tr>
    <th colspan="2" scope="row" >Firmar solicitud por el cliente (el mismo día que venga a documentar)</th>
    <td>
    <div class="custom-control custom-checkbox">
    <input type="checkbox" class="custom-control-input" id="FirmarSolicitud">
    <label class="custom-control-label" for="FirmarSolicitud"></label>
    </div>
    </td>
    <td >16/01/2018</td>
    <td >16/01/2018</td>
    <td >16/01/2018</td>
    <td ><input type="text" class="form-control"> </input></td>
    <th bgcolor="LightGreen">OK</th>
    <td>0</td>
</tr>
<tr>
    <th colspan="2" scope="row" >Realizar pago</th>
    <td>
    <div class="custom-control custom-checkbox">
    <input type="checkbox" class="custom-control-input" id="RealizarPago">
    <label class="custom-control-label" for="RealizarPago"></label>
    </div>
    </td>
    <td >16/01/2018</td>
    <td >16/01/2018</td>
    <td >16/01/2018</td>
    <td ><input type="text" class="form-control"> </input></td>
    <th bgcolor="LightGreen">OK</th>
    <td>0</td>
</tr>
<tr>
    <th colspan="2" scope="row" >Ingresar solicitud</th>
    <td>
    <div class="custom-control custom-checkbox">
    <input type="checkbox" class="custom-control-input" id="IngresarSolicitud">
    <label class="custom-control-label" for="IngresarSolicitud"></label>
    </div>
    </td>
    <td >17/01/2018</td>
    <td >17/01/2018</td>
    <td >17/01/2018</td>
    <td ><input type="text" class="form-control"> </input></td>
    <th bgcolor="LightGreen">OK</th>
    <td>0</td>
</tr>
<tr>
    <th colspan="2" scope="row" >Obtener Talón</th>
    <td>
    <div class="custom-control custom-checkbox">
    <input type="checkbox" class="custom-control-input" id="ObtenerTalon">
    <label class="custom-control-label" for="ObtenerTalon"></label>
    </div>
    </td>
    <td >17/01/2018</td>
    <td >22/01/2018</td>
    <td >18/01/2018</td>
    <td ><input type="text" class="form-control"> </input></td>
    <th bgcolor="LightGreen">OK</th>
    <td>0</td>
</tr>
<!-- _______________________________________________________________________________ -->
<tr>
    <th class="bg-info" colspan="2" scope="row" >Constancia de mejoras </th>
    <td class="bg-info">
    <div class="custom-control custom-checkbox">
    <input type="checkbox" class="custom-control-input" id="ConstanciaMejoras">
    <label class="custom-control-label" for="ConstanciaMejoras"></label>
    </div>
    </td>
    <td class="bg-info">16/01/2018</td>
    <td class="bg-info">22/01/2018</td>
    <td class="bg-info">18/01/2018</td>
    <td class="bg-info"><input type="text" class="form-control"> </input></td>
    <th bgcolor="LightGreen">OK</th>
    <td>0</td>
</tr>
<tr>
    <th colspan="2" scope="row" >Generar línea de captura</th>
    <td>
    <div class="custom-control custom-checkbox">
    <input type="checkbox" class="custom-control-input" id="GenerarLineaCaptura">
    <label class="custom-control-label" for="GenerarLineaCaptura"></label>
    </div>
    </td>
    <td >16/01/2018</td>
    <td >16/01/2018</td>
    <td >16/01/2018</td>
    <td ><input type="text" class="form-control"> </input></td>
    <th bgcolor="LightGreen">OK</th>
    <td>0</td>
</tr>
<tr>
    <th colspan="2" scope="row" >Realizar pago</th>
    <td>
    <div class="custom-control custom-checkbox">
    <input type="checkbox" class="custom-control-input" id="RealizarPago">
    <label class="custom-control-label" for="RealizarPago"></label>
    </div>
    </td>
    <td >16/01/2018</td>
    <td >16/01/2018</td>
    <td >16/01/2018</td>
    <td ><input type="text" class="form-control"> </input></td>
    <th bgcolor="LightGreen">OK</th>
    <td>0</td>
</tr>
<tr>
    <th colspan="2" scope="row" >Ingresar solicitud</th>
    <td>
    <div class="custom-control custom-checkbox">
    <input type="checkbox" class="custom-control-input" id="IngresarSolicitud">
    <label class="custom-control-label" for="IngresarSolicitud"></label>
    </div>
    </td>
    <td >17/01/2018</td>
    <td >17/01/2018</td>
    <td >17/01/2018</td>
    <td ><input type="text" class="form-control"> </input></td>
    <th bgcolor="LightGreen">OK</th>
    <td>0</td>
</tr>
<tr>
    <th colspan="2" scope="row" >Obtener certificado</th>
    <td>
    <div class="custom-control custom-checkbox">
    <input type="checkbox" class="custom-control-input" id="ObtenerCertificado">
    <label class="custom-control-label" for="ObtenerCertificado"></label>
    </div>
    </td>
    <td >17/01/2018</td>
    <td >22/01/2018</td>
    <td >18/01/2018</td>
    <td ><input type="text" class="form-control"> </input></td>
    <th bgcolor="LightGreen">OK</th>
    <td>0</td>
</tr>
<!-- _______________________________________________________________________________ -->
<tr>
    <th class="bg-info" colspan="2" scope="row" >Clave y valor catastral </th>
    <td class="bg-info">
    <div class="custom-control custom-checkbox">
    <input type="checkbox" class="custom-control-input" id="ClaveValorCatastral">
    <label class="custom-control-label" for="ClaveValorCatastral"></label>
    </div>
    </td>
    <td class="bg-info">16/01/2018</td>
    <td class="bg-info">22/01/2018</td>
    <td class="bg-info">18/01/2018</td>
    <td class="bg-info"><input type="text" class="form-control"> </input></td>
    <th bgcolor="LightGreen">OK</th>
    <td>0</td>
</tr>
<tr>
    <th colspan="2" scope="row" >Generar línea de captura</th>
    <td>
    <div class="custom-control custom-checkbox">
    <input type="checkbox" class="custom-control-input" id="GenerarLineaCaptura">
    <label class="custom-control-label" for="GenerarLineaCaptura"></label>
    </div>
    </td>
    <td >16/01/2018</td>
    <td >16/01/2018</td>
    <td >16/01/2018</td>
    <td ><input type="text" class="form-control"> </input></td>
    <th bgcolor="LightGreen">OK</th>
    <td>0</td>
</tr>
<tr>
    <th colspan="2" scope="row" >Realizar pago</th>
    <td>
    <div class="custom-control custom-checkbox">
    <input type="checkbox" class="custom-control-input" id="RealizarPago">
    <label class="custom-control-label" for="RealizarPago"></label>
    </div>
    </td>
    <td >16/01/2018</td>
    <td >16/01/2018</td>
    <td >16/01/2018</td>
    <td ><input type="text" class="form-control"> </input></td>
    <th bgcolor="LightGreen">OK</th>
    <td>0</td>
</tr>
<tr>
    <th colspan="2" scope="row" >Ingresar solicitud</th>
    <td>
    <div class="custom-control custom-checkbox">
    <input type="checkbox" class="custom-control-input" id="IngresarSolicitud">
    <label class="custom-control-label" for="IngresarSolicitud"></label>
    </div>
    </td>
    <td >17/01/2018</td>
    <td >17/01/2018</td>
    <td >17/01/2018</td>
    <td ><input type="text" class="form-control"> </input></td>
    <th bgcolor="LightGreen">OK</th>
    <td>0</td>
</tr>
<tr>
    <th colspan="2" scope="row" >Obtener certificado</th>
    <td>
    <div class="custom-control custom-checkbox">
    <input type="checkbox" class="custom-control-input" id="ObtenerCertificado">
    <label class="custom-control-label" for="ObtenerCertificado"></label>
    </div>
    </td>
    <td >17/01/2018</td>
    <td >22/01/2018</td>
    <td >18/01/2018</td>
    <td ><input type="text" class="form-control"> </input></td>
    <th bgcolor="LightGreen">OK</th>
    <td>0</td>
</tr>
<!-- _______________________________________________________________________________ -->
<tr>
    <th class="bg-info" colspan="2" scope="row" >Avalúo </th>
    <td class="bg-info">
    <div class="custom-control custom-checkbox">
    <input type="checkbox" class="custom-control-input" id="Avaluo">
    <label class="custom-control-label" for="Avaluo"></label>
    </div>
    </td>
    <td class="bg-info">16/01/2018</td>
    <td class="bg-info">25/01/2018</td>
    <td class="bg-info">25/01/2018</td>
    <td class="bg-info"><input type="text" class="form-control"> </input></td>
    <th bgcolor="LightGreen">OK</th>
    <td>0</td>
</tr>
<tr>
    <th colspan="2" scope="row" >Solicitar pre avalúo  </th>
    <td>
    <div class="custom-control custom-checkbox">
    <input type="checkbox" class="custom-control-input" id="SolicitarPreAvaluo  ">
    <label class="custom-control-label" for="SolicitarPreAvaluo"></label>
    </div>
    </td>
    <td >16/01/2018</td>
    <td >19/01/2018</td>
    <td >19/01/2018</td>
    <td ><input type="text" class="form-control"> </input></td>
    <th bgcolor="LightGreen">OK</th>
    <td>0</td>
</tr>
<tr>
    <th colspan="2" scope="row" >Solicitar avalúo  en firme</th>
    <td>
    <div class="custom-control custom-checkbox">
    <input type="checkbox" class="custom-control-input" id="SolicitarAvaluoFirme  ">
    <label class="custom-control-label" for="SolicitarAvaluoFirme"></label>
    </div>
    </td>
    <td >22/01/2018</td>
    <td >22/01/2018</td>
    <td >22/01/2018</td>
    <td ><input type="text" class="form-control"> </input></td>
    <th bgcolor="LightGreen">OK</th>
    <td>0</td>
</tr>
<tr>
    <th colspan="2" scope="row" >Obtener avalúo</th>
    <td>
    <div class="custom-control custom-checkbox">
    <input type="checkbox" class="custom-control-input" id="ObtenerAvaluo  ">
    <label class="custom-control-label" for="ObtenerAvaluo"></label>
    </div>
    </td>
    <td >22/01/2018</td>
    <td >25/01/2018</td>
    <td >25/01/2018</td>
    <td ><input type="text" class="form-control"> </input></td>
    <th bgcolor="LightGreen">OK</th>
    <td>0</td>
</tr>
<!-- _______________________________________________________________________________ -->
<tr>
    <th class="bg-info" colspan="2" scope="row" >Cotización  </th>
    <td class="bg-info">
    <div class="custom-control custom-checkbox">
    <input type="checkbox" class="custom-control-input" id="Cotizacion">
    <label class="custom-control-label" for="Cotizacion"></label>
    </div>
    </td>
    <td class="bg-info">22/01/2018</td>
    <td class="bg-info">22/01/2018</td>
    <td class="bg-info">22/01/2018</td>
    <td class="bg-info"><input type="text" class="form-control"> </input></td>
    <th bgcolor="LightGreen">OK</th>
    <td>0</td>
</tr>
<!-- _________________________________________________________________________________________ -->
<table class="table table-bordered">
    <thead>
        <tr>   
            <th colspan="12"><center><h4>PROYECTO</h4></center></th>
        </tr>
    </thead>
    <tbody>
    <tr>
        <th colspan="2" scope="col"><center>RUBRO </center></th>
        <th scope="col"></th>
        <th scope="col" >FECHA DE INICIO</th>
        <th scope="col">FECHA DE ENTREGA</th>
        <th scope="col">FECHA DE TÉRMINO</th>
        <th scope="col">COMENTARIOS</th>
        <th scope="col">SEMAFORO</th>
        <th scope="col">DIAS ANTES/RETRASO</th>
    </tr>
    <tr>
        <th colspan="2" scope="row" >Elaboración </th>
        <td>
        <div class="custom-control custom-checkbox">
        <input type="checkbox" class="custom-control-input" id="Elaboracion">
        <label class="custom-control-label" for="Elaboracion"></label>
        </div>
        </td>
        <td >16/01/2018</td>
        <td >24/01/2018</td>
        <td >24/01/2018</td>
        <td ><input type="text" class="form-control"> </input></td>
        <th bgcolor="LightGreen">OK</th>
        <td>0</td>
    </tr>
    <tr>
        <th colspan="2" scope="row" >Revisar </th>
        <td>
        <div class="custom-control custom-checkbox">
        <input type="checkbox" class="custom-control-input" id="Revisar">
        <label class="custom-control-label" for="Revisar"></label>
        </div>
        </td>
        <td >25/01/2018</td>
        <td >25/01/2018</td>
        <td >25/01/2018</td>
        <td ><input type="text" class="form-control"> </input></td>
        <th bgcolor="LightGreen">OK</th>
        <td>0</td>
    </tr>
    <tr>
        <th colspan="2" scope="row" >Autorizar abogado </th>
        <td>
        <div class="custom-control custom-checkbox">
        <input type="checkbox" class="custom-control-input" id="AutorizarAbogado">
        <label class="custom-control-label" for="AutorizarAbogado"></label>
        </div>
        </td>
        <td >26/01/2018</td>
        <td >26/01/2018</td>
        <td >26/01/2018</td>
        <td ><input type="text" class="form-control"> </input></td>
        <th bgcolor="LightGreen">OK</th>
        <td>0</td>
    </tr>
    <tr>
        <th colspan="2" scope="row" >Autorizar notario </th>
        <td>
        <div class="custom-control custom-checkbox">
        <input type="checkbox" class="custom-control-input" id="AutorizarNotario">
        <label class="custom-control-label" for="AutorizarNotario"></label>
        </div>
        </td>
        <td >29/01/2018</td>
        <td >30/01/2018</td>
        <td >30/01/2018</td>
        <td ><input type="text" class="form-control"> </input></td>
        <th bgcolor="LightGreen">OK</th>
        <td>0</td>
    </tr>
    
        </tbody>
        </table>
    {{-- fin proyecto --}}
    <div class="row">
    <div class="col-md-4">
    </div>
    <div class="col-md-4">
    <button id="" type="submit" class="btn btn-success">Guardar</button>
    <button id="" type="submit" class="btn btn-warning">Actualizar </button>
    </div>
    <div class="col-md-4">
    </div>
    </div>
    <br>
<!-- _________________________________________________________________________________________ -->
<table class="table table-bordered">
<thead>
    <tr>   
        <th colspan="12"><center><h4>FIRMA</h4></center></th>
    </tr>
</thead>
<tbody>
        <tr>
                <td class="bg-info"></td>
                <td class="bg-info"> </td>
                <th class="bg-info">SEMAFORO</th>
            </tr>
<tr>
    <td >Número de escritura</td>
    <td >  </td>
    <th bgcolor="Yellow">PENDIENTE</th>
</tr>
<tr>
    <td >Libro </td>
    <td >  </td>
    <th bgcolor="Yellow">PENDIENTE</th>
</tr>
<tr>
    <td >Folios </td>
    <td >  </td>
    <th bgcolor="Yellow">PENDIENTE</th>
</tr>
<tr>
    <td >Fecha de pase </td>
    <td >07/02/2018</td>
    <th bgcolor="LightGreen">OK</th>
</tr>
<tr>
    <td >Fecha de firma </td>
    <td >09/02/2018  </td>
    <th bgcolor="LightGreen">OK</th>
</tr>
<tr>
    <td >Fecha límite de firma </td>
    <td >09/03/2018  </td>
    <th bgcolor="LightGreen">OK</th>
</tr>
<tr>
    <td >Fecha de autorización </td>
    <td >09/03/2018  </td>
    <th bgcolor="LightGreen">OK</th>
</tr>
<tr>
    <td >Número de firmantes</td>
    <td >2 </td>
</tr>
<tr>
    <th class="bg-info" scope="row" >Nombre del firmante</th>
    <th class="bg-info" scope="row" >Correo Electrónico</th>
    <th class="bg-info" scope="row" >Lugar de la firma</th>
    <th class="bg-info" scope="row" >Fecha de firma</th>
    <th scope="col">SEMAFORO</th>
    <th scope="col">DIAS ANTES/RETRASO</th>

</tr>
<tr>
<td ><input type="text" placeholder="Nombre del firmante" class="form-control"> </input></td>
<td ><input type="email" placeholder="Correo Electrónico" class="form-control"> </input></td>
<td ><input type="text" placeholder="Lugar de la firma" class="form-control"> </input></td>
<td >09/02/2018</td>
<th bgcolor="LightGreen">PENDIENTE</th>
<td>0 </td>
</tr>
<tr>
<td ><input type="text" placeholder="Nombre del firmante" class="form-control"> </input></td>
<td ><input type="email" placeholder="Correo Electrónico" class="form-control"> </input></td>
<td ><input type="text" placeholder="Lugar de la firma" class="form-control"> </input></td>
<td >09/02/2018</td>
<th bgcolor="LightGreen">PENDIENTE</th>
<td>0 </td>
</tr>
    </tbody>
    </table>
    <!-- fin firma -->
<div class="row">
    <div class="col-md-4">
    </div>
    <div class="col-md-4">
    <button id="" type="submit" class="btn btn-success">Guardar</button>
    <button id="" type="submit" class="btn btn-warning">Actualizar </button>
    </div>
    <div class="col-md-4">
    </div>
    </div>
    <br>
    <!-- _________________________________________________________________________________________ -->
<table class="table table-bordered">
    <tbody>
    <tr>
        <th colspan="2" scope="col"><center>RUBRO </center></th>
        <th scope="col"></th>
        <th scope="col" >FECHA DE INICIO</th>
        <th scope="col">FECHA DE ENTREGA</th>
        <th scope="col">FECHA DE TÉRMINO</th>
        <th scope="col">COMENTARIOS</th>
        <th scope="col">SEMAFORO</th>
        <th scope="col">DIAS ANTES/RETRASO</th>
    </tr>
    <tr>
        <th colspan="2" scope="row" >Acreditación del pago total  del inmueble </th>
        <td>
        <div class="custom-control custom-checkbox">
        <input type="checkbox" class="custom-control-input" id="AcreditacionPagoTotalInmueble">
        <label class="custom-control-label" for="AcreditacionPagoTotalInmueble"></label>
        </div>
        </td>
        <td >09/02/2018</td>
        <td >12/02/2018</td>
        <td >12/02/2018</td>
        <td ><input type="text" class="form-control"> </input></td>
        <th bgcolor="LightGreen">OK</th>
        <td>0 </td>
    </tr>
    <tr>
        <th colspan="2" scope="row" >Pago de honorarios e impuestos a la Notaría </th>
        <td>
        <div class="custom-control custom-checkbox">
        <input type="checkbox" class="custom-control-input" id="PagoHonorarios">
        <label class="custom-control-label" for="PagoHonorarios"></label>
        </div>
        </td>
        <td >09/02/2018</td>
        <td >13/02/2018</td>
        <td >13/02/2018</td>
        <td ><input type="text" class="form-control"> </input></td>
        <th bgcolor="LightGreen">OK</th>
        <td>0 </td>
    </tr>
        </tbody>
        </table>
<!-- _________________________________________________________________________________________ -->
<table class="table table-bordered">
<thead>
    <tr>   
        <th colspan="12"><center><h3>POSTFIRMA</h3></center></th>
    </tr>
</thead>
<tbody>
<tr>
    <th colspan="2" scope="col"><center>RUBRO </center></th>
    <th scope="col"></th>
    <th scope="col" >FECHA DE INICIO</th>
    <th scope="col">FECHA DE ENTREGA</th>
    <th scope="col">FECHA DE TÉRMINO</th>
    <th scope="col">COMENTARIOS</th>
    <th scope="col">SEMAFORO</th>
    <th scope="col">DIAS ANTES/RETRASO</th>
</tr>
<tr>
    <th colspan="2" scope="row" >Aviso de otorgamiento </th>
    <td>
    <div class="custom-control custom-checkbox">
    <input type="checkbox" class="custom-control-input" id="AvisoOtorgamiento">
    <label class="custom-control-label" for="AvisoOtorgamiento"></label>
    </div>
    </td>
    <td >12/03/2018</td>
    <td >12/03/2018</td>
    <td >12/03/2018</td>
    <td ><input type="text" class="form-control"> </input></td>
    <th bgcolor="LightGreen">OK</th>
    <td>0 </td>
</tr>
{{-- _______________________________________________________________________________________ --}}
<thead>
    <tr>   
        <th colspan="12"><center><h4>PAGO DE IMPUESTOS</h4></center></th>
    </tr>
</thead>
<tbody>
<tr>
    <th colspan="2" scope="col"><center>RUBRO </center></th>
    <th scope="col"></th>
    <th scope="col" >FECHA DE INICIO</th>
    <th scope="col">FECHA DE ENTREGA</th>
    <th scope="col">FECHA DE TÉRMINO</th>
    <th scope="col">COMENTARIOS</th>
    <th scope="col">SEMAFORO</th>
    <th scope="col">DIAS ANTES/RETRASO</th>
</tr>
<tr>
    <th class="bg-info" colspan="2" scope="row" >Impuestos totales </th>
    <td class="bg-info">
    <div class="custom-control custom-checkbox">
    <input type="checkbox" class="custom-control-input" id="AvisoOtorgamiento">
    <label class="custom-control-label" for="AvisoOtorgamiento"></label>
    </div>
    </td>
    <td class="bg-info">12/03/2018</td>
    <td class="bg-info">23/03/2018</td>
    <td class="bg-info">23/03/2018</td>
    <td class="bg-info"><input type="text" class="form-control"> </input></td>
    <th bgcolor="LightGreen">OK</th>
    <td>0 </td>
</tr>
<tr>
    <th colspan="2" scope="row" >Impuesto sobre Adquisición de Inmuebles (ISAI)</th>
    <td>
    <div class="custom-control custom-checkbox">
    <input type="checkbox" class="custom-control-input" id="ISAI">
    <label class="custom-control-label" for="ISAI"></label>
    </div>
    </td>
    <td>12/03/2018</td>
    <td>23/03/2018</td>
    <td>23/03/2018</td>
    <td><input type="text" class="form-control"> </input></td>
    <th bgcolor="LightGreen">OK</th>
    <td>0 </td>
</tr>
<tr>
    <th colspan="2" scope="row" >ISR por enajenación de inmuebles</th>
    <td>
    <div class="custom-control custom-checkbox">
    <input type="checkbox" class="custom-control-input" id="ISRenajenacion">
    <label class="custom-control-label" for="ISRenajenacion"></label>
    </div>
    </td>
    <td>12/03/2018</td>
    <td>23/03/2018</td>
    <td>23/03/2018</td>
    <td><input type="text" class="form-control"> </input></td>
    <th bgcolor="LightGreen">OK</th>
    <td>0 </td>
</tr>
<tr>
    <th colspan="2" scope="row" >ISR por adquisición de inmuebles</th>
    <td>
    <div class="custom-control custom-checkbox">
    <input type="checkbox" class="custom-control-input" id="ISRadquisicion">
    <label class="custom-control-label" for="ISRadquisicion"></label>
    </div>
    </td>
    <td>12/03/2018</td>
    <td>23/03/2018</td>
    <td>23/03/2018</td>
    <td><input type="text" class="form-control"> </input></td>
    <th bgcolor="LightGreen">OK</th>
    <td>0 </td>
</tr>
<tr>
    <th colspan="2" scope="row" >Impuesto al Valor Agregado</th>
    <td>
    <div class="custom-control custom-checkbox">
    <input type="checkbox" class="custom-control-input" id="ImpuestoValorAgregado">
    <label class="custom-control-label" for="ImpuestoValorAgregado"></label>
    </div>
    </td>
    <td>12/03/2018</td>
    <td>23/03/2018</td>
    <td>23/03/2018</td>
    <td><input type="text" class="form-control"> </input></td>
    <th bgcolor="LightGreen">OK</th>
    <td>0 </td>
</tr>
<tr>
    <th colspan="2" scope="row" >Traslado de dominio</th>
    <td>
    <div class="custom-control custom-checkbox">
    <input type="checkbox" class="custom-control-input" id="TrasladoDominio">
    <label class="custom-control-label" for="TrasladoDominio"></label>
    </div>
    </td>
    <td>12/03/2018</td>
    <td>23/03/2018</td>
    <td>23/03/2018</td>
    <td><input type="text" class="form-control"> </input></td>
    <th bgcolor="LightGreen">OK</th>
    <td>0 </td>
</tr>
{{-- ______________________________________________________________________________ --}}
<tr>
    <th class="bg-info" colspan="12" scope="row" >Informativas</th>
</tr>
<tr>
    <th colspan="2" scope="row" >Declaración informativa a la UIF</th>
    <td>
    <div class="custom-control custom-checkbox">
    <input type="checkbox" class="custom-control-input" id="UIF">
    <label class="custom-control-label" for="UIF"></label>
    </div>
    </td>
    <td>26/03/2018</td>
    <td>02/04/2018</td>
    <td>02/04/2018</td>
    <td><input type="text" class="form-control"> </input></td>
    <th bgcolor="LightGreen">OK</th>
    <td>0 </td>
</tr>
<tr>
    <th colspan="2" scope="row" >Declaranot</th>
    <td>
    <div class="custom-control custom-checkbox">
    <input type="checkbox" class="custom-control-input" id="Declaranot">
    <label class="custom-control-label" for="Declaranot"></label>
    </div>
    </td>
    <td>26/03/2018</td>
    <td>02/04/2018</td>
    <td>02/04/2018</td>
    <td><input type="text" class="form-control"> </input></td>
    <th bgcolor="LightGreen">OK</th>
    <td>0 </td>
</tr>
<tr>
    <th colspan="2" scope="row" >Sigapred</th>
    <td>
    <div class="custom-control custom-checkbox">
    <input type="checkbox" class="custom-control-input" id="Sigapred">
    <label class="custom-control-label" for="Sigapred"></label>
    </div>
    </td>
    <td>26/03/2018</td>
    <td>02/04/2018</td>
    <td>02/04/2018</td>
    <td><input type="text" class="form-control"> </input></td>
    <th bgcolor="LightGreen">OK</th>
    <td>0 </td>
</tr>
{{-- ______________________________________________________________________________ --}}
<tr>
    <th class="bg-info" colspan="2" scope="col"><center>Testimonios </center></th>
    <th class="bg-info" scope="col"></th>
    <th class="bg-info" scope="col" >FECHA DE INICIO</th>
    <th class="bg-info" scope="col">FECHA DE ENTREGA</th>
    <th class="bg-info" scope="col">FECHA DE TÉRMINO</th>
    <th class="bg-info" scope="col">COMENTARIOS</th>
    <th class="bg-info" scope="col">SEMAFORO</th>
    <th class="bg-info" scope="col">DIAS ANTES/RETRASO</th>
</tr>
<tr>
    <th colspan="2" scope="row" >Notas complementarias, expedición de testimonio y apéndice<br> (Empieza cuando se informa la ultima declaración)</th>
    <td>
    <div class="custom-control custom-checkbox">
    <input type="checkbox" class="custom-control-input" id="Sigapred">
    <label class="custom-control-label" for="Sigapred"></label>
    </div>
    </td>
    <td>02/04/2018</td>
    <td>05/04/2018</td>
    <td>05/04/2018</td>
    <td><input type="text" class="form-control"> </input></td>
    <th bgcolor="LightGreen">OK</th>
    <td>0 </td>
</tr>
<tr>
    <th colspan="2" scope="row" >Firma del notario</th>
    <td>
    <div class="custom-control custom-checkbox">
    <input type="checkbox" class="custom-control-input" id="FirmaNotario">
    <label class="custom-control-label" for="FirmaNotario"></label>
    </div>
    </td>
    <td>06/04/2018</td>
    <td>06/04/2018</td>
    <td>06/04/2018</td>
    <td><input type="text" class="form-control"> </input></td>
    <th bgcolor="LightGreen">OK</th>
    <td>0 </td>
</tr>
<tr>
    <th colspan="2" scope="row" >Ingreso de testimonio al Registro Público de la Propiedad</th>
    <td>
    <div class="custom-control custom-checkbox">
    <input type="checkbox" class="custom-control-input" id="IngresoTestimonio">
    <label class="custom-control-label" for="IngresoTestimonio"></label>
    </div>
    </td>
    <td>09/04/2018</td>
    <td>08/04/2018</td>
    <td>08/04/2018</td>
    <td><input type="text" class="form-control"> </input></td>
    <th bgcolor="LightGreen">OK</th>
    <td>0 </td>
</tr>
<tr>
    <th colspan="2" scope="row" >Enviar copia al banco y/o desarrollador del ingreso a registro</th>
    <td>
    <div class="custom-control custom-checkbox">
    <input type="checkbox" class="custom-control-input" id="EnviarCopiaBanco">
    <label class="custom-control-label" for="EnviarCopiaBanco"></label>
    </div>
    </td>
    <td>09/04/2018</td>
    <td>09/04/2018</td>
    <td>09/04/2018</td>
    <td><input type="text" class="form-control"> </input></td>
    <th bgcolor="LightGreen">OK</th>
    <td>0 </td>
</tr>
<tr>
    <th colspan="2" scope="row" >Entregar protocolo y apéndice al archivo</th>
    <td>
    <div class="custom-control custom-checkbox">
    <input type="checkbox" class="custom-control-input" id="EntregarProtocolo">
    <label class="custom-control-label" for="EntregarProtocolo"></label>
    </div>
    </td>
    <td>08/04/2018</td>
    <td>08/04/2018</td>
    <td>08/04/2018</td>
    <td><input type="text" class="form-control"> </input></td>
    <th bgcolor="LightGreen">OK</th>
    <td>0 </td>
</tr>
<tr>
    <th colspan="2" scope="row" >Testimonio inscrito</th>
    <td>
    <div class="custom-control custom-checkbox">
    <input type="checkbox" class="custom-control-input" id="TestimonioInscrito">
    <label class="custom-control-label" for="TestimonioInscrito"></label>
    </div>
    </td>
    <td>08/04/2018</td>
    <td>23/05/2018</td>
    <td>23/05/2018</td>
    <td><input type="text" class="form-control"> </input></td>
    <th bgcolor="LightGreen">OK</th>
    <td>0 </td>
</tr>
<tr>
    <th colspan="2" scope="row" >Notas complementarias de testimonio inscrito</th>
    <td>
    <div class="custom-control custom-checkbox">
    <input type="checkbox" class="custom-control-input" id=">NotasComplementariasTestimonioInscrito">
    <label class="custom-control-label" for="NotasComplementariasTestimonioInscrito"></label>
    </div>
    </td>
    <td>24/05/2018</td>
    <td>25/05/2018</td>
    <td>25/05/2018</td>
    <td><input type="text" class="form-control"> </input></td>
    <th bgcolor="LightGreen">OK</th>
    <td>0 </td>
</tr>
<tr>
    <th colspan="2" scope="row" >Notificación al cliente de que está inscrito</th>
    <td>
    <div class="custom-control custom-checkbox">
    <input type="checkbox" class="custom-control-input" id=">NotificacionCliente">
    <label class="custom-control-label" for="NotificacionCliente"></label>
    </div>
    </td>
    <td>28/05/2018</td>
    <td>28/05/2018</td>
    <td>28/05/2018</td>
    <td><input type="text" class="form-control"> </input></td>
    <th bgcolor="LightGreen">OK</th>
    <td>0 </td>
</tr>
<tr>
    <th colspan="2" scope="row" >Entrega del testimonio</th>
    <td>
    <div class="custom-control custom-checkbox">
    <input type="checkbox" class="custom-control-input" id=">EntregaTestimonio">
    <label class="custom-control-label" for="EntregaTestimonio"></label>
    </div>
    </td>
    <td>28/05/2018</td>
    <td>07/06/2018</td>
    <td>07/06/2018</td>
    <td><input type="text" class="form-control"> </input></td>
    <th bgcolor="LightGreen">OK</th>
    <td>0 </td>
</tr>
    </tbody>
    </table>
{{-- fin postfirma --}}
<div class="row">
<div class="col-md-4">
</div>
<div class="col-md-4">
<button id="" type="submit" class="btn btn-success">Guardar</button>
<button id="" type="submit" class="btn btn-warning">Actualizar </button>
<button id="" type="submit" class="btn btn-danger">Eliminar </button>
</div>
<div class="col-md-4">
</div>
</div>
<br>
 <!-- _________________________________________________________________________________________ -->
@endsection 

@section('content_js')
{{-- <script src="{{ asset('js/fichasCaptura.js') }}"></script> --}}
@stop 