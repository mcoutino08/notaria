@extends('layouts.metronic') @section('Activo')
<a href="/expedientes">
    <span class="selected"> </span>
</a>
@endsection @section('content')
<div id="replacespace">
<table id="tablaFichasCaptura" class="table table-bordered">
        <h1> Expedientes </h1>
       <thead>
         <tr>
         <td colspan="12"><a class="btn btn-success"  href="/expedientes_captura">Agregar Expediente</a></td>
         </tr>
    <tr>
      <th>Expediente #</th>
      <th>Módulo</th>
      <th>Fecha de Captura</th>
      <th>Ficha</th>
      <th>Avances</th>
    </tr>
  </thead>   
  
    <tbody>
    @foreach ( $TOEDIT as $t )
    <tr>
      <td class="uppercase">{{$t->id}}</td>
      <td class="uppercase">{{$t->Modulo}}</td>
      <td class="uppercase">{{$t->FechaCaptura}}</td>
      <td><a id="fichaEdit" class="btn btn-success">Ficha</a></td>
      <td><a class="btn btn-warning" href="/documentosIntegracion">Avances</a></td>
    </tr>
    @endforeach
  </tbody>
</table>
</div>
@endsection

@section('content_js')
<script src="{{ asset('js/fichasCaptura.js') }}"></script>
<script src="{{ asset('js/jquery.bootstrap-growl.min.js') }}"></script>
@stop
