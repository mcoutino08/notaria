@extends('layouts.metronic') @section('Activo')
<a href="/expedientes">
    <span class="selected"> </span>
</a>

@endsection @section('content')
  

<div class="portlet box yellow">
    <div class="portlet-title">
        <div class="caption">
            <i class="fa fa-gift"></i>Editar Tramite</div>
        <div class="tools">
            <a href="javascript:;" class="collapse" data-original-title="" title=""> </a>
            <a href="#portlet-config" data-toggle="modal" class="config" data-original-title="" title=""> </a>
        </div>
    </div>
    <div class="portlet-body">
        <div class="tabbable-custom ">
            <ul class="nav nav-tabs ">
                <li class="active">
                    <a href="#generales" data-toggle="tab" aria-expanded="true">Datos Editables</a>
                </li>

            <div class="tab-content">
                <div class="tab-pane active" id="generales">

                    <div class="portlet-title">
                        <div class="caption">
                            <span class="caption-subject font-green bold uppercase">
                                <h3>
                                    <center>DATOS GENERALES</center>
                                </h3>
                            </span>
                            <br>
                        </div>
                    </div>
                    <form method="POST" id="DatosGeneralesInmuebleEdit">
                        {{ csrf_field() }}
                        @forelse($TOEDIT as $edit)                         
                            <input style="display:none;" value="{{$edit->id}}" name="idTramite" type="text">
                        @empty 
                        @endforelse
                        <div class="portlet-body">
                            <div class="mt-element-card mt-element-overlay">
                                <!-- ___________________________________________________________________________________ -->
                                <div class="row">
                                    <div class="col-md-3">
                                        <label class="control-label">Abogado titular </label>
                                        <select required name="AbogadoT" class="form-control">
                                            <option value="">Default select</option>
                                            <option value="1">José Antonio Meade</option>
                                        </select>
                                    </div>

                                    <div class="col-md-3">
                                        <label class="control-label">Correo electrónico Abogado</label>
                                        <select required name="emailAbogado" class="form-control">
                                            <option value="">Default select</option>
                                            <option value="1"> pruebaEmailAbogado@prueba.com </option>
                                        </select>
                                    </div>
                                </div>
                                <!-- _____________________________________________________________________________ -->
                                <br>
                                <!-- ______________________________________________________________________________- -->
                            </div>
                        </div>
                        <div class="portlet-title">
                            <div class="caption">
                                <span class="caption-subject font-green bold uppercase">
                                    <h4>
                                        <center>DATOS DEL INMUEBLE </center>
                                    </h4>
                                </span>
                                <br>
                            </div>
                        </div>
                            <div class="portlet-body">
                                <div class="mt-element-card mt-element-overlay">
                                    <div class="row">
                                        <div class="col-md-3">
                                            <div class="form-group">
                                                <label for="">Calle del inmueble</label>
                                            @forelse($TOEDIT as $edit)
                                        <input required type="text" name="Calle" value="{{$edit->Calle}}"  class="form-control">
                                    @empty
                                    @endforelse
                                            </div>
                                        </div>
                                        <!-- _____________________________________________________________________________ -->
                                        <div class="col-md-3">
                                            <div class="form-group">
                                                <label for="">Número exterior</label>
                                                 @forelse($TOEDIT as $edit)
                                            <input required type="text" value="{{$edit->NumExt}}" class="form-control" name="NumExt"  placeholder="Número exterior">
                                             @empty
                                            @endforelse
                                            </div>
                                        </div>
                                        <!-- _____________________________________________________________________________ -->
                                        <div class="col-md-3">
                                            <div class="form-group">
                                                <label for="">Número interior</label>
                                                 @forelse($TOEDIT as $edit)
                                                <input type="text" class="form-control" value="{{$edit->NumInt}}" name="NumInt"  placeholder="Número interior">
                                            @empty
                                            @endforelse
                                            </div>
                                        </div>
                                        <!-- _____________________________________________________________________________ -->
                                        <div class="col-md-3">
                                            <div class="form-group">
                                                <label for="">Colonia</label>
                                                @forelse($TOEDIT as $edit)
                                            <input required type="text" value="{{$edit->Colonia}}" class="form-control" name="Colonia"  placeholder="Colonia">
                                            @empty
                                            @endforelse
                                            </div>
                                        </div>
                                        <!-- _____________________________________________________________________________ -->
                                    </div>
                                    <!-- _____________________________________________________________________________ -->
                                    <div class="row">
                                        <div class="col-md-3">
                                            <div class="form-group">
                                                <label for="">Código postal</label>
                                                     @forelse($TOEDIT as $edit)
                                            <input required type="text" class="form-control" value="{{$edit->CP}}" name="CP"  placeholder="Código postal">
                                            @empty
                                            @endforelse
                                            </div>
                                        </div>
                                        <!-- _____________________________________________________________________________ -->
                                        <div class="col-md-3">
                                            <div class="form-group">
                                                <label for="">Municipio</label>
                                           @forelse($TOEDIT as $edit)
                                            <input type="text" class="form-control" value="{{$edit->municipio}}" name="municipio"  placeholder="Municipio">
                                            @empty
                                            @endforelse
                                            </div>
                                        </div>
                                        <!-- _____________________________________________________________________________ -->
                                        <div class="col-md-3">
                                            <div class="form-group">
                                                <label for="">Valor catastral</label>
                                            @forelse($TOEDIT as $edit)
                                         <input required type="text" value="{{$edit->ValorCatastral}}" class="form-control" name="ValorCatastral"  placeholder="Valor" >
                                            @empty
                                            @endforelse
                                            </div>
                                        </div>
                                        <!-- _____________________________________________________________________________ -->
                                        <div class="col-md-3">
                                            <div class="form-group">
                                                <label for="">Valor avalúo</label>
                                                @forelse($TOEDIT as $edit)
                                            <input required type="text" value="{{$edit->ValorAvaluo}}" class="form-control" name="ValorAvaluo"  placeholder="Valor avalúo">
                                            @empty
                                            @endforelse
                                            </div>
                                        </div>
                                        <!-- _____________________________________________________________________________ -->
                                    </div>
                                    <!-- _____________________________________________________________________________ -->
                                    <div class="row">
                                        <div class="col-md-3">
                                            <div class="form-group">
                                                <label for="">Valor venta</label>
                                          @forelse($TOEDIT as $edit)
                                            <input required type="text" class="form-control" value="{{$edit->ValorVenta}}" name="ValorVenta"  placeholder="Valor venta">
                                            @empty
                                            @endforelse
                                            </div>
                                        </div>
     
                                        <!-- _____________________________________________________________________________ -->
                                        <div class="col-md-3">
                                            <label class="control-label">Tipo de operación</label>
                                            <select required name="TipoOperacion" class="form-control">
                                            @forelse($TOEDIT as $edit)                         
                                            <option value="{{$edit->idTipoOperacion}}" selected>{{$edit->TipoOperacion}} </option>
                                            @empty 
                                            @endforelse
                                            @forelse($tipo_operacion as $tipoOpe)
                                            <option value="{{$tipoOpe->id}}">{{$tipoOpe->TipoOperacion}}</option>
                                            @empty @endforelse
                                            </select>
                                        </div>
                                        <!-- _____________________________________________________________________________ -->
                                        <div class="col-md-3">
                                            <label class="control-label">Desarrollador</label>
                                            <select required name="Desarrollador" class="form-control">
                                            @forelse($TOEDIT as $edit)                         
                                            <option value="{{$edit->idDesarrollador}}" selected>{{$edit->Desarrollador}} </option>
                                            @empty 
                                            @endforelse
                                            @forelse($desarrollador as $des)
                                            <option value="{{$des->id}}">{{$des->Desarrollador}}</option>
                                            @empty @endforelse
                                            </select>
                                        </div>
                                        <!-- _____________________________________________________________________________ -->
                                            <div class="col-md-3">
                                            <label class="control-label">Banco</label>
                                            <select required name="Banco" class="form-control">
                                             @forelse($TOEDIT as $edit)                         
                                            <option value="{{$edit->idBanco}}" selected>{{$edit->Banco}} </option>
                                            @empty 
                                            @endforelse
                                            @forelse($bancos as $ban)
                                            <option value="{{$ban->id}}">{{$ban->Banco}}</option>
                                            @empty @endforelse
                                            </select>
                                        </div>
                                    </div>
                                    <!-- _____________________________________________________________________________ -->
                                    <br>
                                    
                                    <div class="portlet-title">
                                        <div class="caption">
                                        <span class="caption-subject font-green bold uppercase">
                                          <h4><center>DATOS DEL COMPRADOR</center></h4>
                                        </span>
                                        <br>
                                    </div>
                                </div>
                                    <div class="row">
                                    <div class="col-md-3">
                                        <div class="form-group">
                                            <label for="">Nombre </label>
                                            @forelse($TOEDIT as $edit)
                                        <input required type="text" value="{{$edit->Nombre}}" class="form-control" name="Nombre"  placeholder="Nombre ">
                                       @empty
                                       @endforelse
                                    </div>
                                    </div>
                                    <!-- _____________________________________________________________________________ -->
                                    <div class="col-md-3">
                                        <div class="form-group">
                                            <label for="">Domicilio</label>
                                            @forelse($TOEDIT as $edit)
                                        <input required type="text" value="{{$edit->Domicilio}}" class="form-control" name="Domicilio"  placeholder="Domicilio" required>
                                     @empty
                                     @endforelse    
                                    </div>
                                    </div>
                                    <!-- _____________________________________________________________________________ -->
                                    <div class="col-md-3">
                                        <div class="form-group">
                                            <label for="">Correo electrónico </label>
                                            @forelse($TOEDIT as $edit)
                                            <input type="text" value="{{$edit->CorreoElectronico}}" class="form-control" name="CorreoElectronico"  placeholder="Correo electrónico">
                                        @empty
                                        @endforelse
                                        </div>
                                    </div>
                                    <!-- _____________________________________________________________________________ -->
                                    <div class="col-md-3">
                                        <div class="form-group">
                                            <label for="">Teléfono casa </label>
                                            @forelse($TOEDIT as $edit)
                                        <input type="text" value="{{$edit->TelCasa}}" class="form-control" name="TelCasa"  placeholder="Teléfono casa">
                                    @empty
                                    @endforelse    
                                    </div>
                                    </div>
                                    </div>
                                    <!-- _____________________________________________________________________________ -->
                                <div class="row">
                                    <div class="col-md-3">
                                        <div class="form-group">
                                            <label for="">Teléfono oficina </label>
                                            @forelse($TOEDIT as $edit)
                                        <input type="text" value="{{$edit->TelOficina}}" class="form-control" name="TelOficina"  placeholder="Teléfono oficina">
                                      @empty
                                      @endforelse  
                                    </div>
                                    </div>
                                    <!-- _____________________________________________________________________________ -->
                                    <div class="col-md-3">
                                        <div class="form-group">
                                            <label for="">Extensión</label>
                                            @forelse($TOEDIT as $edit)
                                        <input type="text" value="{{$edit->Extension}}" class="form-control" name="Extension"  placeholder="Extensión">
                                        @empty
                                        @endforelse
                                    </div>
                                    </div>
                                    <!-- _____________________________________________________________________________ -->
                                    <div class="col-md-3">
                                        <div class="form-group">
                                            <label for="">Celular </label>
                                            @forelse($TOEDIT as $edit)
                                        <input required type="text" value="{{$edit->Celular}}" class="form-control" name="Celular"  placeholder="Celular">
                                        @empty
                                        @endforelse
                                    </div>
                                    </div>
                                    <!-- _____________________________________________________________________________ -->
                                    <div class="col-md-3">
                                        <div class="form-group">
                                            <label for="">Lugar de nacimiento </label>
                                            @forelse($TOEDIT as $edit)
                                        <input type="text" value="{{$edit->LugarNacimiento}}" class="form-control" name="LugarNacimiento" placeholder="Lugar de nacimiento">
                                        @empty
                                        @endforelse
                                        </div>
                                    </div>
                                </div>
                                    <!-- _____________________________________________________________________________ -->
                                    <div class="row">
                                <div class="col-md-3">
                                        <label class="control-label">Fecha de nacimiento</label>
                                        <br>
                                        @forelse($TOEDIT as $edit)
                                        <input class="form-control" value="{{$edit->FechaNacimiento}}" type="date" name="FechaNacimiento">
                                        @empty
                                        @endforelse
                                    </div>
                                        <!-- _____________________________________________________________________________ -->
                                    <div class="col-md-3">
                                        <div class="form-group">
                                            <label for="">CURP </label>
                                            @forelse($TOEDIT as $edit)
                                        <input  pattern=".{18,}" value="{{$edit->CURP}}" title="Se requieren 18 caracteres"  maxlength="18" required type="text" class="form-control" name="CURP" placeholder="CURP">
                                        @empty
                                        @endforelse
                                        </div>
                                    </div>
                                    <!-- _____________________________________________________________________________ -->
                                    <div class="col-md-3">
                                        <div class="form-group">
                                            <label for="">RFC </label>
                                            @forelse($TOEDIT as $edit)
                                        <input  pattern=".{10,}" value="{{$edit->RFC}}" title="Se requieren 10 caracteres"  maxlength="10" type="text" class="form-control" name="RFC"  placeholder="RFC">
                                       @empty
                                       @endforelse
                                    </div>
                                    </div>
                                    <!-- _____________________________________________________________________________ -->
                                    <div class="col-md-3">
                                        <label class="control-label">Tipo de identificación</label>
                                        <select required name="TipoIdentificacion" class="form-control">
                                            @forelse($TOEDIT as $edit)
                                        <option value="{{$edit->idTipoIdentificacion}}">{{$edit->TipoIdentificacion}}</option>
                                        @empty
                                        @endforelse
                                            @forelse($tipo_iodentificacion as $tipoiod)
                                            <option value="{{$tipoiod->id}}">{{$tipoiod->TipoIdentificacion}}</option>
                                            @empty @endforelse
                                        </select>
                                    </div>
                                </div>
                            <!-- _____________________________________________________________________________ -->
                                <div class="row">
                                    <div class="col-md-3">
                                        <div class="form-group">
                                            <label for="">Número de identificación </label>
                                            @forelse($TOEDIT as $edit)
                                        <input type="text" value="{{$edit->NumeroIdentificacion}}" class="form-control" name="NumIdentificacion"  placeholder="Número de identificación">
                                    @empty
                                    @endforelse    
                                    </div>
                                    </div>
                                </div>
                            <!-- _____________________________________________________________________________ -->
                                <div class="portlet-title">
                                        <div class="caption">
                                        <span class="caption-subject font-green bold uppercase">
                                          <h4><center>DATOS DEL VENDEDOR</center></h4>
                                        </span>
                                        <br>
                                    </div>
                                </div>
                                    <div class="row">
                                    <div class="col-md-3">
                                        <div class="form-group">
                                            <label for="">Nombre </label>
                                            @forelse($TOEDIT as $edit)
                                        <input required type="text" value="{{$edit->Nombre}}" class="form-control" name="Nombre"  placeholder="Nombre ">
                                       @empty
                                       @endforelse
                                    </div>
                                    </div>
                                    <!-- _____________________________________________________________________________ -->
                                    <div class="col-md-3">
                                        <div class="form-group">
                                            <label for="">Domicilio</label>
                                            @forelse($TOEDIT as $edit)
                                        <input required type="text" value="{{$edit->Domicilio}}" class="form-control" name="Domicilio"  placeholder="Domicilio" required>
                                     @empty
                                     @endforelse    
                                    </div>
                                    </div>
                                    <!-- _____________________________________________________________________________ -->
                                    <div class="col-md-3">
                                        <div class="form-group">
                                            <label for="">Correo electrónico </label>
                                            @forelse($TOEDIT as $edit)
                                            <input type="text" value="{{$edit->CorreoElectronico}}" class="form-control" name="CorreoElectronico"  placeholder="Correo electrónico">
                                        @empty
                                        @endforelse
                                        </div>
                                    </div>
                                    <!-- _____________________________________________________________________________ -->
                                    <div class="col-md-3">
                                        <div class="form-group">
                                            <label for="">Teléfono casa </label>
                                            @forelse($TOEDIT as $edit)
                                        <input type="text" value="{{$edit->TelCasa}}" class="form-control" name="TelCasa"  placeholder="Teléfono casa">
                                    @empty
                                    @endforelse    
                                    </div>
                                    </div>
                                    </div>
                                    <!-- _____________________________________________________________________________ -->
                                <div class="row">
                                    <div class="col-md-3">
                                        <div class="form-group">
                                            <label for="">Teléfono oficina </label>
                                            @forelse($TOEDIT as $edit)
                                        <input type="text" value="{{$edit->TelOficina}}" class="form-control" name="TelOficina"  placeholder="Teléfono oficina">
                                      @empty
                                      @endforelse  
                                    </div>
                                    </div>
                                    <!-- _____________________________________________________________________________ -->
                                    <div class="col-md-3">
                                        <div class="form-group">
                                            <label for="">Extensión</label>
                                            @forelse($TOEDIT as $edit)
                                        <input type="text" value="{{$edit->Extension}}" class="form-control" name="Extension"  placeholder="Extensión">
                                        @empty
                                        @endforelse
                                    </div>
                                    </div>
                                    <!-- _____________________________________________________________________________ -->
                                    <div class="col-md-3">
                                        <div class="form-group">
                                            <label for="">Celular </label>
                                            @forelse($TOEDIT as $edit)
                                        <input required type="text" value="{{$edit->Celular}}" class="form-control" name="Celular"  placeholder="Celular">
                                        @empty
                                        @endforelse
                                    </div>
                                    </div>
                                    <!-- _____________________________________________________________________________ -->
                                    <div class="col-md-3">
                                        <div class="form-group">
                                            <label for="">Lugar de nacimiento </label>
                                            @forelse($TOEDIT as $edit)
                                        <input type="text" value="{{$edit->LugarNacimiento}}" class="form-control" name="LugarNacimiento" placeholder="Lugar de nacimiento">
                                        @empty
                                        @endforelse
                                        </div>
                                    </div>
                                </div>
                                    <!-- _____________________________________________________________________________ -->
                                    <div class="row">
                                <div class="col-md-3">
                                        <label class="control-label">Fecha de nacimiento</label>
                                        <br>
                                        @forelse($TOEDIT as $edit)
                                        <input class="form-control" value="{{$edit->FechaNacimiento}}" type="date" name="FechaNacimiento">
                                        @empty
                                        @endforelse
                                    </div>
                                        <!-- _____________________________________________________________________________ -->
                                    <div class="col-md-3">
                                        <div class="form-group">
                                            <label for="">CURP </label>
                                            @forelse($TOEDIT as $edit)
                                        <input  pattern=".{18,}" value="{{$edit->CURP}}" title="Se requieren 18 caracteres"  maxlength="18" required type="text" class="form-control" name="CURP" placeholder="CURP">
                                        @empty
                                        @endforelse
                                        </div>
                                    </div>
                                    <!-- _____________________________________________________________________________ -->
                                    <div class="col-md-3">
                                        <div class="form-group">
                                            <label for="">RFC </label>
                                            @forelse($TOEDIT as $edit)
                                        <input  pattern=".{10,}" value="{{$edit->RFC}}" title="Se requieren 10 caracteres"  maxlength="10" type="text" class="form-control" name="RFC"  placeholder="RFC">
                                       @empty
                                       @endforelse
                                    </div>
                                    </div>
                                    <!-- _____________________________________________________________________________ -->
                                    <div class="col-md-3">
                                        <label class="control-label">Tipo de identificación</label>
                                        <select required name="TipoIdentificacion" class="form-control">
                                            @forelse($TOEDIT as $edit)
                                        <option value="{{$edit->idTipoIdentificacion}}">{{$edit->TipoIdentificacion}}</option>
                                        @empty
                                        @endforelse
                                            @forelse($tipo_iodentificacion as $tipoiod)
                                            <option value="{{$tipoiod->id}}">{{$tipoiod->TipoIdentificacion}}</option>
                                            @empty @endforelse
                                        </select>
                                    </div>
                                </div>
                            <!-- _____________________________________________________________________________ -->
                                <div class="row">
                                    <div class="col-md-3">
                                        <div class="form-group">
                                            <label for="">Número de identificación </label>
                                            @forelse($TOEDIT as $edit)
                                        <input type="text" value="{{$edit->NumeroIdentificacion}}" class="form-control" name="NumIdentificacion"  placeholder="Número de identificación">
                                    @empty
                                    @endforelse    
                                    </div>
                                    </div>
                                </div>    
                    <!-- _____________________________________________________________________________ -->

                                    <div class="row">
                                        <center>
                                            <a  href="/expedientes" type="" class="btn btn-primary">Regresar</a>
                                            <button  type="submit" class="btn btn-success">Actualizar</button>
                                        </center>
                                    </div>
                                </div>
                            </div>
                            

                    <!-- ______________________________________________________________________________- -->
                    </form>
                </div>
                <!-- _____________________________________________________________________________ -->


                {{-- ------------------------------------------------------------------------------------------- --}}

            </div>
        </div>
    </div>
</div>

@endsection @section('content_js')
<script src="{{asset('js/expediente_add.js')}}"></script>
<script src="{{ asset('js/fichasCaptura.js') }}"></script>
<script src="{{ asset('js/jquery.bootstrap-growl.min.js') }}"></script>
@stop
