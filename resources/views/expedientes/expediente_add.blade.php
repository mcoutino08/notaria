@extends('layouts.metronic') @section('Activo')
<a href="/expedientes_captura">
    <span class="selected"> </span>
</a>

@endsection @section('content')

<div class="portlet box yellow">
    <div class="portlet-title">
        <div class="caption">
            <i class="fa fa-gift"></i>Tramite</div>
        <div class="tools">
            <a href="javascript:;" class="collapse" data-original-title="" title=""> </a>
            <a href="#portlet-config" data-toggle="modal" class="config" data-original-title="" title=""> </a>
        </div>
    </div>
    <div class="portlet-body">
        <div class="tabbable-custom ">
            <ul class="nav nav-tabs ">
                <li class="active">
                    <a href="#generales" data-toggle="tab" aria-expanded="true">Datos Generales e Inmueble</a>
                </li>
                <li class="">
                    <a href="#comprador" data-toggle="tab" aria-expanded="false">Datos del Comprador</a>
                </li>
                <li class="">
                    <a href="#vendedor" data-toggle="tab" aria-expanded="true">Datos del Vendedor </a>
                </li>
                <li class="">
                    <a href="#contactoBanco" data-toggle="tab" aria-expanded="false">Contacto del Banco</a>
                </li>
                <li class="">
                    <a href="#contactoDesarrollador" data-toggle="tab" aria-expanded="false">Contacto del Desarrollador </a>
                </li>
            </ul>

            <div class="tab-content">
                <div class="tab-pane active" id="generales">

                    <div class="portlet-title">
                        <div class="caption">
                            <span class="caption-subject font-green bold uppercase">
                                <h3>
                                    <center>DATOS GENERALES</center>
                                </h3>
                            </span>
                            <br>
                        </div>
                    </div>
                    <form method="POST" action="submit" id="DatosGeneralesInmueble">
                        <div class="portlet-body">
                            <div class="mt-element-card mt-element-overlay">
                                <div class="row">
                                    <div class="col-md-3">
                                        <label  class="control-label">Tipo de trámite </label>
                                        <select required name="TipoTramite" class="form-control">
                                            <option value="">Default select</option>
                                            @forelse($tramites_tipo as $t)
                                            <option value="{{$t->idTipo}}">{{$t->TipoTramite}}</option>
                                            @empty @endforelse
                                        </select>
                                    </div>

                                    <div class="col-md-3">
                                        <label class="control-label">Notaría</label>
                                        <select required name="Notaria" class="form-control">
                                            <option value="">Default select</option>
                                            @forelse($notaria as $n)
                                            <option value="{{$n->id}}">{{$n->Numero}}</option>
                                            @empty @endforelse
                                        </select>
                                    </div>

                                    <div class="col-md-3">
                                        <label class="control-label">Correo electrónico Notario</label>
                                        <select required name="emailNotario" class="form-control">
                                            <option value="">Default select</option>
                                            @forelse($user as $us)
                                        <option value="{{$us->id}}}">{{$us->email}}</option>
                                        @empty @endforelse
                                        </select>
                                    </div>

                                    <div class="col-md-3">
                                        <label class="control-label">Módulo</label>
                                        <select required name="modulo" class="form-control">
                                            <option value="">Default select</option>
                                            @forelse($modulos as $m)
                                            <option value="{{$m->id}}">{{$m->Modulo}}</option>
                                            @empty @endforelse
                                        </select>
                                    </div>
                                </div>
                                <!-- ___________________________________________________________________________________ -->
                                <br>
                                <div class="row">
                                    <div class="col-md-3">
                                        <label class="control-label">Abogado titular </label>
                                        <select required name="AbogadoT" class="form-control">
                                            <option value="">Default select</option>
                                            @forelse($user as $us)
                                        <option value="{{$us->id}}}">{{$us->Nombre}}</option>
                                        @empty @endforelse
                                        </select>
                                    </div>

                                    <div class="col-md-3">
                                        <label class="control-label">Correo electrónico Abogado</label>
                                        <select required name="emailAbogado" class="form-control">
                                            <option value="">Default select</option>
                                            @forelse($user as $us)
                                        <option value="{{$us->id}}}">{{$us->email}}</option>
                                        @empty @endforelse
                                        </select>
                                    </div>

                                    <div class="col-md-3">
                                        <label class="control-label">Fecha de apertura</label>
                                        <br>
                                        <input required type="date" name="FechaApertura" value=""  class="form-control">
                                    </div>

                                    <div class="col-md-3">
                                        <label class="control-label">Fecha de término</label>
                                        <br>
                                        <input required type="date" name="FechaTermino" value=""  class="form-control">
                                    </div>
                                </div>
                                <!-- _____________________________________________________________________________ -->

                                <br>
                                <div class="row">
                                    <div class="col-md-3">
                                        <label class="control-label">Entidad Federativa</label>
                                        <select required name="EntidadFederativa" class="form-control">
                                            <option value="">Default select</option>
                                            @forelse($entidad as $e)
                                            <option value="{{$e->id}}">{{$e->EntidadFederativa}}</option>
                                            @empty @endforelse
                                        </select>
                                    </div>
                                </div>
                                <br>
                                <!-- ______________________________________________________________________________- -->
                            </div>
                        </div>
                        <div class="portlet-title">
                            <div class="caption">
                                <span class="caption-subject font-green bold uppercase">
                                    <h4>
                                        <center>DATOS DEL INMUEBLE </center>
                                    </h4>
                                </span>
                                <br>
                            </div>
                        </div>
                            <div class="portlet-body">
                                <div class="mt-element-card mt-element-overlay">
                                    <div class="row">
                                        <div class="col-md-3">
                                            <div class="form-group">
                                                <label for="">Calle del inmueble</label>
                                                <input required type="text" class="form-control" name="Calle"  placeholder="Calle del inmueble">
                                            </div>
                                        </div>
                                        <!-- _____________________________________________________________________________ -->
                                        <div class="col-md-3">
                                            <div class="form-group">
                                                <label for="">Número exterior</label>
                                                <input required type="text" class="form-control" name="NumExt"  placeholder="Número exterior">
                                            </div>
                                        </div>
                                        <!-- _____________________________________________________________________________ -->
                                        <div class="col-md-3">
                                            <div class="form-group">
                                                <label for="">Número interior</label>
                                                <input type="text" class="form-control" name="NumInt"  placeholder="Número interior">
                                            </div>
                                        </div>
                                        <!-- _____________________________________________________________________________ -->
                                        <div class="col-md-3">
                                            <div class="form-group">
                                                <label for="">Colonia</label>
                                                <input required type="text" class="form-control" name="Colonia"  placeholder="Colonia">
                                            </div>
                                        </div>
                                        <!-- _____________________________________________________________________________ -->
                                    </div>
                                    <!-- _____________________________________________________________________________ -->
                                    <div class="row">
                                        <div class="col-md-3">
                                            <div class="form-group">
                                                <label for="">Código Postal</label>
                                                <input required type="text" class="form-control" name="CP"  placeholder="Código postal">
                                            </div>
                                        </div>
                                        <!-- _____________________________________________________________________________ -->
                                        <div class="col-md-3">
                                            <div class="form-group">
                                                <label for="">Municipio</label>
                                                <input type="text" class="form-control" name="municipio"  placeholder="Municipio">
                                            </div>
                                        </div>
                                        <!-- _____________________________________________________________________________ -->
                                        <div class="col-md-3">
                                            <div class="form-group">
                                                <label for="">Valor catastral</label>
                                                <input required type="text" class="form-control" name="ValorCatastral"  placeholder="Valor catastral">
                                            </div>
                                        </div>
                                        <!-- _____________________________________________________________________________ -->
                                        <div class="col-md-3">
                                            <div class="form-group">
                                                <label for="">Valor avalúo</label>
                                                <input required type="text" class="form-control" name="ValorAvaluo"  placeholder="Valor avalúo">
                                            </div>
                                        </div>
                                        <!-- _____________________________________________________________________________ -->
                                    </div>
                                    <!-- _____________________________________________________________________________ -->
                                    <div class="row">
                                        <div class="col-md-3">
                                            <div class="form-group">
                                                <label for="">Valor Venta</label>
                                                <input required type="text" class="form-control" name="ValorVenta"  placeholder="Valor venta">
                                            </div>
                                        </div>
                                        <!-- _____________________________________________________________________________ -->
                                        <div class="col-md-3">
                                            <label class="control-label">Tipo del inmueble</label>
                                            <select required name="TipoInmueble" class="form-control">
                                                <option value="" default selected>Default select</option>
                                                @forelse($tipo_inmueble as $tipoIn)
                                                <option value="{{$tipoIn->id}}">{{$tipoIn->idTipoInmueble}}</option>
                                                @empty @endforelse
                                            </select>
                                        </div>
                                        <!-- _____________________________________________________________________________ -->
                                        <div class="col-md-3">
                                            <label class="control-label">Tipo de operación</label>
                                            <select required name="TipoOperacion" class="form-control">
                                                <option value="">Default select</option>
                                                @forelse($tipo_operacion as $tipoOpe)
                                                <option value="{{$tipoOpe->id}}">{{$tipoOpe->TipoOperacion}}</option>
                                                @empty @endforelse
                                            </select>
                                        </div>
                                        <!-- _____________________________________________________________________________ -->
                                        <div class="col-md-3">
                                            <label class="control-label">Desarrollador</label>
                                            <select required name="Desarrollador" class="form-control">
                                                <option value="">Default select</option>
                                                @forelse($desarrollador as $des)
                                                <option value="{{$des->id}}">{{$des->Desarrollador}}</option>
                                                @empty @endforelse
                                            </select>
                                        </div>
                                        <!-- _____________________________________________________________________________ -->
                                    </div>
                                    <!-- _____________________________________________________________________________ -->
                                    <div class="row">
                                        <div class="col-md-3">
                                            <label class="control-label">Banco</label>
                                            <select required name="Banco" class="form-control">
                                                <option value="">Default select</option>
                                                @forelse($bancos as $ban)
                                                <option value="{{$ban->id}}">{{$ban->Banco}}</option>
                                                @empty @endforelse
                                            </select>
                                        </div>
                                        <!-- _____________________________________________________________________________ -->
                                        <div class="col-md-3">
                                            <label class="control-label">Fondo de vivienda</label>
                                            <select required name="FondoVivienda" class="form-control">
                                                <option value="">Default select</option>
                                                @forelse($fondos as $fon)
                                                <option value="{{$fon->id}}">{{$fon->FondoVivienda}}</option>
                                                @empty @endforelse
                                            </select>
                                        </div>
                                        <!-- _____________________________________________________________________________ -->
                                        <div class="col-md-3">
                                            <label class="control-label">Está en condominio</label>
                                            <select required name="isCondominio" class="form-control">
                                                <option value="">Default select</option>
                                                <option value="1">Si</option>
                                                <option value="0">No</option>
                                            </select>
                                        </div>
                                        <!-- _____________________________________________________________________________ -->
                                        <div class="col-md-3">
                                            <label class="control-label">Cuenta con avalúo bancario</label>
                                            <select required name="TieneAvaluoBancario" class="form-control">
                                                <option value="">Default select</option>
                                                <option value="1">Si</option>
                                                <option value="0">No</option>
                                            </select>
                                        </div>
                                        <!-- _____________________________________________________________________________ -->
                                    </div>
                                    <br>
                                    <!-- _____________________________________________________________________________ -->
                                    <div class="row">
                                        <div class="col-md-3">
                                            <label class="control-label">Aplica jornada notarial y/o sucesiones</label>
                                            <select required name="AplicaJornadaNotarial" class="form-control">
                                                <option value="">Default select</option>
                                                <option value="1">Si</option>
                                                <option value="0">No</option>
                                            </select>
                                        </div>
                                        <!-- _____________________________________________________________________________ -->
                                    </div>
                                    <div class="row">
                                        <center>
                                            <button  type="submit" class="btn btn-success">Guardar</button>
                                        </center>
                                    </div>
                                </div>
                            </div>
                    </form>
                    <!-- ______________________________________________________________________________- -->
                </div>
                <!-- _____________________________________________________________________________ -->

                <div class="tab-pane" id="comprador">

                    <div class="portlet-title">
                        <div class="caption">
                            <span class="caption-subject font-green bold uppercase">
                                <h4>
                                    <center>DATOS DEL COMPRADOR</center>
                                </h4>
                            </span>
                            <br>
                        </div>
                    </div>
                    <form method="POST" action="submit" id="DatosComprador" name="DatosComprador">
                        <input style="display:none;" type="text" name="idTramiteComprador" id="idTramiteComprador" value="">
                        <div class="portlet-body">
                            <div class="mt-element-card mt-element-overlay">
                                    <div class="row">
                                        <div class="col-md-3">
                                        <label class="control-label">Tipo Comprador/Vendedor</label>
                                        <select required name="compravende" class="form-control">
                                            <option value="">Default select</option>
                                            @forelse($tramitetipocompravende as $tramtipocomven)
                                            <option value="{{$tramtipocomven->idTipo}}">{{$tramtipocomven->TipoCompraVende}}</option>
                                            @empty @endforelse
                                        </select>
                                    </div>
                            </div><br>
                                <div class="row">
                                    <div class="col-md-3">
                                        <div class="form-group">
                                            <label for="">Nombre </label>
                                            <input required type="text" class="form-control" name="Nombre"  placeholder="Nombre " required>
                                        </div>
                                    </div>
                                    <!-- _____________________________________________________________________________ -->
                                    <div class="col-md-3">
                                        <div class="form-group">
                                            <label for="">Domicilio</label>
                                            <input required type="text" class="form-control" name="Domicilio"  placeholder="Domicilio" required>
                                        </div>
                                    </div>
                                    <!-- _____________________________________________________________________________ -->
                                    <div class="col-md-3">
                                        <div class="form-group">
                                            <label for="">Correo electrónico </label>
                                            <input type="text" class="form-control" name="CorreoElectronico"  placeholder="Correo electrónico">
                                        </div>
                                    </div>
                                    <!-- _____________________________________________________________________________ -->
                                    <div class="col-md-3">
                                        <div class="form-group">
                                            <label for="">Teléfono casa </label>
                                            <input type="text" class="form-control" name="TelCasa"  placeholder="Teléfono casa">
                                        </div>
                                    </div>
                                    <!-- _____________________________________________________________________________ -->
                                </div>
                                <!-- _____________________________________________________________________________ -->
                                <div class="row">
                                    <div class="col-md-3">
                                        <div class="form-group">
                                            <label for="">Teléfono oficina </label>
                                            <input type="text" class="form-control" name="TelOficina"  placeholder="Teléfono oficina">
                                        </div>
                                    </div>
                                    <!-- _____________________________________________________________________________ -->
                                    <div class="col-md-3">
                                        <div class="form-group">
                                            <label for="">Extensión</label>
                                            <input type="text" class="form-control" name="Extension"  placeholder="Extensión">
                                        </div>
                                    </div>
                                    <!-- _____________________________________________________________________________ -->
                                    <div class="col-md-3">
                                        <div class="form-group">
                                            <label for="">Celular </label>
                                            <input required type="text" class="form-control" name="Celular"  placeholder="Celular" required>
                                        </div>
                                    </div>
                                    <!-- _____________________________________________________________________________ -->
                                    <div class="col-md-3">
                                        <label class="control-label">Estado civil</label>
                                        <select required name="EstadoCivil" class="form-control" required>
                                            <option value="">Default select</option>
                                            @forelse($estado_civil as $estCiv)
                                            <option value="{{$estCiv->id}}">{{$estCiv->EstadoCivil}}</option>
                                            @empty @endforelse
                                        </select>
                                    </div>
                                    <!-- _____________________________________________________________________________ -->
                                </div>
                                <!-- _____________________________________________________________________________ -->
                                <div class="row">
                                    <div class="col-md-3">
                                        <label class="control-label">Régimen patrimonial del matrimonio</label>
                                        <select required name="RegimenPatrimonial" class="form-control" required>
                                            <option value="">Default select</option>
                                            @forelse($regimen_patrimonial as $regPat)
                                            <option value="{{$regPat->id}}">{{$regPat->RegimenPatromonial}}</option>
                                            @empty @endforelse
                                        </select>
                                    </div>
                                    <!-- _____________________________________________________________________________ -->
                                    <div class="col-md-3">
                                        <label class="control-label">Nacionalidad</label>
                                        <select required name="Nacionalidad" class="form-control" required>
                                            <option value="">Default select</option>
                                            @forelse($nacionalidad as $na)
                                            <option value="{{$na->id}}">{{$na->Nacionalidad}}</option>
                                            @empty @endforelse
                                        </select>
                                    </div>
                                    <!-- _____________________________________________________________________________ -->
                                    <div class="col-md-3">
                                        <div class="form-group">
                                            <label for="">Lugar de nacimiento </label>
                                            <input type="text" class="form-control" name="LugarNacimiento" placeholder="Lugar de nacimiento">
                                        </div>
                                    </div>
                                    <!-- _____________________________________________________________________________ -->
                                    <div class="col-md-3">
                                        <label class="control-label">Fecha de nacimiento</label>
                                        <br>
                                        <input class="form-control" type="date" name="FechaNacimiento" value="">
                                    </div>
                                    <!-- _____________________________________________________________________________ -->
                                </div>
                                <!-- _____________________________________________________________________________ -->
                                <div class="row">
                                    <div class="col-md-3">
                                        <div class="form-group">
                                            <label for="">CURP </label>
                                            <input  pattern=".{18,}" title="Se requieren 18 caracteres"  maxlength="18" required type="text" class="form-control" name="CURP" placeholder="CURP">
                                        </div>
                                    </div>
                                    <!-- _____________________________________________________________________________ -->
                                    <div class="col-md-3">
                                        <div class="form-group">
                                            <label for="">RFC </label>
                                            <input  pattern=".{10,}" title="Se requieren 10 caracteres"  maxlength="10" type="text" class="form-control" name="RFC"  placeholder="RFC">
                                        </div>
                                    </div>
                                    <!-- _____________________________________________________________________________ -->
                                    <div class="col-md-3">
                                        <label class="control-label">Adquirente</label>
                                        <select required name="Adquirente" class="form-control">
                                            <option value="">Default select</option>
                                            @forelse($adquirente as $ad)
                                            <option value="{{$ad->id}}">{{$ad->Adquirente}}</option>
                                            @empty @endforelse
                                        </select>
                                    </div>
                                    <!-- ____________________________________________________________________________ -->
                                    <div class="col-md-3">
                                        <label class="control-label">Tipo de identificación</label>
                                        <select required name="TipoIdentificacion" class="form-control">
                                            <option value="">Default select</option>
                                            @forelse($tipo_iodentificacion as $tipoiod)
                                            <option value="{{$tipoiod->id}}">{{$tipoiod->TipoIdentificacion}}</option>
                                            @empty @endforelse
                                        </select>
                                    </div>
                                    <!-- _____________________________________________________________________________ -->
                                </div>
                                <!-- _____________________________________________________________________________ -->
                                <div class="row">
                                    <div class="col-md-3">
                                        <div class="form-group">
                                            <label for="">Número de identificación </label>
                                            <input type="text" class="form-control" name="NumIdentificacion"  placeholder="Número de identificación">
                                        </div>
                                    </div>
                                    <!-- _____________________________________________________________________________ -->
                                    <div class="col-md-3">
                                        <label class="control-label">Aplica poder de representante legal</label>
                                        <select required class="form-control" name="AplicaPoderRepresentanteLegal">
                                            <option value="">Default select</option>
                                            <option value="1">Si</option>
                                            <option value="0">No</option>
                                        </select>
                                    </div>
                                    <!-- ____________________________________________________________________________ -->
                                    <div class="col-md-3">
                                        <label class="control-label">Aplica obligado solidario</label>
                                        <select required class="form-control" name="AplicaObligadoSolidario">
                                            <option value="">Default select</option>
                                            <option value="1">Si</option>
                                            <option value="0">No</option>
                                        </select>
                                    </div>
                                    <!-- _____________________________________________________________________________ -->
                                </div>
                                <!-- _____________________________________________________________________________ -->
                                <div class="row">
                                    <div class="col-md-10"></div>
                                    <div class="col-md-2">
                                        <button type="submit" id="nuevoComprador" class="btn btn-primary">Agregar otro comprador</button>
                                    </div>
                                </div>
                                <div class="row">
                                    <center>
                                        <button  type="submit" class="btn btn-success">Guardar</button>
                                    </center>
                                </div>
                                <!-- _____________________________________________________________________________ -->
                            </div>
                        </div>
                    </form>
                </div>
                <!-- _____________________________________________________________________________ -->

                <div class="tab-pane" id="vendedor">

                    <div class="portlet-title">
                        <div class="caption">
                            <span class="caption-subject font-green bold uppercase">
                                <h4>
                                    <center>DATOS DEL VENDEDOR</center>
                                </h4>
                            </span>
                            <br>
                        </div>
                    </div>
                    <form method="POST" id="DatosVendedor">
                        <div class="portlet-body">
                            <div class="mt-element-card mt-element-overlay">
                            <div class="row">
                                        <div class="col-md-3">
                                        <label class="control-label">Tipo Comprador/Vendedor</label>
                                        <select required name="compravende" class="form-control">
                                            <option value="">Default select</option>
                                            @forelse($tramitetipocompravende as $tramtipocomven)
                                            <option value="{{$tramtipocomven->idTipo}}">{{$tramtipocomven->TipoCompraVende}}</option>
                                            @empty @endforelse
                                        </select>
                                    </div>
                            </div><br>
                                <div class="row">
                                    <div class="col-md-3">
                                        <div class="form-group">
                                            <label for="">Nombre </label>
                                            <input required type="text" class="form-control" name="Nombre" placeholder="Nombre ">
                                        </div>
                                    </div>
                                    <!-- _____________________________________________________________________________ -->
                                    <div class="col-md-3">
                                        <div class="form-group">
                                            <label for="">Domicilio</label>
                                            <input required type="text" class="form-control" name="Domicilio"  placeholder="Domicilio">
                                        </div>
                                    </div>
                                    <!-- _____________________________________________________________________________ -->
                                    <div class="col-md-3">
                                        <div class="form-group">
                                            <label for="">Correo electrónico </label>
                                            <input type="text" name="CorreoElectronico" class="form-control"  placeholder="Correo electrónico">
                                        </div>
                                    </div>
                                    <!-- _____________________________________________________________________________ -->
                                    <div class="col-md-3">
                                        <div class="form-group">
                                            <label for="">Teléfono casa </label>
                                            <input type="text" name="TelCasa" class="form-control"  placeholder="Teléfono casa">
                                        </div>
                                    </div>
                                    <!-- _____________________________________________________________________________ -->
                                </div>
                                <!-- _____________________________________________________________________________ -->
                                <div class="row">
                                    <div class="col-md-3">
                                        <div class="form-group">
                                            <label for="">Teléfono oficina </label>
                                            <input type="text" name="TelOficina" class="form-control"  placeholder="Teléfono oficina  ">
                                        </div>
                                    </div>
                                    <!-- _____________________________________________________________________________ -->
                                    <div class="col-md-3">
                                        <div class="form-group">
                                            <label for="">Extensión</label>
                                            <input type="text" name="Extension" class="form-control"  placeholder="Extensión">
                                        </div>
                                    </div>
                                    <!-- _____________________________________________________________________________ -->
                                    <div class="col-md-3">
                                        <div class="form-group">
                                            <label for="">Celular </label>
                                            <input required type="text" name="Celular" class="form-control"  placeholder="Celular">
                                        </div>
                                    </div>
                                    <!-- _____________________________________________________________________________ -->
                                    <div class="col-md-3">
                                        <label class="control-label">Estado civil</label>
                                        <select required name="EstadoCivil" class="form-control">
                                            <option value="">Default select</option>
                                            @forelse($estado_civil as $estCiv)
                                            <option value="{{$estCiv->id}}">{{$estCiv->EstadoCivil}}</option>
                                            @empty @endforelse
                                        </select>
                                    </div>
                                    <!-- _____________________________________________________________________________ -->
                                </div>
                                <!-- _____________________________________________________________________________ -->
                                <div class="row">
                                    <div class="col-md-3">
                                        <label  class="control-label">Régimen patrimonial del matrimonio</label>
                                        <select required name="RegimenPatrimonial" class="form-control">
                                            <option value="">Default select</option>
                                            @forelse($regimen_patrimonial as $regPat)
                                            <option value="{{$regPat->id}}">{{$regPat->RegimenPatromonial}}</option>
                                            @empty @endforelse
                                        </select>
                                    </div>
                                    <!-- _____________________________________________________________________________ -->
                                    <div class="col-md-3">
                                        <label class="control-label">Nacionalidad</label>
                                        <select required name="Nacionalidad" class="form-control">
                                            <option value="">Default select</option>
                                            @forelse($nacionalidad as $na)
                                            <option value="{{$na->id}}">{{$na->Nacionalidad}}</option>
                                            @empty @endforelse
                                        </select>
                                    </div>
                                    <!-- _____________________________________________________________________________ -->
                                    <div class="col-md-3">
                                        <div class="form-group">
                                            <label for="">Lugar de nacimiento </label>
                                            <input type="text" name="LugarNacimiento" class="form-control"  placeholder="Lugar de nacimiento">
                                        </div>
                                    </div>
                                    <!-- _____________________________________________________________________________ -->
                                    <div class="col-md-3">
                                        <label class="control-label">Fecha de nacimiento</label>
                                        <br>
                                        <input type="date" name="FechaNacimiento" value=""  class="form-control">
                                    </div>
                                    <!-- _____________________________________________________________________________ -->
                                </div>
                                <!-- _____________________________________________________________________________ -->
                                <div class="row">
                                    <div class="col-md-3">
                                        <div class="form-group">
                                            <label for="">CURP </label>
                                            <input  pattern=".{18,}" title="Se requieren 18 caracteres" maxlength="18" required type="text" name="CURP" class="form-control"  placeholder="CURP">
                                        </div>
                                    </div>
                                    <!-- _____________________________________________________________________________ -->
                                    <div class="col-md-3">
                                        <div class="form-group">
                                            <label for="">RFC </label>
                                            <input  pattern=".{10,}" title="Se requieren 10 caracteres" maxlength="10" type="text" name="RFC" class="form-control"  placeholder="RFC">
                                        </div>
                                    </div>
                                    <!-- _____________________________________________________________________________ -->
                                    <div class="col-md-3">
                                        <label class="control-label">Enajenante</label>
                                        <select required name="Enajenante"class="form-control">
                                            <option value="">Default select</option>
                                            @forelse($adquirente as $ad)
                                            <option value="{{$ad->id}}">{{$ad->Adquirente}}</option>
                                            @empty @endforelse
                                        </select>
                                    </div>
                                    <!-- ____________________________________________________________________________ -->
                                    <div class="col-md-3">
                                        <label class="control-label">Tipo de identificación</label>
                                        <select required name="TipoIdentificacion" class="form-control">
                                            <option value="">Default select</option>
                                            @forelse($tipo_iodentificacion as $tipoiod)
                                            <option value="{{$tipoiod->id}}">{{$tipoiod->TipoIdentificacion}}</option>
                                            @empty @endforelse
                                        </select>
                                    </div>
                                    <!-- _____________________________________________________________________________ -->
                                </div>
                                <!-- _____________________________________________________________________________ -->
                                <div class="row">
                                    <div class="col-md-3">
                                        <div class="form-group">
                                            <label for="">Número de identificación </label>
                                            <input required type="text" class="form-control" name="NumIdentificacion"  placeholder="Número de identificación">
                                        </div>
                                    </div>
                                    <!-- _____________________________________________________________________________ -->
                                    <div class="col-md-3">
                                        <label class="control-label">Aplica poder de representante legal</label>
                                        <select required name="AplicaPoderRepresentanteLegal" class="form-control">
                                            <option value="">Default select</option>
                                            <option value="1">Si</option>
                                            <option value="0">No</option>
                                        </select>
                                    </div>
                                    <!-- ____________________________________________________________________________ -->
                                    <div class="col-md-3">
                                        <label class="control-label">Aplica cálculo</label>
                                     <select required name="AplicaCalculo" class="form-control">
                                            <option value="">Default select</option>
                                            <option value="1">Si</option>
                                            <option value="0">No</option>
                                        </select>
                                    </div>
                                    <!-- _____________________________________________________________________________ -->
                                </div>
                                <!-- _____________________________________________________________________________ -->
                                <div class="row">
                                    <div class="col-md-10"></div>
                                    <div class="col-md-2">
                                        <button type="submit" id="nuevoVendedor" class="btn btn-primary">Agregar otro vendedor</button>
                                    </div>
                                </div>
                                <div class="row">
                                    <center>
                                        <button  type="submit" class="btn btn-success">Guardar</button>
                                    </center>
                                </div>
                                <!-- _____________________________________________________________________________ -->
                            </div>
                        </div>
                    </form>
                </div>
                <!-- _____________________________________________________________________________ -->

                <div class="tab-pane" id="contactoBanco">

                    <div class="portlet-title">
                        <div class="caption">
                            <span class="caption-subject font-green bold uppercase">
                                <h4>
                                    <center>DATOS DEL CONTACTO DEL BANCO</center>
                                </h4>
                            </span>
                            <br>
                        </div>
                    </div>
                    <form method="POST" action="submit" id="DatosContactoBanco">
                      <input style="display:none" type="text" name="idTramite" id="idTramite" value="">
                        <div class="portlet-body">
                            <div class="mt-element-card mt-element-overlay">
                                <div class="row">
                                    <div class="col-md-3">
                                        <div class="form-group">
                                            <label for="">Nombre del ejecutivo </label>
                                            <input required type="text" class="form-control" name="NombreEjecutivo"  placeholder="Nombre del ejecutivo ">
                                        </div>
                                    </div>
                                    <!-- _____________________________________________________________________________ -->
                                    <div class="col-md-3">
                                        <div class="form-group">
                                            <label for="">Correo electrónico</label>
                                            <input type="text" class="form-control" name="CorreoElectronico"  placeholder="Correo electrónico">
                                        </div>
                                    </div>
                                    <!-- _____________________________________________________________________________ -->
                                    <div class="col-md-3">
                                        <div class="form-group">
                                            <label for="">Teléfono </label>
                                            <input type="text" class="form-control" name="Telefono"  placeholder="Teléfono">
                                        </div>
                                    </div>
                                    <!-- _____________________________________________________________________________ -->
                                    <div class="col-md-3">
                                        <div class="form-group">
                                            <label for="">Extensión </label>
                                           <input type="text" class="form-control" name="Extension"  placeholder="Extensión">
                                        </div>
                                    </div>
                                    <!-- _____________________________________________________________________________ -->
                                </div>
                                <!-- _____________________________________________________________________________ -->
                                <div class="row">
                                    <div class="col-md-3">
                                        <div class="form-group">
                                            <label for="">Celular </label>
                                            <input required type="text" class="form-control" name="Celular"  placeholder="Celular">
                                        </div>
                                    </div>
                                    <!-- _____________________________________________________________________________ -->
                                </div>
                                <!-- _____________________________________________________________________________ -->
                                <div class="row">
                                    <div class="col-md-10"></div>
                                    <div class="col-md-2">
                                        <button type="submit" id="nuevoContactoBan" class="btn btn-primary">Agregar otro </button>
                                    </div>
                                </div>
                                <div class="row">
                                    <center>
                                        <button  type="submit" class="btn btn-success">Guardar</button>
                                    </center>
                                </div>
                                <!-- _____________________________________________________________________________ -->
                            </div>
                        </div>
                    </form>
                </div>
                <!-- _____________________________________________________________________________ -->


                <div class="tab-pane" id="contactoDesarrollador">

                    <div class="portlet-title">
                        <div class="caption">
                            <span class="caption-subject font-green bold uppercase">
                                <h4>
                                    <center>DATOS DEL CONTACTO DEL DESARROLLADOR</center>
                                </h4>
                            </span>
                            <br>
                        </div>
                    </div>
                    <form method="POST" action="submit" id="DatosContactoDesarrollador">
                        <div class="portlet-body">
                            <div class="mt-element-card mt-element-overlay">
                                <div class="row">
                                    <div class="col-md-3">
                                        <div class="form-group">
                                            <label for="">Nombre del ejecutivo </label>
                                            <input required type="text" class="form-control" name="NombreEjecutivo"  placeholder="Nombre del ejecutivo ">
                                        </div>
                                    </div>
                                    <!-- _____________________________________________________________________________ -->
                                    <div class="col-md-3">
                                        <div class="form-group">
                                            <label for="">Correo electrónico</label>
                                            <input type="email" class="form-control" name="CorreoElectronico"  placeholder="Correo electrónico">
                                        </div>
                                    </div>
                                    <!-- _____________________________________________________________________________ -->
                                    <div class="col-md-3">
                                        <div class="form-group">
                                            <label for="">Teléfono </label>
                                            <input type="text" class="form-control" name="Telefono"  placeholder="Teléfono">
                                        </div>
                                    </div>
                                    <!-- _____________________________________________________________________________ -->
                                    <div class="col-md-3">
                                        <div class="form-group">
                                            <label for="">Extensión </label>
                                            <input type="text" class="form-control" name="Extension"  placeholder="Extensión">
                                        </div>
                                    </div>
                                    <!-- _____________________________________________________________________________ -->
                                </div>
                                <!-- _____________________________________________________________________________ -->
                                <div class="row">
                                    <div class="col-md-3">
                                        <div class="form-group">
                                            <label for="">Celular </label>
                                            <input required type="text" class="form-control" name="Celular"  placeholder="Celular">
                                        </div>
                                    </div>
                                    <!-- _____________________________________________________________________________ -->
                                </div>
                                <!-- _____________________________________________________________________________ -->
                                <div class="row">
                                    <div class="col-md-10"></div>
                                    <div class="col-md-2">
                                        <button id="nuevoContactoDes" type="submit" class="btn btn-primary">Agregar otro </button>
                                    </div>
                                </div>
                                <div class="row">
                                    <center>
                                        <button  type="submit" class="btn btn-success">Guardar</button>
                                        <a  href="/expedientes" class="btn btn-danger">Terminar</a>
                                    </center>
                                </div>
                                <!-- _____________________________________________________________________________ -->
                            </div>
                        </div>
                    </form>
                </div>
                {{-- ------------------------------------------------------------------------------------------- --}}

            </div>
        </div>
    </div>
</div>

@endsection @section('content_js')
<script src="{{asset('js/expediente_add.js')}}"></script>
<script src="{{ asset('js/fichasCaptura.js') }}"></script>
<script src="{{ asset('js/jquery.bootstrap-growl.min.js') }}"></script>
@stop
