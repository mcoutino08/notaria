@unless($notarias->count()>0)
	<option value="">Sin Notarias</option>
@else
	@if($notarias->count()>=1)
	<option value="" selected>Notarias</option>
	@endif
 @foreach($notarias as $N)
    <option value="{{ $N->id}}">{{ $N->Numero}} {{ $N->Nombre}}</option>
@endforeach
@endunless
