$(document).ready(function () {

    $('#tablaFichasCaptura').on('click', '#fichaEdit', function (e) {
        e.preventDefault();
        let currow = $(this).closest('tr');
        var idTramite = currow.find('td:eq(0)').text();
        replace(idTramite);
    });


    $('#DatosGeneralesInmuebleEdit').on('submit', function (e) {
        e.preventDefault();
        $.ajax({
            url: '/expediente_saveEdit',
            type: 'POST',
            data: new FormData(this),
            contentType: false,
            cache: false,
            processData: false,
            timeout: 60000,
            success: function (data) {
                Glow(`${data.success}`, 'success');
            }
        }).fail(function (jqXHR, textStatus, errorThrown) {
            if (jqXHR.status === 0) {
                Glow('Not connect: Verify Network.', 'danger');
            } else if (jqXHR.status == 404) {
                Glow('Requested page not found [404]', 'danger');
            } else if (jqXHR.status == 500) {
                Glow('Internal Server Error [500].', 'danger');
            } else if (textStatus === 'parsererror', 'danger') {
                Glow('Requested JSON parse failed.', 'danger');
            } else if (textStatus === 'timeout', 'danger') {
                Glow('Time out error.', 'danger');
            } else if (textStatus === 'abort', 'danger') {
                Glow('Ajax request aborted.', 'danger');
            } else {
                Glow(`Ocurrio un error.${jqXHR.responseJSON.message}`, 'danger');
            }
        });
    });
});

function replace(valuetofind) {
    $.ajax({
        url: '/expediente_edit',
        type: "post",
        dateType: 'json',
        data: ({ id: valuetofind }),
        success: function (data) {
            $('#replacespace').html(data);
        }
    }).fail(function (jqXHR, textStatus, errorThrown) {
        if (jqXHR.status === 0) {
            Glow('Not connect: Verify Network.', 'danger');
        } else if (jqXHR.status == 404) {
            Glow('Requested page not found [404]', 'danger');
        } else if (jqXHR.status == 500) {
            Glow('Internal Server Error [500].', 'danger');
        } else if (textStatus === 'parsererror', 'danger') {
            Glow('Requested JSON parse failed.', 'danger');
        } else if (textStatus === 'timeout', 'danger') {
            Glow('Time out error.', 'danger');
        } else if (textStatus === 'abort', 'danger') {
            Glow('Ajax request aborted.', 'danger');
        } else {
            Glow(`Ocurrio un error.${jqXHR.responseJSON.message}`, 'danger');
        }
    });
}

function Glow(Message, TipeMessage) {
    setTimeout(function () {
        $.bootstrapGrowl(`${Message}`, {
            ele: 'body', // which element to append to
            type: `${TipeMessage}`, // (null, 'info', 'danger', 'success')
            offset: { from: 'top', amount: 130 }, // 'top', or 'bottom'
            align: 'right', // ('left', 'right', or 'center')
            width: 250, // (integer, or 'auto')
            delay: 4000, // Time while the message will be displayed. It's not equivalent to the *demo* timeOut!
            allow_dismiss: false, // If true then will display a cross to close the popup.
            stackup_spacing: 10 // spacing between consecutively stacked growls.
        }, 100);
    });
};