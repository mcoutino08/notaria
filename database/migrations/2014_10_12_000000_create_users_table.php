<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateUsersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('usuarios', function (Blueprint $table) {
            $table->increments('id',11);
            $table->string('Nombre',125);
            $table->string('Celular',45)->unique();
            $table->string('Passwd',60);
            $table->string('Correo',85);
            $table->increments('idNotaria',11);
            $table->increments('parentid',11);
            $table->increments('idModulo',11);
            $table->string('Paterno',45);
            $table->increments('IdModulo',11);
            $table->rememberToken();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('users');
    }
}
