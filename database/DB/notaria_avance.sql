-- phpMyAdmin SQL Dump
-- version 4.5.1
-- http://www.phpmyadmin.net
--
-- Servidor: 127.0.0.1
-- Tiempo de generación: 11-05-2018 a las 03:43:31
-- Versión del servidor: 10.1.8-MariaDB
-- Versión de PHP: 5.6.35

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Base de datos: `notaria`
--

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `adquirente`
--

CREATE TABLE `adquirente` (
  `id` int(11) NOT NULL,
  `Adquirente` varchar(120) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Volcado de datos para la tabla `adquirente`
--

INSERT INTO `adquirente` (`id`, `Adquirente`) VALUES
(1, 'Persona Fisica Nacional'),
(2, 'Persona Fisica Extranjera'),
(3, 'Persona Moral Nacional'),
(4, 'Persona Moral Extranjera');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `bancos`
--

CREATE TABLE `bancos` (
  `id` int(11) NOT NULL,
  `Banco` varchar(45) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Volcado de datos para la tabla `bancos`
--

INSERT INTO `bancos` (`id`, `Banco`) VALUES
(1, 'No Aplica'),
(2, 'HSBC'),
(3, 'Santander'),
(4, 'Bancomer'),
(5, 'Banamex'),
(6, 'Bansi'),
(7, 'Banorte'),
(8, 'Bx+'),
(9, 'Sabadell');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `calendarios`
--

CREATE TABLE `calendarios` (
  `id` int(11) NOT NULL,
  `dia` varchar(150) DEFAULT NULL,
  `habil` tinyint(1) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  `creado_por` int(11) DEFAULT NULL,
  `modificado_por` int(11) DEFAULT NULL,
  `borrado_por` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `desarrollador`
--

CREATE TABLE `desarrollador` (
  `id` int(11) NOT NULL,
  `Desarrollador` varchar(120) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Volcado de datos para la tabla `desarrollador`
--

INSERT INTO `desarrollador` (`id`, `Desarrollador`) VALUES
(1, 'No Aplica'),
(2, 'Abilia '),
(3, 'B-Grand'),
(4, 'Myania'),
(5, 'IHM');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `ejecutivo_desarrollador`
--

CREATE TABLE `ejecutivo_desarrollador` (
  `id` int(11) NOT NULL,
  `idTipoEjecutivoDesarrollador` int(11) DEFAULT NULL,
  `NombreEjecutivo` varchar(65) DEFAULT NULL,
  `Correo` varchar(85) DEFAULT NULL,
  `Telefono` varchar(20) DEFAULT NULL,
  `Ext` varchar(20) DEFAULT NULL,
  `Celular` varchar(30) DEFAULT NULL,
  `FechaCreate` datetime DEFAULT NULL,
  `UserCreate` int(11) DEFAULT NULL,
  `LastUpdate` datetime DEFAULT NULL,
  `UserUpdate` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `ejecutivo_desarrollador_tipo`
--

CREATE TABLE `ejecutivo_desarrollador_tipo` (
  `id` int(11) NOT NULL,
  `TipoEjecutivoDesarrollador` varchar(45) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `entidad`
--

CREATE TABLE `entidad` (
  `id` int(11) NOT NULL,
  `EntidadFederativa` varchar(45) DEFAULT NULL,
  `parentid` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Volcado de datos para la tabla `entidad`
--

INSERT INTO `entidad` (`id`, `EntidadFederativa`, `parentid`) VALUES
(1, 'Ciudad de México', NULL),
(2, 'Estado de México', NULL),
(3, 'Otros', NULL);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `estado_civil`
--

CREATE TABLE `estado_civil` (
  `id` int(11) NOT NULL,
  `EstadoCivil` varchar(65) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Volcado de datos para la tabla `estado_civil`
--

INSERT INTO `estado_civil` (`id`, `EstadoCivil`) VALUES
(1, 'No Aplica'),
(2, 'Casado'),
(3, 'Soltero');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `fondos`
--

CREATE TABLE `fondos` (
  `id` int(11) NOT NULL,
  `FondoVivienda` varchar(65) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Volcado de datos para la tabla `fondos`
--

INSERT INTO `fondos` (`id`, `FondoVivienda`) VALUES
(1, 'No Aplica'),
(2, 'IMSS'),
(3, 'INFONAVIT'),
(4, 'FOVISSSTE');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `modulos`
--

CREATE TABLE `modulos` (
  `id` int(11) NOT NULL,
  `Modulo` varchar(45) DEFAULT NULL,
  `idNotaria` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Volcado de datos para la tabla `modulos`
--

INSERT INTO `modulos` (`id`, `Modulo`, `idNotaria`) VALUES
(1, '15-A', 1),
(2, '15-B', 1),
(3, '15-C', 1),
(4, '182-A', 2),
(5, '182-B', 2),
(6, '182-C', 2),
(7, '95-A', 3),
(8, '95-B', 3),
(9, '95-C', 3);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `nacionalidad`
--

CREATE TABLE `nacionalidad` (
  `id` int(11) NOT NULL,
  `Nacionalidad` varchar(65) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Volcado de datos para la tabla `nacionalidad`
--

INSERT INTO `nacionalidad` (`id`, `Nacionalidad`) VALUES
(1, 'No Aplica'),
(2, 'Mexicana'),
(3, 'Extranjera');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `notaria`
--

CREATE TABLE `notaria` (
  `id` int(11) NOT NULL,
  `Numero` varchar(10) DEFAULT NULL,
  `Nombre` varchar(120) DEFAULT NULL,
  `TelOficina` varchar(65) DEFAULT NULL,
  `Extension` varchar(25) DEFAULT NULL,
  `Responsable` varchar(45) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Volcado de datos para la tabla `notaria`
--

INSERT INTO `notaria` (`id`, `Numero`, `Nombre`, `TelOficina`, `Extension`, `Responsable`) VALUES
(1, '15', NULL, NULL, NULL, NULL),
(2, '182', NULL, NULL, NULL, NULL),
(3, '95', NULL, NULL, NULL, NULL);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `regimen_patrimonial`
--

CREATE TABLE `regimen_patrimonial` (
  `id` int(11) NOT NULL,
  `RegimenPatromonial` varchar(45) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Volcado de datos para la tabla `regimen_patrimonial`
--

INSERT INTO `regimen_patrimonial` (`id`, `RegimenPatromonial`) VALUES
(1, 'No Aplica'),
(2, 'Sociedad Conyugal'),
(3, 'Separación de Bienes'),
(4, 'Casado en el Extranjero');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `rubros`
--

CREATE TABLE `rubros` (
  `id` int(11) NOT NULL,
  `rubros_categorias_id` int(11) NOT NULL,
  `rubros_id` int(11) DEFAULT NULL,
  `nombre` varchar(150) DEFAULT NULL,
  `orden` int(3) DEFAULT NULL,
  `fecha_inicio_tabla` varchar(45) DEFAULT NULL,
  `fecha_inicio_campo` varchar(45) DEFAULT NULL,
  `fecha_inicio_numero` int(3) DEFAULT NULL,
  `fecha_inicio_habil` tinyint(1) DEFAULT NULL,
  `fecha_fin_tabla` varchar(45) DEFAULT NULL,
  `fecha_fin_campo` varchar(45) DEFAULT NULL,
  `fecha_fin_numero` int(3) DEFAULT NULL,
  `fecha_fin_habil` tinyint(1) DEFAULT NULL,
  `depende_tabla` varchar(45) DEFAULT NULL,
  `depende_campo` varchar(45) DEFAULT NULL COMMENT 'Si se activa dependiendo de un campo anterior',
  `depende_max_valor` varchar(45) DEFAULT NULL COMMENT 'Si se activa el maximo valor que pueda tomar\n',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  `creado_por` int(11) DEFAULT NULL,
  `modificado_por` int(11) DEFAULT NULL,
  `borrado_por` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Volcado de datos para la tabla `rubros`
--

INSERT INTO `rubros` (`id`, `rubros_categorias_id`, `rubros_id`, `nombre`, `orden`, `fecha_inicio_tabla`, `fecha_inicio_campo`, `fecha_inicio_numero`, `fecha_inicio_habil`, `fecha_fin_tabla`, `fecha_fin_campo`, `fecha_fin_numero`, `fecha_fin_habil`, `depende_tabla`, `depende_campo`, `depende_max_valor`, `created_at`, `updated_at`, `deleted_at`, `creado_por`, `modificado_por`, `borrado_por`) VALUES
(1, 1, NULL, 'Escritura que acredite su propiedad', 1, '', '', 0, 0, '', '', 0, 0, NULL, '', '', '0000-00-00 00:00:00', '0000-00-00 00:00:00', NULL, 1, 0, 0),
(2, 1, NULL, 'Precio del inmueble para cotizaci?n', 2, '', '', 0, 0, '', '', 0, 0, NULL, '', '', '0000-00-00 00:00:00', '0000-00-00 00:00:00', NULL, 1, 0, 0),
(3, 1, NULL, 'Carta de instrucci?n de cancelaci?n de hipoteca o de reserva de dominio vigente', 3, '', '', 0, 0, '', '', 0, 0, NULL, '', '', '0000-00-00 00:00:00', '0000-00-00 00:00:00', NULL, 1, 0, 0),
(4, 1, NULL, '?ltima boleta predial', 4, '', '', 0, 0, '', '', 0, 0, NULL, '', '', '0000-00-00 00:00:00', '0000-00-00 00:00:00', NULL, 1, 0, 0),
(5, 1, NULL, '?ltima boleta de agua', 5, '', '', 0, 0, '', '', 0, 0, NULL, '', '', '0000-00-00 00:00:00', '0000-00-00 00:00:00', NULL, 1, 0, 0),
(6, 1, NULL, 'Manifestaci?n de construcci?n (en caso de compra de terreno)', 6, '', '', 0, 0, '', '', 0, 0, NULL, '', '', '0000-00-00 00:00:00', '0000-00-00 00:00:00', NULL, 1, 0, 0),
(7, 1, NULL, 'Aviso de terminaci?n de obra (en caso de compra de terreno)', 7, '', '', 0, 0, '', '', 0, 0, NULL, '', '', '0000-00-00 00:00:00', '0000-00-00 00:00:00', NULL, 1, 0, 0),
(8, 1, NULL, 'Alineamiento y n?mero oficial (en caso de compra de terreno)', 8, '', '', 0, 0, '', '', 0, 0, NULL, '', '', '0000-00-00 00:00:00', '0000-00-00 00:00:00', NULL, 1, 0, 0),
(9, 1, NULL, 'Regulaci?n de construcci?n (por casa o depto. de terreno, edificios no nuevos, adecuaciones)', 9, '', '', 0, 0, '', '', 0, 0, NULL, '', '', '0000-00-00 00:00:00', '0000-00-00 00:00:00', NULL, 1, 0, 0),
(10, 1, NULL, 'Escritura de constituci?n del condominio', 10, '', '', 0, 0, '', '', 0, 0, NULL, '', '', '0000-00-00 00:00:00', '0000-00-00 00:00:00', NULL, 1, 0, 0),
(11, 1, NULL, 'Constancia de no adeudo por pago de mantenimiento', 11, '', '', 0, 0, '', '', 0, 0, NULL, '', '', '0000-00-00 00:00:00', '0000-00-00 00:00:00', NULL, 1, 0, 0),
(12, 1, NULL, 'Reglamento del condominio', 12, '', '', 0, 0, '', '', 0, 0, NULL, '', '', '0000-00-00 00:00:00', '0000-00-00 00:00:00', NULL, 1, 0, 0),
(13, 1, NULL, 'Acta de matrimonio', 13, '', '', 0, 0, '', '', 0, 0, NULL, '', '', '0000-00-00 00:00:00', '0000-00-00 00:00:00', NULL, 1, 0, 0),
(14, 1, NULL, 'Acta de nacimiento', 14, '', '', 0, 0, '', '', 0, 0, NULL, '', '', '0000-00-00 00:00:00', '0000-00-00 00:00:00', NULL, 1, 0, 0),
(15, 1, NULL, 'Forma Migratoria', 15, '', '', 0, 0, '', '', 0, 0, NULL, '', '', '0000-00-00 00:00:00', '0000-00-00 00:00:00', NULL, 1, 0, 0),
(16, 1, NULL, 'Medio de Pago - Contado', 16, '', '', 0, 0, '', '', 0, 0, NULL, '', '', '0000-00-00 00:00:00', '0000-00-00 00:00:00', NULL, 1, 0, 0),
(17, 1, NULL, 'Medio de Pago - Transferencia electr?nica', 17, '', '', 0, 0, '', '', 0, 0, NULL, '', '', '0000-00-00 00:00:00', '0000-00-00 00:00:00', NULL, 1, 0, 0),
(18, 1, NULL, 'Medio de Pago - Cheque de caja', 18, '', '', 0, 0, '', '', 0, 0, NULL, '', '', '0000-00-00 00:00:00', '0000-00-00 00:00:00', NULL, 1, 0, 0),
(19, 1, NULL, 'Medio de Pago - Cheque certificado', 19, '', '', 0, 0, '', '', 0, 0, NULL, '', '', '0000-00-00 00:00:00', '0000-00-00 00:00:00', NULL, 1, 0, 0),
(20, 1, NULL, 'Comprobante de domicilio (luz, tel?fono, estado de cuenta)', 20, '', '', 0, 0, '', '', 0, 0, NULL, '', '', '0000-00-00 00:00:00', '0000-00-00 00:00:00', NULL, 1, 0, 0),
(21, 1, NULL, 'Identificaci?n oficial (INE, pasaporte, c?dula profesional, licencia)', 21, '', '', 0, 0, '', '', 0, 0, NULL, '', '', '0000-00-00 00:00:00', '0000-00-00 00:00:00', NULL, 1, 0, 0),
(22, 1, NULL, 'CURP', 22, '', '', 0, 0, '', '', 0, 0, NULL, '', '', '0000-00-00 00:00:00', '0000-00-00 00:00:00', NULL, 1, 0, 0),
(23, 1, NULL, 'RFC', 23, '', '', 0, 0, '', '', 0, 0, NULL, '', '', '0000-00-00 00:00:00', '0000-00-00 00:00:00', NULL, 1, 0, 0),
(24, 1, NULL, 'UIF (Comprobante de domicilio)', 24, '', '', 0, 0, '', '', 0, 0, NULL, '', '', '0000-00-00 00:00:00', '0000-00-00 00:00:00', NULL, 1, 0, 0),
(25, 1, NULL, 'UIF (Identificaci?n)', 25, '', '', 0, 0, '', '', 0, 0, NULL, '', '', '0000-00-00 00:00:00', '0000-00-00 00:00:00', NULL, 1, 0, 0),
(26, 1, NULL, 'UIF (RFC)', 26, '', '', 0, 0, '', '', 0, 0, NULL, '', '', '0000-00-00 00:00:00', '0000-00-00 00:00:00', NULL, 1, 0, 0),
(27, 1, NULL, 'UIF (Medio de Pago)', 27, '', '', 0, 0, '', '', 0, 0, NULL, '', '', '0000-00-00 00:00:00', '0000-00-00 00:00:00', NULL, 1, 0, 0),
(28, 1, NULL, 'UIF (Cartas de due?o y beneficiario)', 28, '', '', 0, 0, '', '', 0, 0, NULL, '', '', '0000-00-00 00:00:00', '0000-00-00 00:00:00', NULL, 1, 0, 0),
(29, 1, NULL, 'UIF (Pago de tercero)', 29, '', '', 0, 0, '', '', 0, 0, NULL, '', '', '0000-00-00 00:00:00', '0000-00-00 00:00:00', NULL, 1, 0, 0),
(30, 1, NULL, 'Acta constitutiva', 30, '', '', 0, 0, '', '', 0, 0, NULL, '', '', '0000-00-00 00:00:00', '0000-00-00 00:00:00', NULL, 1, 0, 0),
(31, 1, NULL, 'Poder del representante legal', 31, '', '', 0, 0, '', '', 0, 0, NULL, '', '', '0000-00-00 00:00:00', '0000-00-00 00:00:00', NULL, 1, 0, 0),
(32, 1, NULL, 'Aval?o para efectos del cr?dito otorgado por el banco', 32, '', '', 0, 0, '', '', 0, 0, NULL, '', '', '0000-00-00 00:00:00', '0000-00-00 00:00:00', NULL, 1, 0, 0),
(33, 1, NULL, 'Car?tula de cr?dito', 33, '', '', 0, 0, '', '', 0, 0, NULL, '', '', '0000-00-00 00:00:00', '0000-00-00 00:00:00', NULL, 1, 0, 0),
(34, 1, NULL, 'Tabla de amortizaci?n', 34, '', '', 0, 0, '', '', 0, 0, NULL, '', '', '0000-00-00 00:00:00', '0000-00-00 00:00:00', NULL, 1, 0, 0),
(35, 1, NULL, 'Kit CESI Infonavit', 35, '', '', 0, 0, '', '', 0, 0, NULL, '', '', '0000-00-00 00:00:00', '0000-00-00 00:00:00', NULL, 1, 0, 0),
(36, 1, NULL, 'Kit FOVISSSTE', 36, '', '', 0, 0, '', '', 0, 0, NULL, '', '', '0000-00-00 00:00:00', '0000-00-00 00:00:00', NULL, 1, 0, 0),
(37, 1, NULL, '?ltima declaraci?n anual (solo vendedor pf con act empresarial)', 37, '', '', 0, 0, '', '', 0, 0, NULL, '', '', '0000-00-00 00:00:00', '0000-00-00 00:00:00', NULL, 1, 0, 0),
(38, 1, NULL, 'Tres declaraciones mensuales (solo local comercial)', 38, '', '', 0, 0, '', '', 0, 0, NULL, '', '', '0000-00-00 00:00:00', '0000-00-00 00:00:00', NULL, 1, 0, 0),
(39, 1, NULL, 'Complemento de factura que dio el Notario al vendedor CFDI', 39, '', '', 0, 0, '', '', 0, 0, NULL, '', '', '0000-00-00 00:00:00', '0000-00-00 00:00:00', NULL, 1, 0, 0),
(40, 1, NULL, 'Facturas de inversiones que demuestren mejoras hechas al inmueble', 40, '', '', 0, 0, '', '', 0, 0, NULL, '', '', '0000-00-00 00:00:00', '0000-00-00 00:00:00', NULL, 1, 0, 0),
(41, 1, NULL, 'Gastos notariales por el tr?mite factura (abril 2014 para atr?s)', 41, '', '', 0, 0, '', '', 0, 0, NULL, '', '', '0000-00-00 00:00:00', '0000-00-00 00:00:00', NULL, 1, 0, 0),
(42, 1, NULL, 'Comisiones y mediaciones Facturas de corredor y br?ker', 42, '', '', 0, 0, '', '', 0, 0, NULL, '', '', '0000-00-00 00:00:00', '0000-00-00 00:00:00', NULL, 1, 0, 0),
(43, 1, NULL, 'Otros deducibles', 43, '', '', 0, 0, '', '', 0, 0, NULL, '', '', '0000-00-00 00:00:00', '0000-00-00 00:00:00', NULL, 1, 0, 0),
(44, 1, NULL, 'COMPRADOR', 44, '', '', 0, 0, '', '', 0, 0, NULL, '', '', '0000-00-00 00:00:00', '0000-00-00 00:00:00', NULL, 1, 0, 0),
(45, 1, NULL, 'RUBRO', 45, '', '', 0, 0, '', '', 0, 0, NULL, '', '', '0000-00-00 00:00:00', '0000-00-00 00:00:00', NULL, 1, 0, 0),
(46, 1, NULL, 'Anticipo', 46, '', '', 0, 0, '', '', 0, 0, NULL, '', '', '0000-00-00 00:00:00', '0000-00-00 00:00:00', NULL, 1, 0, 0),
(47, 1, NULL, 'Identificaci?n oficial (INE, pasaporte, c?dula profesional, otras)', 47, '', '', 0, 0, '', '', 0, 0, NULL, '', '', '0000-00-00 00:00:00', '0000-00-00 00:00:00', NULL, 1, 0, 0),
(48, 1, NULL, 'Acta de matrimonio', 48, '', '', 0, 0, '', '', 0, 0, NULL, '', '', '0000-00-00 00:00:00', '0000-00-00 00:00:00', NULL, 1, 0, 0),
(49, 1, NULL, 'Acta de Nacimiento', 49, '', '', 0, 0, '', '', 0, 0, NULL, '', '', '0000-00-00 00:00:00', '0000-00-00 00:00:00', NULL, 1, 0, 0),
(50, 1, NULL, 'CURP', 50, '', '', 0, 0, '', '', 0, 0, NULL, '', '', '0000-00-00 00:00:00', '0000-00-00 00:00:00', NULL, 1, 0, 0),
(51, 1, NULL, 'RFC', 51, '', '', 0, 0, '', '', 0, 0, NULL, '', '', '0000-00-00 00:00:00', '0000-00-00 00:00:00', NULL, 1, 0, 0),
(52, 1, NULL, 'Acta constitutiva', 52, '', '', 0, 0, '', '', 0, 0, NULL, '', '', '0000-00-00 00:00:00', '0000-00-00 00:00:00', NULL, 1, 0, 0),
(53, 1, NULL, 'Poder del representante legal', 53, '', '', 0, 0, '', '', 0, 0, NULL, '', '', '0000-00-00 00:00:00', '0000-00-00 00:00:00', NULL, 1, 0, 0),
(54, 1, NULL, 'Extranjero', 54, '', '', 0, 0, '', '', 0, 0, NULL, '', '', '0000-00-00 00:00:00', '0000-00-00 00:00:00', NULL, 1, 0, 0),
(55, 1, NULL, 'Forma Migratoria', 55, '', '', 0, 0, '', '', 0, 0, NULL, '', '', '0000-00-00 00:00:00', '0000-00-00 00:00:00', NULL, 1, 0, 0),
(56, 1, NULL, 'Identificaci?n del garante hipotecario', 56, '', '', 0, 0, '', '', 0, 0, NULL, '', '', '0000-00-00 00:00:00', '0000-00-00 00:00:00', NULL, 1, 0, 0),
(57, 1, NULL, 'Identificaci?n oficial del obligado solidario', 57, '', '', 0, 0, '', '', 0, 0, NULL, '', '', '0000-00-00 00:00:00', '0000-00-00 00:00:00', NULL, 1, 0, 0);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `rubros_categorias`
--

CREATE TABLE `rubros_categorias` (
  `id` int(11) NOT NULL,
  `nombre` varchar(150) DEFAULT NULL,
  `orden` int(3) DEFAULT NULL,
  `tabla` varchar(45) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  `creado_por` int(11) DEFAULT NULL,
  `modificado_por` int(11) DEFAULT NULL,
  `borrado_por` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Volcado de datos para la tabla `rubros_categorias`
--

INSERT INTO `rubros_categorias` (`id`, `nombre`, `orden`, `tabla`, `created_at`, `updated_at`, `deleted_at`, `creado_por`, `modificado_por`, `borrado_por`) VALUES
(1, 'VENDEDOR', 1, NULL, '2018-05-10 05:00:00', NULL, NULL, 1, 0, 0),
(2, 'COMPRADOR', 2, NULL, '2018-05-10 05:00:00', NULL, NULL, 1, 0, 0),
(3, 'PREFIRMA / PREVIOS', 3, NULL, '2018-05-10 05:00:00', NULL, NULL, 1, 0, 0),
(4, 'PROYECTO', 4, NULL, '2018-05-10 05:00:00', NULL, NULL, 1, 0, 0),
(5, 'FIRMA', 5, 'tramites_avances_firmas', '2018-05-10 05:00:00', NULL, NULL, 1, 0, 0),
(6, 'PAGO', 6, NULL, '2018-05-10 05:00:00', NULL, NULL, 1, 0, 0),
(7, 'POSTFIRMA', 7, NULL, '2018-05-10 05:00:00', NULL, NULL, 1, 0, 0);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `tipo_inmueble`
--

CREATE TABLE `tipo_inmueble` (
  `id` int(11) NOT NULL,
  `idTipoInmueble` varchar(45) DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Volcado de datos para la tabla `tipo_inmueble`
--

INSERT INTO `tipo_inmueble` (`id`, `idTipoInmueble`) VALUES
(1, 'Terreno'),
(2, 'TC Nuevas-CasaHabitación'),
(3, 'TC Nuevas-Comercio'),
(4, 'Casa Habitación'),
(5, 'Comercio');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `tipo_iodentificacion`
--

CREATE TABLE `tipo_iodentificacion` (
  `id` int(11) NOT NULL,
  `TipoIdentificacion` varchar(45) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Volcado de datos para la tabla `tipo_iodentificacion`
--

INSERT INTO `tipo_iodentificacion` (`id`, `TipoIdentificacion`) VALUES
(1, 'No Aplica'),
(2, 'INE'),
(3, 'Licencia de Conducir'),
(4, 'Cédula Profesional'),
(5, 'Pasaporte'),
(6, 'Otro');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `tipo_operacion`
--

CREATE TABLE `tipo_operacion` (
  `id` int(11) NOT NULL,
  `TipoOperacion` varchar(45) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Volcado de datos para la tabla `tipo_operacion`
--

INSERT INTO `tipo_operacion` (`id`, `TipoOperacion`) VALUES
(1, 'Contado'),
(2, 'Credito Bancario'),
(3, 'Cofinanciado'),
(4, 'Fideicomiso');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `tramite`
--

CREATE TABLE `tramite` (
  `id` int(11) NOT NULL,
  `Folio` varchar(45) DEFAULT NULL,
  `idNotaria` int(11) NOT NULL,
  `FechaApertura` date DEFAULT NULL,
  `FechaTermino` date DEFAULT NULL,
  `idEntidad` int(11) NOT NULL,
  `Calle` varchar(120) DEFAULT NULL,
  `NumExt` varchar(15) DEFAULT NULL,
  `NumInt` varchar(15) DEFAULT NULL,
  `Colonia` varchar(45) DEFAULT NULL,
  `idTipoTramite` int(11) NOT NULL,
  `CP` varchar(7) DEFAULT NULL,
  `municipio` varchar(30) DEFAULT NULL,
  `ValorCatastral` double DEFAULT NULL,
  `ValorAvaluo` double DEFAULT NULL,
  `ValorVenta` double DEFAULT NULL,
  `idModulos` int(11) NOT NULL,
  `idTipoInmuebles` int(11) NOT NULL,
  `idTipoOperacion` int(11) NOT NULL,
  `idDesarrollador` int(11) NOT NULL,
  `idBanco` int(11) NOT NULL,
  `idFondoVivienda` int(11) NOT NULL,
  `isCodominio` int(1) DEFAULT '0',
  `TieneAvaluoBancario` int(1) DEFAULT '0',
  `AplicaJornadaNotarial` int(1) DEFAULT '0',
  `FechaCaptura` datetime DEFAULT NULL,
  `UserCreate` int(11) DEFAULT NULL,
  `LastUpdate` datetime DEFAULT NULL,
  `UserUpdate` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Volcado de datos para la tabla `tramite`
--

INSERT INTO `tramite` (`id`, `Folio`, `idNotaria`, `FechaApertura`, `FechaTermino`, `idEntidad`, `Calle`, `NumExt`, `NumInt`, `Colonia`, `idTipoTramite`, `CP`, `municipio`, `ValorCatastral`, `ValorAvaluo`, `ValorVenta`, `idModulos`, `idTipoInmuebles`, `idTipoOperacion`, `idDesarrollador`, `idBanco`, `idFondoVivienda`, `isCodominio`, `TieneAvaluoBancario`, `AplicaJornadaNotarial`, `FechaCaptura`, `UserCreate`, `LastUpdate`, `UserUpdate`) VALUES
(1, '1', 1, '2018-05-10', '2018-05-24', 1, '1 OTE SUR', '1968', '1', 'SAN FRANCISCO', 2, '29066', 'TUXTLA GUTIERREZ', 1, 1, 1, 1, 1, 1, 1, 4, 1, 1, 1, 1, '2018-05-10 20:47:23', 1, '2018-05-11 00:00:00', NULL);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `tramites_avances`
--

CREATE TABLE `tramites_avances` (
  `id` int(11) NOT NULL,
  `tramites_id` int(11) NOT NULL,
  `rubros_id` int(11) NOT NULL,
  `activado` tinyint(1) DEFAULT NULL,
  `fecha_inicio` date DEFAULT NULL,
  `fecha_entrega` date DEFAULT NULL,
  `fecha_termino` date DEFAULT NULL,
  `comentario` varchar(245) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  `creado_por` int(11) DEFAULT NULL,
  `modificado_por` int(11) DEFAULT NULL,
  `borrado_por` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Volcado de datos para la tabla `tramites_avances`
--

INSERT INTO `tramites_avances` (`id`, `tramites_id`, `rubros_id`, `activado`, `fecha_inicio`, `fecha_entrega`, `fecha_termino`, `comentario`, `created_at`, `updated_at`, `deleted_at`, `creado_por`, `modificado_por`, `borrado_por`) VALUES
(1, 1, 1, 1, '2018-05-11', '2018-05-10', '2018-05-11', '', '2018-05-11 06:29:26', '2018-05-11 06:30:38', NULL, 1, 1, NULL);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `tramites_avances_firmantes`
--

CREATE TABLE `tramites_avances_firmantes` (
  `id` int(11) NOT NULL,
  `tramites_avances_firmas_id` int(11) NOT NULL,
  `nombre` varchar(150) DEFAULT NULL,
  `correo` varchar(145) DEFAULT NULL,
  `lugar_firma` varchar(45) DEFAULT NULL,
  `fecha_firma` date DEFAULT NULL,
  `veces_notificado` int(2) DEFAULT NULL,
  `ultima_notificacion` date DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  `creado_por` int(11) DEFAULT NULL,
  `modificado_por` int(11) DEFAULT NULL,
  `borrado_por` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `tramites_avances_firmas`
--

CREATE TABLE `tramites_avances_firmas` (
  `id` int(11) NOT NULL,
  `tramites_id` int(11) NOT NULL,
  `numero_escritura` varchar(45) DEFAULT NULL,
  `libro` varchar(45) DEFAULT NULL,
  `folio` varchar(45) DEFAULT NULL,
  `fecha_pase` date DEFAULT NULL,
  `fecha_firma` date DEFAULT NULL,
  `fecha_limite_firma` date DEFAULT NULL,
  `fecha_autorizacion` date DEFAULT NULL,
  `numero_firmantes` int(2) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  `creado_por` int(11) DEFAULT NULL,
  `modificado_por` int(11) DEFAULT NULL,
  `borrado_por` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `tramites_rubros`
--

CREATE TABLE `tramites_rubros` (
  `id` int(11) NOT NULL,
  `tramites_tipos_id` int(11) NOT NULL,
  `rubros_id` int(11) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  `creado_por` int(11) DEFAULT NULL,
  `modificado_por` int(11) DEFAULT NULL,
  `borrado_por` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `tramites_tipo`
--

CREATE TABLE `tramites_tipo` (
  `idTipo` int(11) NOT NULL,
  `TipoTramite` varchar(45) DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Volcado de datos para la tabla `tramites_tipo`
--

INSERT INTO `tramites_tipo` (`idTipo`, `TipoTramite`) VALUES
(1, 'Inmobiliario desarrollador'),
(2, 'Inmobiliario Bancos'),
(3, 'Inmobiliario Particular'),
(4, 'Otros');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `tramitetipocompravende`
--

CREATE TABLE `tramitetipocompravende` (
  `idTipo` int(11) NOT NULL,
  `TipoCompraVende` varchar(45) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Volcado de datos para la tabla `tramitetipocompravende`
--

INSERT INTO `tramitetipocompravende` (`idTipo`, `TipoCompraVende`) VALUES
(1, 'Comprador'),
(2, 'Vendedor');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `tramite_comprador`
--

CREATE TABLE `tramite_comprador` (
  `id` int(11) NOT NULL,
  `idTramite` int(11) DEFAULT NULL,
  `Nombre` varchar(120) DEFAULT NULL,
  `Domicilio` varchar(200) DEFAULT NULL,
  `CorreoElectronico` varchar(95) DEFAULT NULL,
  `TelCasa` varchar(20) DEFAULT NULL,
  `TelOficina` varchar(45) DEFAULT NULL,
  `Extension` varchar(10) DEFAULT NULL,
  `Celular` varchar(25) DEFAULT NULL,
  `idEstadoCivil` int(11) NOT NULL,
  `idRegimenPatrimonial` int(11) NOT NULL,
  `idNacionalidad` int(11) NOT NULL,
  `LugarNacimiento` varchar(45) DEFAULT NULL,
  `FechaNacimiento` date DEFAULT NULL,
  `CURP` varchar(45) DEFAULT NULL,
  `RFC` varchar(45) DEFAULT NULL,
  `idAdquirente` int(11) NOT NULL,
  `idTipoIdentificacion` int(11) NOT NULL,
  `NumeroIdentificacion` varchar(45) DEFAULT NULL,
  `AplicaPoderRepLegal` int(1) DEFAULT '0',
  `AplicaObligadoSolidario` int(1) DEFAULT '0',
  `idTipoCompraVende` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Volcado de datos para la tabla `tramite_comprador`
--

INSERT INTO `tramite_comprador` (`id`, `idTramite`, `Nombre`, `Domicilio`, `CorreoElectronico`, `TelCasa`, `TelOficina`, `Extension`, `Celular`, `idEstadoCivil`, `idRegimenPatrimonial`, `idNacionalidad`, `LugarNacimiento`, `FechaNacimiento`, `CURP`, `RFC`, `idAdquirente`, `idTipoIdentificacion`, `NumeroIdentificacion`, `AplicaPoderRepLegal`, `AplicaObligadoSolidario`, `idTipoCompraVende`) VALUES
(1, 1, 'ELIECER RAMIREZ ESQUINCA', '1 OTE SUR # 1968, SAN FRANCISCO', 'ramirez.esquinca@gmail.com', '111111111111', '11111', '1', '111111111111111111111', 2, 2, 2, 'tuxtla', '1985-06-05', 'raee850605hcsmls01', 'raee850605', 1, 2, '123', 1, 1, 1),
(2, 1, 'ELIECER RAMIREZ ESQUINCA', '1 OTE SUR # 1968, SAN FRANCISCO', 'ramirez.esquinca@gmail.com', '111111111111', '11111', '1', '111111111111111111111', 2, 2, 2, 'tuxtla', '1985-06-05', 'raee850605hcsmls01', 'raee850605', 2, 2, '123', 1, 1, 1);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `tramite_ejecutivo_desarrollador`
--

CREATE TABLE `tramite_ejecutivo_desarrollador` (
  `idTramite` int(11) NOT NULL,
  `idDesarrollador` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `users`
--

CREATE TABLE `users` (
  `id` int(11) NOT NULL,
  `Nombre` varchar(125) DEFAULT NULL,
  `Celular` varchar(45) DEFAULT NULL,
  `password` varchar(60) DEFAULT NULL,
  `email` varchar(85) DEFAULT NULL,
  `idNotaria` int(11) NOT NULL,
  `parentid` int(11) DEFAULT NULL,
  `idModulo` int(11) DEFAULT NULL,
  `Paterno` varchar(45) DEFAULT NULL,
  `Foto` longtext,
  `idStatus` int(2) DEFAULT '1',
  `idPerfil` int(11) DEFAULT '1',
  `LastUpdate` datetime DEFAULT NULL,
  `LandingPage` varchar(45) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Volcado de datos para la tabla `users`
--

INSERT INTO `users` (`id`, `Nombre`, `Celular`, `password`, `email`, `idNotaria`, `parentid`, `idModulo`, `Paterno`, `Foto`, `idStatus`, `idPerfil`, `LastUpdate`, `LandingPage`) VALUES
(1, 'ELIECER RAMIREZ ESQUINCA', '9612449031', '$2y$10$CxNDdSoC27yH5/uw//n7Pu.and28Ns.sU5/n.U9D8n9N0y4RumrEC', 'ramirez.esquinca@gmail.com', 1, NULL, 1, NULL, NULL, 1, 1, NULL, NULL);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `usuarios_accesos`
--

CREATE TABLE `usuarios_accesos` (
  `id` int(11) NOT NULL,
  `Username` varchar(45) DEFAULT NULL,
  `Agente` varchar(120) DEFAULT NULL,
  `IPaddress` varchar(65) DEFAULT NULL,
  `Comentario` varchar(120) DEFAULT NULL,
  `FechaHora` datetime DEFAULT NULL,
  `HashCode` varchar(120) DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Índices para tablas volcadas
--

--
-- Indices de la tabla `adquirente`
--
ALTER TABLE `adquirente`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `bancos`
--
ALTER TABLE `bancos`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `calendarios`
--
ALTER TABLE `calendarios`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `desarrollador`
--
ALTER TABLE `desarrollador`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `ejecutivo_desarrollador`
--
ALTER TABLE `ejecutivo_desarrollador`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `ejecutivo_desarrollador_tipo`
--
ALTER TABLE `ejecutivo_desarrollador_tipo`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `entidad`
--
ALTER TABLE `entidad`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `estado_civil`
--
ALTER TABLE `estado_civil`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `fondos`
--
ALTER TABLE `fondos`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `modulos`
--
ALTER TABLE `modulos`
  ADD PRIMARY KEY (`id`),
  ADD KEY `fk_modulos_notaria1_idx` (`idNotaria`);

--
-- Indices de la tabla `nacionalidad`
--
ALTER TABLE `nacionalidad`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `notaria`
--
ALTER TABLE `notaria`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `regimen_patrimonial`
--
ALTER TABLE `regimen_patrimonial`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `rubros`
--
ALTER TABLE `rubros`
  ADD PRIMARY KEY (`id`),
  ADD KEY `fk_rubros_rubros_categorias1_idx` (`rubros_categorias_id`),
  ADD KEY `fk_rubros_rubros1_idx` (`rubros_id`);

--
-- Indices de la tabla `rubros_categorias`
--
ALTER TABLE `rubros_categorias`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `tipo_inmueble`
--
ALTER TABLE `tipo_inmueble`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `tipo_iodentificacion`
--
ALTER TABLE `tipo_iodentificacion`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `tipo_operacion`
--
ALTER TABLE `tipo_operacion`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `tramite`
--
ALTER TABLE `tramite`
  ADD PRIMARY KEY (`id`),
  ADD KEY `fk_tramite_usuarios1_idx` (`UserCreate`),
  ADD KEY `fk_tramite_usuarios2_idx` (`UserUpdate`);

--
-- Indices de la tabla `tramites_avances`
--
ALTER TABLE `tramites_avances`
  ADD PRIMARY KEY (`id`),
  ADD KEY `fk_tramites_avances_tramite1_idx` (`tramites_id`),
  ADD KEY `fk_tramites_avances_rubros1_idx` (`rubros_id`);

--
-- Indices de la tabla `tramites_avances_firmantes`
--
ALTER TABLE `tramites_avances_firmantes`
  ADD PRIMARY KEY (`id`),
  ADD KEY `fk_tramites_avances_firmantes_tramites_avances_firmas1_idx` (`tramites_avances_firmas_id`);

--
-- Indices de la tabla `tramites_avances_firmas`
--
ALTER TABLE `tramites_avances_firmas`
  ADD PRIMARY KEY (`id`),
  ADD KEY `fk_tramites_avances_firmas_tramite1_idx` (`tramites_id`);

--
-- Indices de la tabla `tramites_rubros`
--
ALTER TABLE `tramites_rubros`
  ADD PRIMARY KEY (`id`),
  ADD KEY `fk_tramites_rubros_tramites_tipo1_idx` (`tramites_tipos_id`),
  ADD KEY `fk_tramites_rubros_rubros1_idx` (`rubros_id`);

--
-- Indices de la tabla `tramites_tipo`
--
ALTER TABLE `tramites_tipo`
  ADD PRIMARY KEY (`idTipo`);

--
-- Indices de la tabla `tramitetipocompravende`
--
ALTER TABLE `tramitetipocompravende`
  ADD PRIMARY KEY (`idTipo`);

--
-- Indices de la tabla `tramite_comprador`
--
ALTER TABLE `tramite_comprador`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `tramite_ejecutivo_desarrollador`
--
ALTER TABLE `tramite_ejecutivo_desarrollador`
  ADD PRIMARY KEY (`idTramite`,`idDesarrollador`),
  ADD KEY `fk_tramite_has_ejecutivo_desarrollador_ejecutivo_desarrolla_idx` (`idDesarrollador`),
  ADD KEY `fk_tramite_has_ejecutivo_desarrollador_tramite1_idx` (`idTramite`);

--
-- Indices de la tabla `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`),
  ADD KEY `fk_usuarios_notaria_idx` (`idNotaria`),
  ADD KEY `fk_usuarios_modulos1_idx` (`idModulo`);

--
-- Indices de la tabla `usuarios_accesos`
--
ALTER TABLE `usuarios_accesos`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT de las tablas volcadas
--

--
-- AUTO_INCREMENT de la tabla `adquirente`
--
ALTER TABLE `adquirente`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;
--
-- AUTO_INCREMENT de la tabla `bancos`
--
ALTER TABLE `bancos`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=10;
--
-- AUTO_INCREMENT de la tabla `calendarios`
--
ALTER TABLE `calendarios`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT de la tabla `desarrollador`
--
ALTER TABLE `desarrollador`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;
--
-- AUTO_INCREMENT de la tabla `ejecutivo_desarrollador`
--
ALTER TABLE `ejecutivo_desarrollador`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT de la tabla `ejecutivo_desarrollador_tipo`
--
ALTER TABLE `ejecutivo_desarrollador_tipo`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT de la tabla `entidad`
--
ALTER TABLE `entidad`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;
--
-- AUTO_INCREMENT de la tabla `estado_civil`
--
ALTER TABLE `estado_civil`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;
--
-- AUTO_INCREMENT de la tabla `fondos`
--
ALTER TABLE `fondos`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;
--
-- AUTO_INCREMENT de la tabla `modulos`
--
ALTER TABLE `modulos`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=10;
--
-- AUTO_INCREMENT de la tabla `nacionalidad`
--
ALTER TABLE `nacionalidad`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;
--
-- AUTO_INCREMENT de la tabla `notaria`
--
ALTER TABLE `notaria`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;
--
-- AUTO_INCREMENT de la tabla `regimen_patrimonial`
--
ALTER TABLE `regimen_patrimonial`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;
--
-- AUTO_INCREMENT de la tabla `rubros`
--
ALTER TABLE `rubros`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=58;
--
-- AUTO_INCREMENT de la tabla `rubros_categorias`
--
ALTER TABLE `rubros_categorias`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;
--
-- AUTO_INCREMENT de la tabla `tipo_inmueble`
--
ALTER TABLE `tipo_inmueble`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;
--
-- AUTO_INCREMENT de la tabla `tipo_iodentificacion`
--
ALTER TABLE `tipo_iodentificacion`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;
--
-- AUTO_INCREMENT de la tabla `tipo_operacion`
--
ALTER TABLE `tipo_operacion`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;
--
-- AUTO_INCREMENT de la tabla `tramite`
--
ALTER TABLE `tramite`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT de la tabla `tramites_avances`
--
ALTER TABLE `tramites_avances`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT de la tabla `tramites_avances_firmantes`
--
ALTER TABLE `tramites_avances_firmantes`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT de la tabla `tramites_avances_firmas`
--
ALTER TABLE `tramites_avances_firmas`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT de la tabla `tramites_rubros`
--
ALTER TABLE `tramites_rubros`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT de la tabla `tramites_tipo`
--
ALTER TABLE `tramites_tipo`
  MODIFY `idTipo` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;
--
-- AUTO_INCREMENT de la tabla `tramitetipocompravende`
--
ALTER TABLE `tramitetipocompravende`
  MODIFY `idTipo` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT de la tabla `tramite_comprador`
--
ALTER TABLE `tramite_comprador`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT de la tabla `users`
--
ALTER TABLE `users`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT de la tabla `usuarios_accesos`
--
ALTER TABLE `usuarios_accesos`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- Restricciones para tablas volcadas
--

--
-- Filtros para la tabla `modulos`
--
ALTER TABLE `modulos`
  ADD CONSTRAINT `fk_modulos_notaria1` FOREIGN KEY (`idNotaria`) REFERENCES `notaria` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Filtros para la tabla `rubros`
--
ALTER TABLE `rubros`
  ADD CONSTRAINT `fk_rubros_rubros1` FOREIGN KEY (`rubros_id`) REFERENCES `rubros` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `fk_rubros_rubros_categorias1` FOREIGN KEY (`rubros_categorias_id`) REFERENCES `rubros_categorias` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Filtros para la tabla `tramite`
--
ALTER TABLE `tramite`
  ADD CONSTRAINT `fk_tramite_usuarios1` FOREIGN KEY (`UserCreate`) REFERENCES `users` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `fk_tramite_usuarios2` FOREIGN KEY (`UserUpdate`) REFERENCES `users` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Filtros para la tabla `tramites_avances`
--
ALTER TABLE `tramites_avances`
  ADD CONSTRAINT `fk_tramites_avances_rubros1` FOREIGN KEY (`rubros_id`) REFERENCES `rubros` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `fk_tramites_avances_tramite1` FOREIGN KEY (`tramites_id`) REFERENCES `tramite` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Filtros para la tabla `tramites_avances_firmantes`
--
ALTER TABLE `tramites_avances_firmantes`
  ADD CONSTRAINT `fk_tramites_avances_firmantes_tramites_avances_firmas1` FOREIGN KEY (`tramites_avances_firmas_id`) REFERENCES `tramites_avances_firmas` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Filtros para la tabla `tramites_avances_firmas`
--
ALTER TABLE `tramites_avances_firmas`
  ADD CONSTRAINT `fk_tramites_avances_firmas_tramite1` FOREIGN KEY (`tramites_id`) REFERENCES `tramite` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Filtros para la tabla `tramites_rubros`
--
ALTER TABLE `tramites_rubros`
  ADD CONSTRAINT `fk_tramites_rubros_rubros1` FOREIGN KEY (`rubros_id`) REFERENCES `rubros` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `fk_tramites_rubros_tramites_tipo1` FOREIGN KEY (`tramites_tipos_id`) REFERENCES `tramites_tipo` (`idTipo`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Filtros para la tabla `tramite_ejecutivo_desarrollador`
--
ALTER TABLE `tramite_ejecutivo_desarrollador`
  ADD CONSTRAINT `fk_tramite_has_ejecutivo_desarrollador_ejecutivo_desarrollador1` FOREIGN KEY (`idDesarrollador`) REFERENCES `ejecutivo_desarrollador` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `fk_tramite_has_ejecutivo_desarrollador_tramite1` FOREIGN KEY (`idTramite`) REFERENCES `tramite` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Filtros para la tabla `users`
--
ALTER TABLE `users`
  ADD CONSTRAINT `fk_usuarios_modulos1` FOREIGN KEY (`idModulo`) REFERENCES `modulos` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `fk_usuarios_notaria` FOREIGN KEY (`idNotaria`) REFERENCES `notaria` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
