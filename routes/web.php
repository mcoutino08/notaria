<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

/***********************************************************************************
expediente routes
************************************************************************************/
Route::get('/expedientes_captura','ExpedientesController@init')->middleware('auth');

Route::post('/add_expediente','ExpedientesController@GeneralInmueble');
Route::get('/expediente_edit', 'ExpedientesController@edit');
Route::post('/expediente_edit', 'ExpedientesController@edit');
Route::post('/expediente_saveEdit', 'ExpedientesController@savereplace');

Route::post('/comprador_add','ExpedientesController@Comprador');
Route::post('/vendedor_add','ExpedientesController@Vendedor');
Route::post('/contactoBanco','ExpedientesController@ContactoBanco');
Route::post('/ContactoDesarrollador','ExpedientesController@ContactoDesarrollador');
 	




// Route::post('/add_expediente_Comprador','ExpedientesController@guardarDatosComprador');
/***********************************************************************************
fichas captura routes
************************************************************************************/
Route::get('/expedientes','fichaCapturaController@init');
/***********************************************************************************
 documentos Integracion routes
************************************************************************************/
Route::get('/documentosIntegracion','documentosIntegracionController@init');
/***********************************************************************************
 documentos Integracion routes
************************************************************************************/
Route::get('/reportesExpedientes','reportesExpedientesController@init');



Auth::routes();



Route::get('/home', 'HomeController@index')->name('home');
Route::get('/notarias','Auth\RegisterController@notariasCombo');
Route::get('/modulos','Auth\RegisterController@modulosCombo');
