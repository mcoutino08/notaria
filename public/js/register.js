$.ajax({
  url: '/notarias',
  type: 'GET',
}).done(function(data){
  $('#SelectNotaria').html(data.html);
}).fail(function(jqXHR, textStatus, error){
  bootbox.dialog({
    message: `<p class="text-center">${error}</p>`,
    closeButton: true
});
});

$.ajax({
  url: '/modulos',
  type: 'GET',
}).done(function (data) {
  $('#SelectModulo').html(data.html);
}).fail(function (jqXHR, textStatus, error) {
  bootbox.dialog({
    message: `<p class="text-center">${error}</p>`,
    closeButton: true
  });
});

$(document).ready(function(){

});


$('form').submit(function (e) {
  // si la cantidad de checkboxes "chequeados" es cero,
  // entonces se evita que se envíe el formulario y se
  // muestra una alerta al usuario
  if ($('input[type=checkbox]:checked').length === 0) {
    e.preventDefault();
    alert('Debe seleccionar al menos un modulo');
  }
});

function Glow(Message, TipeMessage) {
  setTimeout(function () {
    $.bootstrapGrowl(`${Message}`, {
      ele: 'body', // which element to append to
      type: `${TipeMessage}`, // (null, 'info', 'danger', 'success')
      offset: { from: 'top', amount: 130 }, // 'top', or 'bottom'
      align: 'right', // ('left', 'right', or 'center')
      width: 250, // (integer, or 'auto')
      delay: 4000, // Time while the message will be displayed. It's not equivalent to the *demo* timeOut!
      allow_dismiss: false, // If true then will display a cross to close the popup.
      stackup_spacing: 10 // spacing between consecutively stacked growls.
    }, 100);
  });
};