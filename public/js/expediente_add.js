$(document).ready(function() {
  $('.nav-tabs a').click(function(ev){
    $(this).tab('');
})

let comprador=false;
$('#nuevoComprador').on('click', function() {
  comprador=true;
});

let vendedor = false;
$('#nuevoVendedor').on('click', function () {
    vendedor = true;
});

let contactoBan = false;
    $('#nuevoContactoBan').on('click', function () {
    contactoBan = true;
});

    let contactoDes = false;
    $('#nuevoContactoDes').on('click', function () {
    contactoDes = true;
});
 
    let editarTram = false;
    $('#editarTramite').on('click', function () {
        editarTram = true;
    });
    
let idTramite;
    $('#DatosGeneralesInmueble').on('submit', function(ev){
        ev.preventDefault();
        $.ajax({
            type : 'post',
            url : '/add_expediente',
            data : new FormData(this),
            contentType : false,
            cache : false,
            processData : false,
            timeout : 6000,
            success : function (data) {
              console.log(data);
              Glow(data.message,'success');
              $('#idTramiteComprador').val(data.lastTramite);
              idTramite = data.lastTramite;
            },error: function(jqXHR, exception) {
              Glow(`[${jqXHR.status}] ${jqXHR.statusText}`,'danger');
            }
         });
    });

    $('#DatosComprador').on('submit', function (ev) {
        ev.preventDefault();
        $.ajax({
            type: 'post',
            url: '/comprador_add',
            data: new FormData(this),
            contentType: false,
            cache: false,
            processData: false,
            timeout: 6000,
            success: function (data) {
              Glow(data.message,'success');
              if (comprador && data.success) {
                document.getElementById('DatosComprador').reset();
                comprador = false;
              }
            },
            error: function(jqXHR,exception){
                Glow(`[${jqXHR.status}] ${jqXHR.statusText}`,'danger');
            }
        });
    });

    $('#DatosVendedor').on('submit', function (ev) {
        ev.preventDefault();
        console.log();
        $.ajax({
            type: 'post',
            url: '/vendedor_add',
            data: new FormData(this),
            contentType: false,
            cache: false,
            processData: false,
            timeout: 6000,
            success: function (data) {
              console.log(data);
              Glow(data.message,'success');
                if (vendedor && data.success) {
                    document.getElementById('DatosVendedor').reset();
                    vendedor = false;
                }
            },
            error: function(jqXHR,exception){
                Glow(`[${jqXHR.status}] ${jqXHR.statusText}`,'danger');
            }
        });
    });


    $('#DatosContactoBanco').on('submit', function (ev) {
        ev.preventDefault();
        console.log();
        $.ajax({
            type: 'post',
            url: '/contactoBanco',
            data: new FormData(this),
            contentType: false,
            cache: false,
            processData: false,
            timeout: 6000,
            success: function (data) {
              console.log(data);
              Glow(data.message,'success');
                if (contactoBan && data.success) {
                    document.getElementById('DatosContactoBanco').reset();
                    contactoBan = false;
                }
            },
            error: function(jqXHR,exception){
                Glow(`[${jqXHR.status}] ${jqXHR.statusText}`,'danger');
            }
        });
    });


    $('#DatosContactoDesarrollador').on('submit', function (ev) {
        ev.preventDefault();
        console.log();
        $.ajax({
            type: 'post',
            url: '/ContactoDesarrollador',
            data: new FormData(this),
            contentType: false,
            cache: false,
            processData: false,
            timeout: 6000,
            success: function (data) {
              console.log(data);
              Glow(data.message,'success');
                if (contactoDes && data.success) {
                    document.getElementById('DatosContactoDesarrollador').reset();
                    contactoDes = false;
                }
            },
            error: function(jqXHR,exception){
                Glow(`[${jqXHR.status}] ${jqXHR.statusText}`,'danger');
            }
        });
      });
});






function Glow (Message, TipeMessage){
  setTimeout(function() {
      $.bootstrapGrowl(`${Message}`, {
        ele: 'body', // which element to append to
        type: `${TipeMessage}`, // (null, 'info', 'danger', 'success')
        offset: {from: 'top', amount: 130}, // 'top', or 'bottom'
        align: 'right', // ('left', 'right', or 'center')
        width: 250, // (integer, or 'auto')
        delay: 4000, // Time while the message will be displayed. It's not equivalent to the *demo* timeOut!
        allow_dismiss: false, // If true then will display a cross to close the popup.
        stackup_spacing: 10 // spacing between consecutively stacked growls.
  }, 100);
  });
};
