let mix = require('laravel-mix');

/*
 |--------------------------------------------------------------------------
 | Mix Asset Management
 |--------------------------------------------------------------------------
 |
 | Mix provides a clean, fluent API for defining some Webpack build steps
 | for your Laravel application. By default, we are compiling the Sass
 | file for the application as well as bundling up all the JS files.
 |
 */

/*mix.js('resources/assets/js/app.js', 'public/js')
   .sass('resources/assets/sass/app.scss', 'public/css');*/
   mix.styles([
      'resources/assets/global/plugins/bootstrap/css/bootstrap.css',
      'resources/assets/global/plugins/bootstrap-switch/css/bootstrap-switch.min.css',
      'resources/assets/global/css/components-rounded.css',
      'resources/assets/global/css/plugins.min.css',
      'resources/assets/layouts/layout/css/layout.min.css',
      'resources/assets/layouts/layout/css/themes/darkblue.min.css',
      'resources/assets/layouts/layout/css/custom.min.css',
      ],'public/css/nacional.css')
   .styles([
      'resources/assets/global/plugins/simple-line-icons/simple-line-icons.css'
      ],'public/css/simple-line-icons.css')
   .styles([
      'resources/assets/global/plugins/font-awesome/css/font-awesome.css'
      ],'public/css/font-awesome.css')
   .styles([
      'resources/assets/global/plugins/bootstrap-timepicker/css/bootstrap-timepicker.min.css',
      'resources/assets/global/plugins/bootstrap-datetimepicker/css/bootstrap-datetimepicker.min.css'
      ],'public/css/bootstrap-timepicker.css')
   .styles([
      'resources/assets/global/plugins/bootstrap-datepicker/css/bootstrap-datepicker3.min.css'
      ],'public/css/bootstrap-datepicker3.min.css')
   .styles([
      'resources/assets/global/plugins/bootstrap-select/css/bootstrap-select.min.css'
      ],'public/css/bootstrap-select.min.css')
   .styles([
      'resources/assets/global/plugins/jquery-minicolors/jquery.minicolors.css'
      ],'public/css/jquery.minicolors.css')
   .styles([
      'resources/assets/pages/css/error.min.css'
      ],'public/css/error.min.css')
     .styles([
       'resources/assets/global/plugins/bootstrap-datepicker/css/bootstrap-datepicker3.css'
     ], 'public/css/bootstrap-datepicker3.css')

   .scripts([
      'resources/assets/global/plugins/jquery.min.js',
      'resources/assets/global/plugins/bootstrap/js/bootstrap.min.js',
      'resources/assets/global/plugins/js.cookie.min.js',
      'resources/assets/global/plugins/jquery-slimscroll/jquery.slimscroll.min.js',
      'resources/assets/global/plugins/jquery.blockui.min.js',
      'resources/assets/global/plugins/bootstrap-switch/js/bootstrap-switch.min.js',
      'resources/assets/global/scripts/app.min.js',
      'resources/assets/layouts/layout/scripts/layout.min.js',
      'resources/assets/layouts/global/scripts/quick-sidebar.min.js',
      'resources/assets/layouts/global/scripts/quick-nav.min.js',
      'resources/assets/global/plugins/jquery-ui/jquery-ui.min.js',
      'resources/assets/global/plugins/bootbox/bootbox.min.js',
      'resources/js/ajax-token.js'
      ],'public/js/nacional.js')
   .scripts([
      'resources/assets/global/plugins/bootstrap-timepicker/js/bootstrap-timepicker.min.js',
      'resources/assets/global/plugins/bootstrap-datetimepicker/js/bootstrap-datetimepicker.min.js'
      ],'public/js/bootstrap-timepicker.js')
   .scripts([
      'resources/assets/global/plugins/bootstrap-datepicker/js/bootstrap-datepicker.min.js',
      'resources/assets/global/plugins/bootstrap-datepicker/locales/bootstrap-datepicker.es.min.js'
      ],'public/js/bootstrap-datepicker.min.js')
   .scripts([
      'resources/assets/global/plugins/jquery-validation/js/jquery.validate.min.js'
      ],'public/js/jquery.validate.min.js')
   .scripts([
      'resources/assets/global/plugins/jquery-validation/js/jquery.validate.js'
      ],'public/js/jquery.validate.js')
   .scripts([
      'resources/assets/global/plugins/jquery-validation/js/additional-methods.min.js'
      ],'public/js/additional-methods.min.js')
   .scripts([
      'resources/assets/global/plugins/bootstrap-select/js/bootstrap-select.min.js'
      ],'public/js/bootstrap-select.min.js')
    .scripts([
      'resources/assets/global/plugins/bootstrap-tabdrop/js/bootstrap-tabdrop.js'
      ],'public/js/bootstrap-tabdrop.js')
    .scripts([
      'resources/assets/global/plugins/jquery-minicolors/jquery.minicolors.min.js'
      ],'public/js/jquery.minicolors.min.js')
   .scripts([
      'resources/js/jquery.form.js'
      ],'public/js/jquery.form.js')
   .scripts([
      'resources/js/jquery_temas.js'
      ],'public/js/jquery_temas.js')
   .scripts([
      'resources/js/jquery_temas-add-edit.js'
      ],'public/js/jquery_temas-add-edit.js')
     .scripts(['resources/js/bootstrap-select.js'
     ], 'public/js/bootstrap-select.js')
     .scripts(['resources/js/fichasCaptura.js'
     ], 'public/js/fichasCaptura.js')
     .scripts(['resources/js/expediente_add.js'
     ], 'public/js/expediente_add.js')
     .scripts(['resources/js/register.js'
   ], 'public/js/register.js')

     .scripts(['resources/assets/global/plugins/select2/js/select2.full.min.js'
     ], 'public/js/select2.full.min.js')
     .scripts(['resources/assets/global/plugins/jquery-validation/js/jquery.validate.min.js'
     ], 'public/js/jquery.validate.min.js')
     .scripts(['resources/assets/global/plugins/jquery-validation/js/additional-methods.min.js'
     ], 'public/js/additional-methods.min.js')
     .scripts(['resources/assets/global/plugins/bootstrap-wizard/jquery.bootstrap.wizard.min.js'
     ], 'public/js/jquery.bootstrap.wizard.js')
     .scripts(['resources/assets/pages/scripts/form-wizard.min.js'
     ], 'public/js/form-wizard.min.js')
     .scripts(['resources/assets/global/plugins/bootstrap-growl/jquery.bootstrap-growl.min.js'
     ], 'public/js/jquery.bootstrap-growl.min.js')


   .copy([
      'resources/assets/pages/img/logo-big.png',
      'resources/assets/pages/img/bg-opacity.png',
      'resources/assets/pages/img/bg-white.png',
      'resources/assets/pages/img/inbox-nav-arrow-blue.png',
      'resources/assets/pages/img/logo-big-white.png',
      'resources/assets/pages/img/logo-invert.png',
      'resources/assets/layouts/layout/img/avatar3_small.jpg',
      'resources/assets/pages/img/logo.png',
      'resources/assets/pages/media/users/avatar1.jpg',
      'resources/assets/global/img/remove-icon-small.png'
      ],'public/img')
   .copy([
      'resources/assets/global/plugins/bootstrap/fonts/bootstrap/glyphicons-halflings-regular.eot',
      'resources/assets/global/plugins/bootstrap/fonts/bootstrap/glyphicons-halflings-regular.woff2',
      'resources/assets/global/plugins/bootstrap/fonts/bootstrap/glyphicons-halflings-regular.woff',
      'resources/assets/global/plugins/bootstrap/fonts/bootstrap/glyphicons-halflings-regular.ttf',
      'resources/assets/global/plugins/bootstrap/fonts/bootstrap/glyphicons-halflings-regular.svg'
      ],'public/fonts_bootstrap')
   .copy([
      'resources/assets/global/plugins/font-awesome/fonts/fontawesome-webfont.eot',
      'resources/assets/global/plugins/font-awesome/fonts/fontawesome-webfont.svg',
      'resources/assets/global/plugins/font-awesome/fonts/fontawesome-webfont.ttf',
      'resources/assets/global/plugins/font-awesome/fonts/fontawesome-webfont.woff',
      'resources/assets/global/plugins/font-awesome/fonts/fontawesome-webfont.woff2',
      'resources/assets/global/plugins/font-awesome/fonts/FontAwesome.otf'
      ],'public/fonts_fontawesome')
   .copy([
      'resources/assets/global/plugins/simple-line-icons/fonts/Simple-Line-Icons.eot',
      'resources/assets/global/plugins/simple-line-icons/fonts/Simple-Line-Icons.woff',
      'resources/assets/global/plugins/simple-line-icons/fonts/Simple-Line-Icons.ttf',
      'resources/assets/global/plugins/simple-line-icons/fonts/Simple-Line-Icons.svg'
      ],'public/fonts_simple-line-icons')
   .copy([
      'resources/assets/fonts_google_api/59ZRklaO5bWGqF5A9baEERJtnKITppOI_IvcXXDNrsc.woff2',
      'resources/assets/fonts_google_api/DXI1ORHCpsQm3Vp6mXoaTRWV49_lSm1NYrwo-zkhivY.woff2',
      'resources/assets/fonts_google_api/DXI1ORHCpsQm3Vp6mXoaTT0LW-43aMEzIO6XUTLjad8.woff2',
      'resources/assets/fonts_google_api/DXI1ORHCpsQm3Vp6mXoaTZX5f-9o1vgP2EXwfjgl7AY.woff2',
      'resources/assets/fonts_google_api/DXI1ORHCpsQm3Vp6mXoaTa-j2U0lmluP9RWlSytm3ho.woff2',
      'resources/assets/fonts_google_api/DXI1ORHCpsQm3Vp6mXoaTaaRobkAwv3vxw3jMhVENGA.woff2',
      'resources/assets/fonts_google_api/DXI1ORHCpsQm3Vp6mXoaTegdm0LZdjqr5-oayXSOefg.woff2',
      'resources/assets/fonts_google_api/DXI1ORHCpsQm3Vp6mXoaTf8zf_FOSsgRmwsS7Aa9k2w.woff2',
      'resources/assets/fonts_google_api/K88pR3goAWT7BTt32Z01mxJtnKITppOI_IvcXXDNrsc.woff2',
      'resources/assets/fonts_google_api/LWCjsQkB6EMdfHrEVqA1KRJtnKITppOI_IvcXXDNrsc.woff2',
      'resources/assets/fonts_google_api/MTP_ySUJH_bn48VBG8sNShWV49_lSm1NYrwo-zkhivY.woff2',
      'resources/assets/fonts_google_api/MTP_ySUJH_bn48VBG8sNSj0LW-43aMEzIO6XUTLjad8.woff2',
      'resources/assets/fonts_google_api/MTP_ySUJH_bn48VBG8sNSpX5f-9o1vgP2EXwfjgl7AY.woff2',
      'resources/assets/fonts_google_api/MTP_ySUJH_bn48VBG8sNSq-j2U0lmluP9RWlSytm3ho.woff2',
      'resources/assets/fonts_google_api/MTP_ySUJH_bn48VBG8sNSqaRobkAwv3vxw3jMhVENGA.woff2',
      'resources/assets/fonts_google_api/MTP_ySUJH_bn48VBG8sNSugdm0LZdjqr5-oayXSOefg.woff2',
      'resources/assets/fonts_google_api/MTP_ySUJH_bn48VBG8sNSv8zf_FOSsgRmwsS7Aa9k2w.woff2',
      'resources/assets/fonts_google_api/RjgO7rYTmqiVp7vzi-Q5URJtnKITppOI_IvcXXDNrsc.woff2',
      'resources/assets/fonts_google_api/cJZKeOuBrn4kERxqtaUH3VtXRa8TVwTICgirnJhmVJw.woff2',
      'resources/assets/fonts_google_api/k3k702ZOKiLJc3WVjuplzBWV49_lSm1NYrwo-zkhivY.woff2',
      'resources/assets/fonts_google_api/k3k702ZOKiLJc3WVjuplzD0LW-43aMEzIO6XUTLjad8.woff2',
      'resources/assets/fonts_google_api/k3k702ZOKiLJc3WVjuplzJX5f-9o1vgP2EXwfjgl7AY.woff2',
      'resources/assets/fonts_google_api/k3k702ZOKiLJc3WVjuplzK-j2U0lmluP9RWlSytm3ho.woff2',
      'resources/assets/fonts_google_api/k3k702ZOKiLJc3WVjuplzKaRobkAwv3vxw3jMhVENGA.woff2',
      'resources/assets/fonts_google_api/k3k702ZOKiLJc3WVjuplzOgdm0LZdjqr5-oayXSOefg.woff2',
      'resources/assets/fonts_google_api/k3k702ZOKiLJc3WVjuplzP8zf_FOSsgRmwsS7Aa9k2w.woff2',
      'resources/assets/fonts_google_api/u-WUoqrET9fUeobQW7jkRRJtnKITppOI_IvcXXDNrsc.woff2',
      'resources/assets/fonts_google_api/xozscpT2726on7jbcb_pAhJtnKITppOI_IvcXXDNrsc.woff2'
      ],'public/fonts');
